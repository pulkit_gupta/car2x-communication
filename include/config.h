//WLAN-Environment

#define PLANET_08
#define PLANET
//#define KNET
//#define TUC

/*EEPROM Address-Definitionen */
#define SERIAL          0
#define TYPE            2
#define HARDWARE        4

//ETHERNET
#define ETH_MAC         6
#define ETH_IP          12
#define ETH_MASK        16
#define ETH_GATEWAY     24

//USB
#define USB_MAC         30


//XBEE_MODUL_1    

/*
Network-ID  MY  2 Byte
PAN-ID          2 Byte
Channel + MAC-Mode     6 Bit + 2 Bit 
*/
#define  X0_MY          0x20
#define  X0_PAN         0x22
#define  X0_CHMAC       0x24

#define  X1_MY          0x30
#define  X1_PAN         0x32
#define  X1_CHMAC       0x34

#define  X2_MY          0x40
#define  X2_PAN         0x42
#define  X2_CHMAC       0x44

//Zigbee Communication
//IEEE EUI - globally unique number
#define EUI_7 0x00
#define EUI_6 0x01
#define EUI_5 0x02
#define EUI_4 0x03
#define EUI_3 0x04
#define EUI_2 0x05
#define EUI_1 0x06
#define EUI_0 0x07


#define PANID_L	0x30
#define PANID_H	0x33

#define NODEID_L 0x11
#define NODEID_H 0x22


/* QUEUES  & BUFFER */
#define UART0_DMA
#define UART1_DMA
#define UARTD_DMA

#define USART0_RX_BUF      128
#define USART0_TX_BUF      16
#define USART1_RX_BUF      64
#define USART1_TX_BUF      16

#define USARTD_RX_BUF      16
#define USARTD_TX_BUF      32

#define  QUEUE_RXUART0_SIZE    64
#define  QUEUE_RXUART1_SIZE    32
#define  QUEUE_RXUARTD_SIZE    64

#define  QUEUE_TXUART0_SIZE     16
#define  QUEUE_TXUART1_SIZE     16
#define  QUEUE_TXUARTD_SIZE     64

//----------- Tasks ----------------------------------------------------------

#define  TASK_PROCESSING_PRIORITY    5
#define  TASK_IO_PRIORITY           5
#define  TASK_ALIVE_PRIORITY         5
#define  TASK_RECV_PRIORITY         5
#define  TASK_ETH_PRIORITY         5

#define  TASK_PROCESSING_STK_SIZE  128
#define  TASK_ALIVE_STK_SIZE      64
#define  TASK_IO_STK_SIZE          128
#define  TASK_RECV_STK_SIZE          128
#define  TASK_ETH_STK_SIZE          256

#define EVENT_RECEIVE_USB 0x01
#define EVENT_RECEIVE_XBEE 0x02
#define EVENT_RECEIVE_OTHER 0x04
