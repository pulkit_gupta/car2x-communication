/*
 * lcd.h
 *
 *  Created on: 10.09.2008
 *      Author: limir
 */

#ifndef LCD_H
#define LCD_H

void LcdWriteData(unsigned char data);
void LcdWriteDataParts(unsigned char data);
void LcdWriteDataHighNibble(unsigned char data);
void LcdWriteDataLowNibble(unsigned char data);
void LcdEnable(void);
void LcdWriteCommand(unsigned char command);

void InitLcd(void);
void LCD_Write_Data(unsigned char);
void LCD_Text(const char*);
void LCD_WriteHex(unsigned long data,unsigned char mincharlen,unsigned char row, unsigned char col);
void LCD_WriteInt(unsigned long data,unsigned char mincharlen,unsigned char row, unsigned char col);
void LCD_WriteTextPos(unsigned char *text, unsigned char row,unsigned char col);
void LcdWriteCharPos(unsigned char c, unsigned char row,unsigned char col);


/* LCD_COMMANDS */
#define CLEAR_LCD         0x01 /* Clear Display                        */
#define RETURN_HOME       0x02 /* Cursor to Home position              */
#define ENTRY_MODE        0x06 /* Normal entry mode                    */
#define ENTRY_MODE_SHIFT  0x07 /* - with shift                         */
#define SYSTEM_SET_8_BIT  0x38 /* 8 bit data mode 2 line ( 5x7 font )  */
#define SYSTEM_SET_4_BIT  0x28 /* 4 bit data mode 2 line ( 5x7 font )  */
#define DISPLAY_ON        0x0c /* Switch ON Display                    */
#define DISPLAY_OFF       0x08 /* Cursor plus blink                    */
#define SET_DD_LINE1      0x80 /* Line 1 position 1                    */
#define SET_DD_LINE2      0xC0 /* Line 2 position 1                    */
#define SET_DD_RAM        0x80 /* Line 1 position 1                    */
#define WRITE_DATA        0x00 /* With RS = 1                          */
#define CURSOR_ON         0x0E /* Switch Cursor ON                     */
#define CURSOR_OFF        0x0C /* Switch Cursor OFF    				   */

/*
Anfangsoffsetpositionen der einzelnen Zeilen innerhalb des LCD-Speicher
*/
#define LCD_ROW_OFFSET_0  0x00
#define LCD_ROW_OFFSET_1  0x40
#define LCD_ROW_OFFSET_2  0x14
#define LCD_ROW_OFFSET_3  0x54

#endif /* LCD_H_ */
