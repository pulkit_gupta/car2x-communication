#ifndef UART_H
#define UART_H

#include  "config.h"

int uart0_putc(int ch);
int uart1_putc(int ch);
int uartd_putc(int ch);

int uart0_putbuf(unsigned char* buffer, unsigned int lenght);
int uart1_putbuf(unsigned char* buffer, unsigned int lenght);
int uartd_putbuf(unsigned char* buffer, unsigned int lenght);


/* QUEUES */
//
//TN_DQUE  queueRxUart0;void *queueRxUart0Mem[QUEUE_RXUART0_SIZE];
//TN_DQUE  queueTxUart0;void *queueTxUart0Mem[QUEUE_TXUART0_SIZE];
volatile unsigned char usart0_rx_buf0[USART0_RX_BUF];
volatile unsigned char usart0_rx_buf1[USART0_RX_BUF];
volatile unsigned char usart0_tx_buf0[USART0_TX_BUF];


//TN_DQUE  queueRxUart1;void *queueRxUart1Mem[QUEUE_RXUART1_SIZE];
//TN_DQUE  queueTxUart1;void *queueTxUart1Mem[QUEUE_TXUART1_SIZE];
volatile unsigned char usart1_rx_buf0[USART1_RX_BUF];
volatile unsigned char usart1_rx_buf1[USART1_RX_BUF];
volatile unsigned char usart1_tx_buf0[USART1_TX_BUF];


//TN_DQUE  queueRxUartD;
void *queueRxUartDMem[QUEUE_RXUARTD_SIZE];
//TN_DQUE  queueTxUartD;
void *queueTxUartDMem[QUEUE_TXUARTD_SIZE];
volatile unsigned char usartD_rx_buf0[USARTD_RX_BUF];
volatile unsigned char usartD_rx_buf1[USARTD_RX_BUF];
volatile unsigned char usartD_tx_buf0[USARTD_TX_BUF];


#endif

