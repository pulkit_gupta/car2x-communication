#ifndef HARDWARE_H
#define HARDWARE_H

#include "typedefs.h"

#define nop()  __asm__ __volatile__("nop")

//Taktgeber
#define EXT_OC          18432000   // external xtal
#define MCK             47923200
//48054857   // MCK (PLLRC div by 2)
#define MCKKHz          (MCK/1000) //

//default USART-Baudrate
#define BR    115200                        /* default Baud Rate */
#define BRD  (MCK/16/19200)                    /* Async Baud Rate Divisor */
#define BRDS (MCK/BR)                       /*Sync Baud Rate Divisor*/
#define BRD_19200  (MCK/16/19200)                    /* Baud Rate Divisor */
//#define BRD  (MCK/16/9600)                    /* Baud Rate Divisor */

//Initialisierung der Grundlegenden Hardware

#define LED1	0x00000010	//PA4
#define LED2	0x08000000	//PA27
#define LED3	0x10000000	//PA28
#define LED4	0x20000000 	//PA29

#define LCD_E	0x04000000	//PA26
#define LCD_RS	0x00800000	//PA23

#define LCD_D4	0x00010000	//PA16
#define LCD_D5	0x00020000	//PA17
#define LCD_D6	0x00040000	//PA18
#define LCD_D7	0x00080000	//PA19


#define MRF24J40_CS     0x01000000	//PA24
#define MRF24J40_RESET  0x00000001	//PA0
#define MRF24J40_INT    0x00000004	//PA2

#define MRF24WB0_CS     0x01000000	//PA24
#define MRF24WB0_RESET  0x00000001	//PA0
#define MRF24WB0_HIB    0x00000002	//PA1
#define MRF24WB0_INT    0x00000004	//PA2

#define USB_PUP     0x02000000

//SPI - Chipselect
#define SPI_EXTERN    0
#define SPI_ETHERNET  1
#define SPI_SDCARD    3
#define SPI_EEPROM    7
#define SPI_NONE      15

#define SPI_MRF24J40 0
#define SPI_MRF24WB0 0


//Taster
//SD_ON -> PA8
//LCD_ON -> PA7

#define SDCARD_POWER	0x00000100  //PA8
#define LCD_BACKLIGHT	0x00000080  //PA7

#define SDCARD_DETECT   0x00008000

#define   BIT0        0x00000001
#define   BIT1        0x00000002
#define   BIT2        0x00000004
#define   BIT3        0x00000008
#define   BIT4        0x00000010
#define   BIT5        0x00000020
#define   BIT6        0x00000040
#define   BIT7        0x00000080
#define   BIT8        0x00000100
#define   BIT9        0x00000200
#define   BIT10       0x00000400
#define   BIT11       0x00000800
#define   BIT12       0x00001000
#define   BIT13       0x00002000
#define   BIT14       0x00004000
#define   BIT15       0x00008000
#define   BIT16       0x00010000
#define   BIT17       0x00020000
#define   BIT18       0x00040000
#define   BIT19       0x00080000
#define   BIT20       0x00100000
#define   BIT21       0x00200000
#define   BIT22       0x00400000
#define   BIT23       0x00800000
#define   BIT24       0x01000000
#define   BIT25       0x02000000
#define   BIT26       0x04000000
#define   BIT27       0x08000000
#define   BIT28       0x10000000
#define   BIT29       0x20000000
#define   BIT30       0x40000000
#define   BIT31       0x80000000

void Delay (unsigned long a);
void InitHardware(void);
void UsbPup(bool e);
//unsigned int GetAdcChanel(unsigned char chanel);

unsigned char ReadSwitches(void);
unsigned char SDCardDetect(void);

void InitUart0(void);
void InitUart1(void);
void InitUartD(void);

//Taster
#define KEY_DOWN   1
#define KEY_LEFT   2
#define KEY_RIGHT  4
#define KEY_UP     8


#define MRF24J40_RESET_LOW()	(AT91C_BASE_PIOA->PIO_CODR = MRF24J40_RESET)
#define MRF24J40_RESET_HIGH()	(AT91C_BASE_PIOA->PIO_SODR = MRF24J40_RESET)

#define MRF24J40_SELECT_LOW()	(SPI_SelectDevice(SPI_MRF24J40))
#define MRF24J40_SELECT_HIGH()	(SPI_UnselectDevice())

#define MRF24WB0_RESET_LOW()	(AT91C_BASE_PIOA->PIO_CODR = MRF24WB0_RESET)
#define MRF24WB0_RESET_HIGH()	(AT91C_BASE_PIOA->PIO_SODR = MRF24WB0_RESET)

#define MRF24WB0_SELECT_LOW()	(SPI_SelectDevice(SPI_MRF24WB0))
#define MRF24WB0_SELECT_HIGH()	(SPI_UnselectDevice())

#define MRF24WB0_IRQ_DISABLE()	(AT91C_BASE_PIOA->PIO_IDR = MRF24WB0_INT)
#define MRF24WB0_IRQ_ENABLE()	(AT91C_BASE_PIOA->PIO_IER = MRF24WB0_INT)





#if 0

extern volatile unsigned char usart0_rx_buf0[USART0_RX_BUF];
extern volatile unsigned char usart0_rx_buf1[USART0_RX_BUF];
extern volatile unsigned char usart1_rx_buf0[USART1_RX_BUF];
extern volatile unsigned char usart1_rx_buf1[USART1_RX_BUF];
extern volatile unsigned char usartD_rx_buf0[USARTD_RX_BUF];
extern volatile unsigned char usartD_rx_buf1[USARTD_RX_BUF];

int tn_snprintf( char *outStr, int maxLen, const char *fmt, ... );
void * s_memcpy(void * s1, const void *s2, int n);
void * s_memset(void * dst, int ch, int length);
int s_strcmp(char * str1, char * str2);
int s_strncmp(char * str1, char * str2, int num);
char * s_strncat(char * dst, char *src, int n);
char * s_strcat(char * str1, char *str2);
char * s_strncpy(char * dst, char *src, int n);
char * s_strcpy(char * dst, char *src);
int s_strlen(char * str);
int s_abs(int i);
#endif
/*=========================================================================*/
/*  DEFINE: Definition of all local Procedures                             */
/*=========================================================================*/



void Delay (unsigned long a);
static void InitUARTs (void);                  /* Initialize all serial interfaces */
static void Usart0Int(void);
void InitUart0(void);
void InitUart1(void);
void InitUartD(void);
void InitCPU(void);
void InitADC(void);
void InitLEDs(void);
void InitPowerSwitches(void);
void InitLcdPins(void);
void InitSwitches(void);
void InitSPI(void);
void InitTimer0(void);
unsigned int GetAdcChanel(unsigned char channel); 
unsigned char ReadSwitches(void);
unsigned char SDCardDetect(void);;
void SDCardPower(unsigned char action);
void SwitchLed(unsigned char number, unsigned char action);
void InitHardware(void);
#endif