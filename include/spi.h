#ifndef _SPI_H_
#define _SPI_H_


#include "hardware.h"
#include "typedefs.h"
#include "AT91SAM7S256.h"


void SPI_SelectDevice(unsigned char device);
void  SPI_UnselectDevice(void);

unsigned char spiTransferByte(unsigned char data);
unsigned long spiTransfer32(unsigned long data);
unsigned short spiTransfer16(unsigned short data);
u16 SPIWrite(u08 * ptrBuffer, u16 ui_Len);
u16 SPIRead(u08 * ptrBuffer, u16 ui_Len);

#endif
