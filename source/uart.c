/*
 * lcd.c
 *
 *  Created on: 10.09.2008
 *      Author: limir
 */

#include "uart.h"
#include "typedefs.h"
#include "AT91SAM7S256.h"
#include "hardware.h"


/* DEBUG - UART */
/**
Schreibt den Inhalt eines Buffers mit der L�nge lenght auf die UART
*/
int uartd_putbuf(unsigned char* buffer, unsigned int lenght)
{
	while (lenght>0)
        {
            uartd_putc( *buffer++ );
              lenght--;
	}
	return 0;
}

int uartd_putc(int ch) 
{
      //AT91C_BASE_DBGU->DBGU_THR
         
        
        while (!(AT91C_BASE_DBGU->DBGU_CSR & AT91C_US_TXRDY));   /* Wait for Empty Tx Buffer */
	return (AT91C_BASE_DBGU->DBGU_THR = ch);                 /* Transmit Character */
}	

int uartd_putchar (int ch)                       /* Write Character to Serial Port */
{
  if (ch == '\n')  
    {                            /* Check for LF */
      uartd_putc( '\r' );                         /* Output CR */
  }
  return uartd_putc( ch );                     /* Transmit Character */
}

int uartd_puts ( char* s )
{
	int i = 0;
	while ( *s ) {
		uartd_putc( *s++ );
		i++;
	}
	return i;
}

int uartd_prints ( char* s )
{
	int i = 0;
	while ( *s ) {
		uartd_putchar( *s++ );
		i++;
	}
	return i;
}

int uartd_kbhit( void ) /* returns true if character in receive buffer */
{
	if ( AT91C_BASE_DBGU->DBGU_CSR & AT91C_US_RXRDY) {
		return 1;
	}
	else {
		return 0;
	}
}

int uartd_getc ( void )  /* Read Character from Serial Port */
{    
  while (!(AT91C_BASE_DBGU->DBGU_CSR & AT91C_US_RXRDY));   /* Wait for Full Rx Buffer */
  return (AT91C_BASE_DBGU->DBGU_RHR);                      /* Read Character */
}



/* UART 0 */
int uart0_putbuf(unsigned char* buffer, unsigned int lenght)
{
	while (lenght>0)
        {
            uart0_putc( *buffer++ );
            lenght--;
	}
	return 0;
}

int uart0_putc(int ch) 
{

//tn_queue_send(&queueTxUart0, (void *)(ch),TN_WAIT_INFINITE)  ;
//AT91C_BASE_US0->US_IER = AT91C_US_TXRDY;

//return 0;
/**/

	while (!(AT91C_BASE_US0->US_CSR & AT91C_US_TXRDY))
        
        
        ;   /* Wait for Empty Tx Buffer */
	return (AT91C_BASE_US0->US_THR = ch);                 /* Transmit Character */
        
}	

int uart0_putchar (int ch) {                      /* Write Character to Serial Port */

  if (ch == '\n')  {                            /* Check for LF */
    uart0_putc( '\r' );                         /* Output CR */
  }
  return uart0_putc( ch );                     /* Transmit Character */
}

int uart0_puts ( char* s )
{
	int i = 0;
	while ( *s ) {
		uart0_putc( *s++ );
		i++;
	}
	return i;
}

int uart0_prints ( char* s )
{
	int i = 0;
	while ( *s ) {
		uart0_putchar( *s++ );
		i++;
	}
	return i;
}

int uart0_kbhit( void ) /* returns true if character in receive buffer */
{
	if ( AT91C_BASE_US0->US_CSR & AT91C_US_RXRDY) {
		return 1;
	}
	else {
		return 0;
	}
}

int uart0_getc ( void )  /* Read Character from Serial Port */
{    
  while (!(AT91C_BASE_US0->US_CSR & AT91C_US_RXRDY));   /* Wait for Full Rx Buffer */
  return (AT91C_BASE_US0->US_RHR);                      /* Read Character */
}

/* UART 1 */
int uart1_putbuf(unsigned char* buffer, unsigned int lenght)
{
	while (--lenght>0)
        {
            uart1_putc( *buffer++ );
	}
	return 0;
}


int uart1_putc(int ch) 
{
	while (!(AT91C_BASE_US1->US_CSR & AT91C_US_TXRDY));   /* Wait for Empty Tx Buffer */
	return (AT91C_BASE_US1->US_THR = ch);                 /* Transmit Character */
}	

int uart1_putchar (int ch) {                      /* Write Character to Serial Port */

  if (ch == '\n')  {                            /* Check for LF */
    uart1_putc( '\r' );                         /* Output CR */
  }
  return uart0_putc( ch );                     /* Transmit Character */
}

int uart1_puts ( char* s )
{
	int i = 0;
	while ( *s ) {
		uart1_putc( *s++ );
		i++;
	}
	return i;
}

int uart1_prints ( char* s )
{
	int i = 0;
	while ( *s ) {
		uart1_putchar( *s++ );
		i++;
	}
	return i;
}

int uart1_kbhit( void ) /* returns true if character in receive buffer */
{
	if ( AT91C_BASE_US1->US_CSR & AT91C_US_RXRDY) {
		return 1;
	}
	else {
		return 0;
	}
}

int uart1_getc ( void )  /* Read Character from Serial Port */
{    
  while (!(AT91C_BASE_US1->US_CSR & AT91C_US_RXRDY));   /* Wait for Full Rx Buffer */
  return (AT91C_BASE_US1->US_RHR);                      /* Read Character */
}


int uart0_getc_queue(void)
{
  int rc,in_data;
/*  
  rc = tn_queue_receive(&queueRxUart0,(void **)&in_data,TN_WAIT_INFINITE);
  if  (rc== TERR_NO_ERR) return in_data;
*/
  return -1;  //oder rc, wenn stehts <0


}


int uart1_getc_queue(void)
{
  int rc,in_data;
/*
  rc = tn_queue_receive(&queueRxUart1,(void **)&in_data,TN_WAIT_INFINITE);
  if  (rc== TERR_NO_ERR) return in_data;
*/
  return -1;  //oder rc, wenn stehts <0


}

int uartd_getc_queue(void)
{
  int rc,in_data;
/*
  rc = tn_queue_receive(&queueRxUartD,(void **)&in_data,TN_WAIT_INFINITE);
  if  (rc== TERR_NO_ERR) return in_data;
*/
  return -1;  //oder rc, wenn stehts <0

}



/* Generic */
int uart_putbuf(unsigned char device, unsigned char* buffer, unsigned int lenght)
{
  switch (device)
  {
    case 0:
    	while (lenght>0)
        {
            uart0_putc( *buffer++ );
            lenght--;
	}
        break;
    case 1:
        while (lenght>0)
        {
            uart1_putc( *buffer++ );
            lenght--;
	}
        break;
    case 2:
        while (lenght>0)
        {
            uartd_putc( *buffer++ );
            lenght--;
	}
        break;
    default: 
      return -1;
  }
	return 0;
}

int UartPutChar(unsigned char device, unsigned int ch)
//int uart_putc(unsigned char device, unsigned int ch)
{
  switch (device)
  {
    case 0:
        uart0_putc(ch);
        break;
    case 1:
        uart1_putc(ch);
        break;
    case 2:
        uartd_putc(ch);
        break;
    default: 
      return -1;
  }
  return 0;
}

int uart_getc_queue(unsigned char device)
{
  int rc;
  int in_data;
  unsigned char *data;
  switch (device)
  {
/*
    case 0:
      //rc = 0x11;

      rc = tn_queue_receive(&queueRxUart0,(void**)&in_data,TN_WAIT_INFINITE);
      break;
    case 1:
      rc = tn_queue_receive(&queueRxUart1,(void**)&in_data,TN_WAIT_INFINITE);
      break;
    case 2:
      rc = tn_queue_receive(&queueRxUartD,(void**)&in_data,TN_WAIT_INFINITE);
      break;
    default: 
      return -1;
*/
  }

  //if (in_data == 0xAA) return in_data+1;
  //*data = in_data;

  //if  (rc== TERR_NO_ERR) return in_data;
  return -1;  //oder rc, wenn stehts <0
}





/*
void U1RX_IRQ_Service (void)
{

	//schreibe in Queue
	
	//solange Daten im Puffer sind ...
	while (U1STAbits.URXDA)
	{
		tn_queue_isend_polling(&queueRxUart1, (void *)((int)(U1RXREG)));
	}
	
	IFS0bits.U1RXIF =0;

}

void U1TX_IRQ_Service (void)
{
	
	char data;int rc;
	//Schreibe raus und 
	rc = tn_queue_ireceive(&queueTxUart1, (void **)&data);
	if (rc == TERR_NO_ERR)
		U1TXREG = data;
	IFS0bits.U1TXIF =0;
}
*/
