#include "hardware.h"
#include "AT91SAM7S256.h"
#include "typedefs.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "main.h"
#include "lcd.h"


extern unsigned char flagCan;

extern xQueueHandle xbeeRxQueue;
//extern xQueueHandle pctRxQueue;
//extern xQueueHandle dummyRxQueue;
//extern xQueueHandle spiRxQueue;


portBASE_TYPE xHigherPriorityTaskWoken;

unsigned char rx_buffer[256];
unsigned char *p_buffer = &rx_buffer[0];
unsigned int rx_crc = 0;
unsigned int len_frame = 4;

unsigned char spi_buffer[256];

void pitc_handler(void) __attribute__ ((interrupt ("IRQ")));
void pitc_handler(void)
{
    volatile unsigned long pivr = 0;   
    volatile unsigned long pisr = 0;  
    pisr = AT91C_BASE_PITC->PITC_PISR & AT91C_PITC_PITS;   
    pivr = AT91C_BASE_PITC->PITC_PIVR;   
    LedSwitch(0,2);
    //AT91C_BASE_AIC->AIC_ICCR = (0x1 << (AT91C_ID_SYS)) ;   // clear the system interrupt
}


void ISR_Timer0(void) __attribute__ ((interrupt ("IRQ")));
void ISR_Timer0(void)
{
    volatile unsigned int uiStat = 0;   
    //LedSwitch(0,2);
   //api_switch_led(1,2);
    uiStat = AT91C_BASE_TC0->TC_SR; 
}


void ISR_Uart0(void) __attribute__ ((interrupt ("IRQ")));   //Xbee
void ISR_Uart0(void)
{
    
    LedSwitch(2,1);    
    *p_buffer++ = uart0_getc();    
  
  if ( ++rx_crc == rx_buffer[2] + 4)
    { 
          len_frame = rx_buffer[2] + 4;
          p_buffer = &rx_buffer[0];
          while(rx_crc)
          {

            //uart1_putc(rx_buffer[len_frame - rx_crc]);
            xQueueSendToBackFromISR( xbeeRxQueue, &rx_buffer[len_frame - rx_crc], &xHigherPriorityTaskWoken);
            rx_crc--;
          }
          memset(rx_buffer,0,sizeof(rx_buffer));
          rx_crc = 0;
    }
    LedSwitch(2,0);    
   
   
}


void ISR_Uart1(void) __attribute__ ((interrupt ("IRQ")));
void ISR_Uart1(void)
{
   //uart0_putc(uart1_getc());   
  /*  volatile unsigned int uiStat = 0;  
    uiStat = AT91C_BASE_US0->US_CSR;
    unsigned char c = AT91C_BASE_US1->US_RHR;
    
    xQueueSendToBackFromISR( dummyRxQueue, &c, &xHigherPriorityTaskWoken);
    */

}

void ISR_UartD(void)
{
    LedSwitch(0,1);
    unsigned char c = AT91C_BASE_DBGU->DBGU_RHR;    
    xQueueSendToBackFromISR( pctRxQueue, &c, &xHigherPriorityTaskWoken);
    //LedSwitch(0,0);
}

void ISR_Pio(void) __attribute__ ((interrupt ("IRQ")));
void ISR_Pio(void)
{
    volatile unsigned int uiStat = 0;   
    uiStat = AT91C_BASE_PIOA->PIO_ISR; 
    if ((uiStat & MRF24J40_INT) != 0)
    {
       flagCan |= 0x01;
    }
    
}

