/*
 * hardware.c
 *
 *  Created on: 09.09.2008
 *      Author: limir
 */


#include "hardware.h"
#include "config.h"
#include "utils.h"
#include "typedefs.h"
#include "lcd.h"
#include "data.h"
#include "AT91SAM7S256.h"
#include <usb/device/rndis/RNDISDriver.h>
#include <usb/device/rndis/RNDISDriverDescriptors.h>


#define SPI_SCKDIV      8
unsigned long mck;
#define INT_LEVEL_TIMER     0           // Priority of interrupt
#define TIME_PERIOD       100000   // Period of 200 ms

//trace_LOG(trace_INFO, "Ethernet: received %d bytes, buf:", 44);

extern void ISR_Timer0(void);
extern void ISR_Pio(void);
extern void ISR_Usb(void);
extern void ISR_Uart0(void);
extern void ISR_Uart1(void);
extern void ISR_SPI(void);
extern void pitc_handler(void);


extern volatile unsigned char usart0_rx_buf0[USART0_RX_BUF];
extern volatile unsigned char usart0_rx_buf1[USART0_RX_BUF];
extern volatile unsigned char usart1_rx_buf0[USART1_RX_BUF];
extern volatile unsigned char usart1_rx_buf1[USART1_RX_BUF];
extern volatile unsigned char usartD_rx_buf0[USARTD_RX_BUF];
extern volatile unsigned char usartD_rx_buf1[USARTD_RX_BUF];

/*
extern volatile unsigned char usartD_rx_buf0[USARTD_RX_BUF];
extern volatile unsigned char usartD_rx_buf1[USARTD_RX_BUF];
*/

int tn_snprintf( char *outStr, int maxLen, const char *fmt, ... );
void * s_memcpy(void * s1, const void *s2, int n);
void * s_memset(void * dst, int ch, int length);
int s_strcmp(char * str1, char * str2);
int s_strncmp(char * str1, char * str2, int num);
char * s_strncat(char * dst, char *src, int n);
char * s_strcat(char * str1, char *str2);
char * s_strncpy(char * dst, char *src, int n);
char * s_strcpy(char * dst, char *src);
int s_strlen(char * str);
int s_abs(int i);

/*=========================================================================*/
/*  DEFINE: Definition of all local Procedures                             */
/*=========================================================================*/



void Delay (unsigned long a)
{
#ifndef __FLASH_BUILD
  a<<=1;    //Ausgleich f�r Laufzeit
#endif
	while (--a!=0)
          nop();
}

void InitUARTs (void)                  /* Initialize all serial interfaces */
{

  //Takt
  *AT91C_PMC_PCER |= 
    (1 << AT91C_ID_US0) |   /* Enable Clock for USART0 */
    (1 << AT91C_ID_US1);    /* Enable Clock for USART1 */

  InitUart0();
  InitUart1();
  InitUartD();
}


void Usart0Int()
{
    // AT91C_BASE_US0->US_IDR = 0xFFFFFFFF;    
}

void InitUart0(void)
{
  AT91C_BASE_PIOA->PIO_PDR |= AT91C_PA5_RXD0 | AT91C_PA6_TXD0;         /* Enalbe TxD1+RxD1 Pin */
  AT91C_BASE_PIOA->PIO_ASR |= AT91C_PA5_RXD0 | AT91C_PA6_TXD0; 

  AT91C_BASE_US0->US_CR = AT91C_US_RSTRX |          /* Reset Receiver      */
                  AT91C_US_RSTTX |          /* Reset Transmitter   */
                  AT91C_US_RXDIS |          /* Receiver Disable    */
                  AT91C_US_TXDIS;           /* Transmitter Disable */
  AT91C_BASE_US0->US_MR = AT91C_US_USMODE_NORMAL |  /* Normal Mode */
                  AT91C_US_CLKS_CLOCK    |  /* Clock = MCK */
                  AT91C_US_CHRL_8_BITS   |  /* 8-bit Data  */
                  //BIT8                   |      /*AT91C_US_SYNC_1_BIT*/
                  AT91C_US_PAR_NONE      |  /* No Parity   */
                  AT91C_US_NBSTOP_1_BIT  ;    /* 1 Stop Bit  */
  AT91C_BASE_US0->US_BRGR = BRD;                    /* Baud Rate Divisor */

  AT91C_BASE_US0->US_CR = AT91C_US_RXEN  | AT91C_US_TXEN;      /* Receiver+Transmitter Enable  */

// enable reaction on different interrupt sources
  AT91C_BASE_US0->US_IDR = 0xFFFFFFFF;
  AT91C_BASE_US0->US_IER = AT91C_US_RXRDY ;


}
void InitUart1(void)
{
  AT91C_BASE_PIOA->PIO_PDR |= AT91C_PA21_RXD1 | AT91C_PA22_TXD1;         /* Enalbe TxD1+RxD1 Pin */
  AT91C_BASE_PIOA->PIO_ASR |= AT91C_PA21_RXD1 | AT91C_PA22_TXD1;

  AT91C_BASE_US1->US_CR = AT91C_US_RSTRX |          /* Reset Receiver      */
                  AT91C_US_RSTTX |          /* Reset Transmitter   */
                  AT91C_US_RXDIS |          /* Receiver Disable    */
                  AT91C_US_TXDIS;           /* Transmitter Disable */
  AT91C_BASE_US1->US_MR = AT91C_US_USMODE_NORMAL |  /* Normal Mode */
                  AT91C_US_CLKS_CLOCK    |  /* Clock = MCK */
                  AT91C_US_CHRL_8_BITS   |  /* 8-bit Data  */
                  AT91C_US_PAR_NONE      |  /* No Parity   */
                  AT91C_US_NBSTOP_1_BIT;    /* 1 Stop Bit  */
  AT91C_BASE_US1->US_BRGR = BRD;                    /* Baud Rate Divisor */
  AT91C_BASE_US1->US_CR = AT91C_US_RXEN  | AT91C_US_TXEN;      /* Receiver+Transmitter Enable  */

// enable reaction on different interrupt sources
  AT91C_BASE_US1->US_IDR = 0xFFFFFFFF;
  AT91C_BASE_US1->US_IER = AT91C_US_RXRDY ; //Enable Interrupt
}

void InitUartD(void)
{
  AT91C_BASE_PIOA->PIO_PDR |= AT91C_PA9_DRXD | AT91C_PA10_DTXD;         /* Enalbe TxD1+RxD1 Pin */
  AT91C_BASE_PIOA->PIO_ASR |= AT91C_PA9_DRXD | AT91C_PA10_DTXD;

  AT91C_BASE_DBGU->DBGU_CR = AT91C_US_RSTRX |          /* Reset Receiver      */
                  AT91C_US_RSTTX |          /* Reset Transmitter   */
                  AT91C_US_RXDIS |          /* Receiver Disable    */
                  AT91C_US_TXDIS;           /* Transmitter Disable */
  AT91C_BASE_DBGU->DBGU_MR = AT91C_US_USMODE_NORMAL |  /* Normal Mode */
                  AT91C_US_CLKS_CLOCK    |  /* Clock = MCK */
                  AT91C_US_CHRL_8_BITS   |  /* 8-bit Data  */
                  AT91C_US_PAR_NONE      |  /* No Parity   */
                  AT91C_US_NBSTOP_1_BIT;    /* 1 Stop Bit  */
  AT91C_BASE_DBGU->DBGU_BRGR = BRD_19200;                    /* Baud Rate Divisor */
  AT91C_BASE_DBGU->DBGU_CR = AT91C_US_RXEN  | AT91C_US_TXEN;      /* Receiver+Transmitter Enable  */
   
  AT91C_BASE_DBGU->DBGU_IDR = 0xFFFFFFFF;  
  AT91C_BASE_DBGU->DBGU_IER = AT91C_US_RXRDY; //Enable Interrupt
}


void InitCPU(void)
{
   *AT91C_PMC_PCER |= (1<<2);    //-- Enable PIO clock
   *AT91C_PIOA_IDR = 0xFFFFFFFF;  //-- Disable I/O interrupts

  //--  Enable reset input
  *AT91C_RSTC_RMR  |= 0xA5000000 | 1;
#if 0
  //-- Setup  Periodic Interval Timer for interrupt 1 ms
  *AT91C_PITC_PIMR = (1 << 24) |   //-- PIT Enable
            (1 << 25) |   //-- PIT Interrupt Enable
              (1000-1);        //-- Periodic Interval Value - 1 ms
//              (3000*1-1);        //-- Periodic Interval Value - 1 ms

  // Setup System Interrupt Mode and Vector with Priority 7 and Enable it */
  // PIT's int is System Interrupt,Peripheral ID for PIT is 1
  //AT91C_BASE_AIC->AIC_SMR[1] = (1 << 5) | 7;         // AT91C_ID_SYS - System Peripheral
  AT91C_BASE_AIC->AIC_SMR[1] =| 7;         // AT91C_ID_SYS - System Peripheral
  
  
  //rAIC_IECR = (1<<1);               //-- Enable System Peripheral Int
  AT91C_BASE_AIC->AIC_IECR = 2;

  *AT91C_PMC_PCER |= (1 << AT91C_ID_PIOA) |  /* Enable Clock for PIO    */
                (1 << AT91C_ID_TC0)|    /* Enable Clock fpr Timer0 */
		(1 << AT91C_ID_IRQ0) |  /* Enable Clock for IRQ0   */
		(1 << AT91C_ID_US0) |   /* Enable Clock for USART0 */
                (1 << AT91C_ID_US1);    /* Enable Clock for USART1 */
#endif
  // Disable watchwog
  AT91C_BASE_WDTC->WDTC_WDMR= AT91C_WDTC_WDDIS;



  
  
  AT91C_BASE_PMC->PMC_MCKR = AT91C_PMC_CSS_PLL_CLK | AT91C_PMC_PRES_CLK_2; 

  AT91C_BASE_PMC->PMC_MOR = (AT91C_CKGR_OSCOUNT & (0x8 << 8)) | AT91C_CKGR_MOSCEN;
  while (!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MOSCS));

  //FLASH
  AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS;
  //AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS | (1 + (((MCK * 15) / 10000000)) << 16);


  //PLL & DIV -> siehe ATMEL Datasheet (Starting guide)

/* etwas zu ungenau zwecks SerialPort
  AT91C_BASE_PMC->PMC_PLLR = AT91C_CKGR_OUT_0 |
                   (AT91C_CKGR_PLLCOUNT & (10 << 8)) | 
                   (AT91C_CKGR_MUL & (72 << 16)) | 
                   (AT91C_CKGR_DIV & 14);
*/
  AT91C_BASE_PMC->PMC_PLLR = AT91C_CKGR_OUT_0 |
                   (AT91C_CKGR_PLLCOUNT & (10 << 8)) | 
                   (AT91C_CKGR_MUL & (25 << 16)) | 
                   (AT91C_CKGR_DIV & 5);

  while(!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_LOCK));

  AT91C_BASE_PMC->PMC_MCKR = AT91C_PMC_PRES_CLK_2 | AT91C_PMC_CSS_PLL_CLK;    
  while (!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MCKRDY));
  

  
}
void InitCPU2(void)
{
   *AT91C_PMC_PCER |= (1<<2);    //-- Enable PIO clock
   *AT91C_PIOA_IDR = 0xFFFFFFFF;  //-- Disable I/O interrupts

  //--  Enable reset input
  *AT91C_RSTC_RMR  |= 0xA5000000 | 1;

  *AT91C_PMC_PCER |= (1 << AT91C_ID_PIOA) |  /* Enable Clock for PIO    */
                (1 << AT91C_ID_TC0)|    /* Enable Clock fpr Timer0 */
		(1 << AT91C_ID_IRQ0) |  /* Enable Clock for IRQ0   */
		(1 << AT91C_ID_US0) |   /* Enable Clock for USART0 */
                (1 << AT91C_ID_US1);    /* Enable Clock for USART1 */
  // Disable watchwog
  AT91C_BASE_WDTC->WDTC_WDMR= AT91C_WDTC_WDDIS;

  AT91C_BASE_PMC->PMC_MCKR = AT91C_PMC_CSS_PLL_CLK | AT91C_PMC_PRES_CLK_2; 

  AT91C_BASE_PMC->PMC_MOR = (AT91C_CKGR_OSCOUNT & (0x8 << 8)) | AT91C_CKGR_MOSCEN;
  while (!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MOSCS));


  //FLASH
  AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS;
  //AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS | (1 + (((MCK * 15) / 10000000)) << 16);


  //PLL & DIV -> siehe ATMEL Datasheet (Starting guide)
  
  AT91C_BASE_PMC->PMC_PLLR = AT91C_CKGR_OUT_0 |
                   (AT91C_CKGR_PLLCOUNT & (10 << 8)) | 
                   (AT91C_CKGR_MUL & (25 << 16)) | 
                   (AT91C_CKGR_DIV & 5);

  while(!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_LOCK));

  AT91C_BASE_PMC->PMC_MCKR = AT91C_PMC_PRES_CLK_2 | AT91C_PMC_CSS_PLL_CLK;    
  while (!(AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MCKRDY));
  
}


void InitADC(void)
{
  //ADC: PRES=4->MCK/10 SHTIM =3 -> 4/4,8MHz=833ns STARTUP =11 ->20us
  AT91C_BASE_ADC->ADC_MR = (3<24)|(11<<16)|(4<<8);
  //start first conversion
  AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

  //enable channel and set PIN to ADCx function (no need to write PIO!!)
  AT91C_BASE_ADC->ADC_CHER = 0xF0;

} 


void InitLEDs(void)
{
	//Config LEDs
	AT91C_BASE_PIOA->PIO_PER |= (LED1 | LED2 | LED3 | LED4);    //Enable LED1
	AT91C_BASE_PIOA->PIO_OER |= (LED1 | LED2 | LED3 | LED4);    //Configure in Output
	AT91C_BASE_PIOA->PIO_SODR |=(LED1 | LED2 | LED3 | LED4);   //set reg to 1

}
void InitPowerSwitches(void)
{
	AT91C_BASE_PIOA->PIO_PER |= LCD_BACKLIGHT;    //Enable PA18
	AT91C_BASE_PIOA->PIO_OER |= LCD_BACKLIGHT;    //Configure in Output
	AT91C_BASE_PIOA->PIO_CODR |= LCD_BACKLIGHT;   //set reg to 0

	AT91C_BASE_PIOA->PIO_PER |= SDCARD_POWER;    //Enable PA18
	AT91C_BASE_PIOA->PIO_OER |= SDCARD_POWER;    //Configure in Output
	AT91C_BASE_PIOA->PIO_CODR |= SDCARD_POWER;   //set reg to 0
}


void InitLcdPins(void)
{
	AT91C_BASE_PIOA->PIO_PER  |= (LCD_E | LCD_RS | LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7);    //Enable
	AT91C_BASE_PIOA->PIO_OER  |= (LCD_E | LCD_RS | LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7);    //Configure in Output
	AT91C_BASE_PIOA->PIO_CODR |= (LCD_E | LCD_RS | LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7);   //set reg to 0
}



void InitSwitches(void)
{
        AT91C_BASE_PIOA->PIO_PER |= SDCARD_DETECT;
}

void InitSPI(void)
{
   // enable clock to SPI interface
     AT91C_BASE_PMC->PMC_PCER = (1<<AT91C_ID_SPI);
 
     // setup PIO pins for SPI bus
     AT91C_BASE_PIOA->PIO_ASR   = AT91C_PA12_MISO|AT91C_PA13_MOSI|AT91C_PA14_SPCK|AT91C_PIO_PA2;   // PA1 = cs, PA2 = int; assign pins to SPI interface
     AT91C_BASE_PIOA->PIO_PDR   = AT91C_PA12_MISO|AT91C_PA13_MOSI|AT91C_PA14_SPCK|AT91C_PIO_PA2;
     AT91C_BASE_PIOA->PIO_PPUER = AT91C_PA12_MISO|AT91C_PA13_MOSI|AT91C_PA14_SPCK|AT91C_PIO_PA2;    // set pullups

     AT91C_BASE_PIOA->PIO_PER =   AT91C_PIO_PA0|AT91C_PIO_PA1;  //Enable SPI_CS
     AT91C_BASE_PIOA->PIO_OER =   AT91C_PIO_PA0|AT91C_PIO_PA1;   //Configure in Output = 
     AT91C_BASE_PIOA->PIO_SODR =  AT91C_PIO_PA0|AT91C_PIO_PA1; 
        
  
     AT91C_BASE_PIOA->PIO_ODR =   AT91C_PIO_PA2;
     // setup PIO pins for SPI chip selects
     AT91C_BASE_PIOA->PIO_ASR = AT91C_PA11_NPCS0|AT91C_PA31_NPCS1;
     AT91C_BASE_PIOA->PIO_BSR = AT91C_PA30_NPCS2|AT91C_PA3_NPCS3;
     AT91C_BASE_PIOA->PIO_PDR = AT91C_PA11_NPCS0|AT91C_PA31_NPCS1|AT91C_PA30_NPCS2|AT91C_PA3_NPCS3;
     AT91C_BASE_PIOA->PIO_OER = AT91C_PA11_NPCS0|AT91C_PA31_NPCS1;

    // reset and enable SPI
    AT91C_BASE_SPI->SPI_CR = AT91C_SPI_SPIEN | AT91C_SPI_SWRST;
    AT91C_BASE_SPI->SPI_CR = AT91C_SPI_SPIEN; 
 
    // SPI mode: master, fixed periph. sel., FDIV=0, fault detection disabled
    //AT91C_BASE_SPI->SPI_MR = AT91C_SPI_MODFDIS | AT91C_SPI_PS_FIXED | AT91C_SPI_MSTR | (0x0E<<16);
	AT91C_BASE_SPI->SPI_MR =
                        AT91C_SPI_MSTR |	/* we use MASTER only mode */
			AT91C_SPI_PS_FIXED | 	/* we only use Fixed Peripheral Select */
			AT91C_SPI_MODFDIS | 	/* no Fault protection since there is no second master */
			//AT91C_SPI_FDIV  | 	/* no peripheral selected */
    			(0x0E<<16)|
                        ((10<<24) & AT91C_SPI_DLYBCS);	/* 10 Clock Cycles betwenn two different chip selects */
    
    //AT91C_BASE_SPI->SPI_MR = AT91C_SPI_MODFDIS | AT91C_SPI_PS_VARIABLE | AT91C_SPI_MSTR | (0x0E<<16);
 
    // set chip-select-register
    // 8 bits per transfer, CPOL=0, ClockPhase=0,
    //AT91C_SPI_CSR[0] = AT91C_SPI_BITS_8|(8<<8)|AT91C_SPI_CSAAT|AT91C_SPI_NCPHA; //AT91C_SPI_CPOL|
    //AT91C_SPI_CSR[1] = AT91C_SPI_BITS_8|(2<<8)|AT91C_SPI_CSAAT|AT91C_SPI_NCPHA; //AT91C_SPI_CPOL|
    //AT91C_SPI_CSR[2] = AT91C_SPI_BITS_8|(2<<8)|AT91C_SPI_CSAAT|AT91C_SPI_NCPHA; //AT91C_SPI_CPOL|
    //AT91C_SPI_CSR[2] = AT91C_SPI_BITS_8|(SPI_SCKDIV<<8)|AT91C_SPI_CPOL|AT91C_SPI_CSAAT;
    //AT91C_SPI_CSR[3] = AT91C_SPI_BITS_8|(SPI_SCKDIV<<8)|AT91C_SPI_CPOL|AT91C_SPI_CSAAT;
    
    AT91C_SPI_CSR[0] = AT91C_SPI_BITS_8|(50<<8)|AT91C_SPI_NCPHA;//|AT91C_SPI_CPOL;
    AT91C_SPI_CSR[1] = AT91C_SPI_BITS_8|(50<<8)|AT91C_SPI_NCPHA;//|AT91C_SPI_CPOL;
    AT91C_SPI_CSR[2] = AT91C_SPI_BITS_8|(50<<8)|AT91C_SPI_NCPHA;//|AT91C_SPI_CPOL;
    //AT91C_SPI_CSR[2] = AT91C_SPI_BITS_8|(SPI_SCKDIV<<8)|AT91C_SPI_CPOLs|AT91C_SPI_CSAAT;
    AT91C_SPI_CSR[3] = AT91C_SPI_BITS_8|(50<<8)|AT91C_SPI_NCPHA;;
     //enable the Interrupt 
   /* AT91C_BASE_SPI->SPI_IDR = 0xFFFFFFFF ;
    AT91C_BASE_SPI->SPI_PTCR = AT91C_PDC_TXTEN + AT91C_PDC_RXTEN;
    AT91C_BASE_SPI->SPI_IER = AT91C_SPI_RXBUFF   ;
  */  
}


void InitTimer0(void)
{

    AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_TC0;   // Enable clock to TC0
    AT91C_BASE_TC0->TC_CMR =  AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO | AT91C_TC_ACPA_SET | AT91C_TC_ACPC_CLEAR;   // Prescaler set to 1024, Compare mode on RC, Change TIOA0 according to RA and RC
    //AT91C_BASE_TC0->TC_RC = 0xBB9;   // Compare value of RC, clear TIOA0 and reset timer
    AT91C_BASE_TC0->TC_RC = 0x4000;   // Compare value of RC, clear TIOA0 and reset timer
  //  AT91C_BASE_TC0->TC_RA = 0x4001;  // Compare value of RA, set TIOA0
    AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKEN; // Enable timer


   //
   AT91C_BASE_TC0->TC_IER =  AT91C_TC_CPCS ;        //Timer Interrupt enable /*AT91C_TC_COVFS | AT91C_TC_LDRAS | AT91C_TC_LDRBS |*/
   AT91C_BASE_TC0->TC_CCR = 0x5;        //Timer Clock enable

   
   AT91C_BASE_AIC->AIC_IECR = (1<<AT91C_ID_TC0);
   AT91C_BASE_AIC->AIC_ICCR = (1<<AT91C_ID_TC0);
   AT91C_BASE_AIC->AIC_SMR[AT91C_ID_TC0] = (1 << 5) | 7;         // AT91C_ID_SYS - System Peripheral
   AT91C_BASE_TC0->TC_CCR = AT91C_TC_SWTRG;

}


unsigned int GetAdcChanel(unsigned char channel) 
{
  int result=1000;
  AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;
  //warten bis Convertierung abgeschlossen
  while (!(AT91C_BASE_ADC->ADC_SR & (1<<channel)) && result>0)
  {
    result--;
    nop(); 
  }
  if (result==0) return 0;
  switch (channel) {
    case 0: result = AT91C_BASE_ADC->ADC_CDR0; break;
    case 1: result = AT91C_BASE_ADC->ADC_CDR1; break;
    case 2: result = AT91C_BASE_ADC->ADC_CDR2; break;
    case 3: result = AT91C_BASE_ADC->ADC_CDR3; break;
    case 4: result = AT91C_BASE_ADC->ADC_CDR4; break;
    case 5: result = AT91C_BASE_ADC->ADC_CDR5; break;
    case 6: result = AT91C_BASE_ADC->ADC_CDR6; break;
    case 7: result = AT91C_BASE_ADC->ADC_CDR7; break;
  }
  if (result >1000)   result++;
  return result;
} 


unsigned char ReadSwitches(void)
{
    unsigned char result=0;
    int i;
    for (i=4;i<8;i++)
    {
        result<<=1;
        if (GetAdcChanel(i)<550)
          result|=1;
    }
    return result;
}

unsigned char SDCardDetect(void)
{
  if ((AT91C_BASE_PIOA->PIO_PDSR & SDCARD_DETECT)) 
    return 0;
  else 
    return 1;


}

void SDCardPower(unsigned char action)
{
	if (action ==1)   
		AT91C_BASE_PIOA->PIO_SODR = SDCARD_POWER;
	else 
		AT91C_BASE_PIOA->PIO_CODR = SDCARD_POWER;
}

void LedSwitch(unsigned char number, unsigned char action)
{
	//AT91PS_PIO    m_pPio   = AT91C_BASE_PIOA;
	 //param aktion 0 = aus, 1 = an, 2 = toogle

	switch (number)
	{
		case 0:
			if (action ==0)   AT91C_BASE_PIOA->PIO_SODR = LED1;
			else if (action ==1)   AT91C_BASE_PIOA->PIO_CODR = LED1;
			else
			{
				  if  ((AT91C_BASE_PIOA->PIO_ODSR & LED1) == LED1) AT91C_BASE_PIOA->PIO_CODR = LED1;
				  else 	AT91C_BASE_PIOA->PIO_SODR = LED1;
			}
			break;
		case 1:
			if (action ==0)   AT91C_BASE_PIOA->PIO_SODR = LED2;
			else if (action ==1)   AT91C_BASE_PIOA->PIO_CODR = LED2;
			else
			{
				  if  ((AT91C_BASE_PIOA->PIO_ODSR & LED2) == LED2) AT91C_BASE_PIOA->PIO_CODR = LED2;
				  else 	AT91C_BASE_PIOA->PIO_SODR = LED2;
			}
			break;
		case 2:
			if (action ==0)   AT91C_BASE_PIOA->PIO_SODR = LED3;
			else if (action ==1)   AT91C_BASE_PIOA->PIO_CODR = LED3;
			else
			{
				  if  ((AT91C_BASE_PIOA->PIO_ODSR & LED3) == LED3) AT91C_BASE_PIOA->PIO_CODR = LED3;
				  else 	AT91C_BASE_PIOA->PIO_SODR = LED3;
			}
			break;
		case 3:
			if (action ==0)   AT91C_BASE_PIOA->PIO_SODR = LED4;
			else if (action ==1)   AT91C_BASE_PIOA->PIO_CODR = LED4;
			else
			{
				  if  ((AT91C_BASE_PIOA->PIO_ODSR & LED4) == LED4) AT91C_BASE_PIOA->PIO_CODR = LED4;
				  else 	AT91C_BASE_PIOA->PIO_SODR = LED4;
			}
			break;
	}

}

InitAIC(void)
{

    AT91C_BASE_PMC->PMC_PCER = ((unsigned int) 1 << AT91C_ID_PIOA   )    ;    // enable PIOA perip clock    
    AT91C_BASE_AIC->AIC_EOICR = AT91C_BASE_AIC->AIC_EOICR;  // clear the AIC stack (???)
    
    AT91C_BASE_AIC->AIC_DCR = 1;   // put the AIC in protect mode (so debug reads don't trigger action)
    
    // initialize the PIT
    

    AT91C_BASE_PITC->PITC_PIMR =0xFFFFFF;
    AT91C_BASE_PITC->PITC_PIMR |= AT91C_PITC_PITEN;   
    AT91C_BASE_PITC->PITC_PIMR |= AT91C_PITC_PITIEN;    // enable the PIT interrupt
    

    AT91C_BASE_AIC->AIC_IDCR = (0x1 << AT91C_ID_SYS) ;       // disable the system interrupt
    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_SYS] = (unsigned int) pitc_handler ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_SYS] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE | AT91C_AIC_PRIOR_HIGHEST  ;    // set mode, priority
    AT91C_BASE_AIC->AIC_ICCR = (0x1 << AT91C_ID_SYS) ;   // clear the system interrupt
    AT91C_BASE_AIC->AIC_IECR = 1 << AT91C_ID_SYS;   // enable the system interrupt

    

     // Setup TC0
    AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_TC0;   // Enable clock to TC0
    AT91C_BASE_TC0->TC_CMR = AT91C_TC_CLKS_TIMER_DIV5_CLOCK | AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO;    // Prescaler set to 1024, Compare mode on RC
    AT91C_BASE_TC0->TC_RC = TIME_PERIOD;   // Compare value of RC
    AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKEN; // Enable timer

    // Setup interrupts
    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_TC0] = (unsigned long) ISR_Timer0 ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_TC0] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE | INT_LEVEL_TIMER  ;    // set mode, priority

    
    AT91C_BASE_TC0->TC_IER  = AT91C_TC_CPCS;  //  IRQ enable CPC
    AT91C_BASE_AIC->AIC_IECR = 1 << AT91C_ID_TC0;   // Enable interrupt in AIC

    AT91C_BASE_TC0->TC_CCR = AT91C_TC_SWTRG;    // Start timer


    AT91C_BASE_PIOA->PIO_PER |= MRF24WB0_INT;   //Enable
    AT91C_BASE_PIOA->PIO_ODR |= MRF24WB0_INT;   //Input
    AT91C_BASE_PIOA->PIO_PPUER |= MRF24WB0_INT;
    AT91C_BASE_PIOA->PIO_IER = MRF24WB0_INT;   //Int Enable
    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_PIOA] = (unsigned long) ISR_Pio ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_PIOA] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE | INT_LEVEL_TIMER  ;    // set mode, priority
    AT91C_BASE_AIC->AIC_IECR = 1 << AT91C_ID_PIOA;

    //Uart 0
    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_US0] = (unsigned long) ISR_Uart0 ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_US0] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE |7;
    AT91C_BASE_AIC->AIC_IECR = 0x1 << AT91C_ID_US0;
    //Uart 1
    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_US1] = (unsigned long) ISR_Uart1 ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_US1] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE |7;
    AT91C_BASE_AIC->AIC_IECR = 0x1 << AT91C_ID_US1;

    //Uart D         
   
    //USB
//    AT91C_BASE_AIC->AIC_SVR[AT91C_ID_UDP] = (unsigned long) ISR_Usb ; // point vector to function
//    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_UDP] = AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE | 5;
//    AT91C_BASE_AIC->AIC_IECR = 0x1 << AT91C_ID_UDP;

    //SPI
  /*  AT91C_BASE_AIC->AIC_SVR[AT91C_ID_SPI] = (unsigned long) ISR_SPI ; // point vector to function
    AT91C_BASE_AIC->AIC_SMR[AT91C_ID_SPI] = AT91C_AIC_SRCTYPE_EXT_POSITIVE_EDGE |6;
    AT91C_BASE_AIC->AIC_IECR = 0x1 << AT91C_ID_SPI;
*/

    

}


void InitHardware(void)
{
        
	//InitCPU();  //PLL,DIV,WD,FLASH,...
        InitCPU2();  //PLL,DIV,WD,FLASH,...
        InitSPI();

	InitLEDs();
	InitPowerSwitches();
	InitLcdPins();
        InitADC();
	Lcd_Init();
       
// USB 
        AT91C_BASE_PIOA->PIO_PER  |=USB_PUP;
        AT91C_BASE_PIOA->PIO_OER |= USB_PUP;
        AT91C_BASE_PIOA->PIO_SODR |=USB_PUP;   //set reg to 1


        InitUARTs();

        mck=ctl_at91sam7_get_mck_frequency(18432000);
  
        AT91C_BASE_CKGR->CKGR_PLLR |= (1<<28);

        InitAIC();
	return;
}


void XbeeSleep(bool sleep)
{
  if (sleep)
    AT91C_BASE_PIOA->PIO_SODR |=MRF24J40_RESET;   //set reg to 1
  else 
    AT91C_BASE_PIOA->PIO_CODR |=MRF24J40_RESET;   //set reg to 1
}

void UsbPup(bool e)
{
  if (e)
    AT91C_BASE_PIOA->PIO_SODR |=USB_PUP;   //set reg to 1
  else 
    AT91C_BASE_PIOA->PIO_CODR |=USB_PUP;   //set reg to 1
 

}