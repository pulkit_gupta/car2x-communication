#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>

//#define PACKAGEING
#define CHAR_DEBUG_START  0xFD
#define CHAR_DEBUG_END    0xFF
#define DEBUGUART 1


//#define DEBUG

void debug_out(const char *s, ...)
{
#ifdef DEBUG
  unsigned int i, move;
  char c, str[16], *ptr;
  va_list ap;

  va_start(ap, s);
#ifdef PACKAGEING
  debugPutChar(CHAR_DEBUG_START);
#endif
  for(;;)
  {
    c = *s++;

    if(c == 0)
    {
      break;
    }
    else if(c == '%')
    {
      c = *s++;
      if(isdigit(c) > 0)
      {
        move = c-'0';
        c = *s++;
      }
      else
      {
        move = 0;
      }

      switch(c)
      {
        case 's':
          ptr = va_arg(ap, char *);
          debugPutString(ptr);
          break;
        case 'b': //bin
          ltoa(va_arg(ap, long), str, 2);
          if(move)
          {
            for(i=0; str[i]; i++);
            for(; move>i; move--)
            {
              debugPutChar('0');
            }
          }
          debugPutString(str);
          break;
        case 'i': //dec
          ltoa(va_arg(ap, long), str, 10);
          if(move)
          {
            for(i=0; str[i]; i++);
            for(; move>i; move--)
            {
              debugPutChar('0');
            }
          }
          debugPutString(str);
          break;
        case 'u': //unsigned dec
          ultoa(va_arg(ap, unsigned long), str, 10);
          if(move)
          {
            for(i=0; str[i]; i++);
            for(; move>i; move--)
            {
              debugPutChar('0');
            }
          }
          debugPutString(str);
          break;
        case 'x': //hex
          ltoa(va_arg(ap, long), str, 16);
          if(move)
          {
            for(i=0; str[i]; i++);
            for(; move>i; move--)
            {
              debugPutChar('0');
            }
          }
          debugPutString(str);
          break;
      }
    }
    else
    {
      debugPutChar(c);
    }
  }
#ifdef PACKAGEING
  debugPutChar(CHAR_DEBUG_END);
#endif
  va_end(ap);
#endif //Debug
  return;
}


void debugPutString(const char *s)
{
  while(*s)
  {
    debugPutChar(*s++);
  }

  return;
}


void debugPutChar(unsigned int c)
{
  UartPutChar(DEBUGUART, c);

  return;
}

int __putchar(int c)
{
  UartPutChar(DEBUGUART, c);
}