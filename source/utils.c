//#include "at91sam7s128_.h"
#include "AT91SAM7S256.h"
#include "utils.h"


unsigned long ctl_at91sam7_get_mck_frequency(unsigned long mainck_frequency)
{
  unsigned long result = 0;
  switch (AT91C_BASE_PMC->PMC_MCKR & 3)
    {
      case 0:
        result = 32768;
        break;
      case 1:
        result = mainck_frequency;
        break;
      case 2:
      case 3:
        result = mainck_frequency * (((AT91C_BASE_CKGR->CKGR_PLLR >> 16) & 0x7FF) + 1) / (AT91C_BASE_CKGR->CKGR_PLLR & 0xFF);
        break;        
    }
  return result / (1 << ((AT91C_BASE_PMC->PMC_MCKR >> 2) & 7));
}


