
#include "hardware.h"
#include "spi.h"

/* SPI Instructions */
#define SPI_WRSR        0x01
#define SPI_WRITE       0x02
#define SPI_READ        0x03
#define SPI_WRDI        0x04
#define SPI_RDSR        0x05
#define SPI_WREN        0x06

/* Status Register Bits */
#define SR_NRDY         0x01
#define SR_WIP          0x01
#define SR_WEN          0x02
#define SR_BP0          0x04
#define SR_BP1          0x08
#define SR_WPEN         0x80

#define PAGE_SZ         16





void SPI_MemRead (unsigned long address, unsigned long size, unsigned char *buffer) 
{
  unsigned int i;
  SPI_SelectDevice(SPI_EEPROM);
  spiTransferByte(SPI_READ);
  spiTransferByte((unsigned char)((address >> 8)&0xFF));
  spiTransferByte((unsigned char)((address)&0xFF));

  for (i = 0; i < size; i++) 
  {
    *buffer++ = spiTransferByte(0x00);
  }
  SPI_UnselectDevice();   
}



void SPI_MemWrite (unsigned long address, unsigned long size, unsigned char *buffer) {
  unsigned char sr;
  unsigned int  i, n;

  while (size) {

    n = PAGE_SZ - (address & (PAGE_SZ - 1));
    if (size < n) n = size;

    SPI_SelectDevice(SPI_EEPROM);
    spiTransferByte(SPI_WREN);
    SPI_UnselectDevice();   
    
    SPI_SelectDevice(SPI_EEPROM);
    spiTransferByte(SPI_WRITE);
    spiTransferByte((unsigned char)((address >> 8)&0xFF));
    spiTransferByte((unsigned char)((address)&0xFF));
    
    for (i = 0; i < n; i++) {
      spiTransferByte(*buffer++);      
    }
    SPI_UnselectDevice(); 

    do {
      SPI_SelectDevice(SPI_EEPROM);
      spiTransferByte(SPI_RDSR);
      sr = spiTransferByte(0xFF);
      SPI_UnselectDevice();   
    } while (sr & SR_NRDY);

    address += n;
    size  -= n;

  }

}

