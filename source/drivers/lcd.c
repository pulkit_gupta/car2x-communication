/*
 * lcd.c
 *
 *  Created on: 10.09.2008
 *      Author: limir
 */
#include "hardware.h"
#include "lcd.h"
#include "typedefs.h"
#include "AT91SAM7S256.h"

void LCD_Backlight(unsigned char value)
{
  if (value==0)
        AT91C_BASE_PIOA->PIO_CODR = LCD_BACKLIGHT;   //set reg to 0
  else
        AT91C_BASE_PIOA->PIO_SODR = LCD_BACKLIGHT;   //set reg to 0
}

void LCD_Delay(unsigned long value)
{
  int i;
        //for (i=0;i<100;i++)
          Delay (value<<1);
}

//Write Data to Port (only low nibble)
void LcdWriteDataParts(unsigned char data)
{
	if (data&0x01)  
          AT91C_BASE_PIOA->PIO_SODR = LCD_D4;
	else			
          AT91C_BASE_PIOA->PIO_CODR = LCD_D4;

	if (data&0x02)  
          AT91C_BASE_PIOA->PIO_SODR = LCD_D5;
	else			
          AT91C_BASE_PIOA->PIO_CODR = LCD_D5;

	if (data&0x04)  
          AT91C_BASE_PIOA->PIO_SODR = LCD_D6;
	else			
          AT91C_BASE_PIOA->PIO_CODR = LCD_D6;

	if (data&0x08)  
          AT91C_BASE_PIOA->PIO_SODR = LCD_D7;
	else			
          AT91C_BASE_PIOA->PIO_CODR = LCD_D7;

	//evtl auch einfacher mit schieben und verunden, da D4-D7
	//aufeinanderfolgend und zusammenliegend sind
}

void LcdWriteDataHighNibble(unsigned char data)
{
	LcdWriteDataParts(data>>4);
}

void LcdWriteDataLowNibble(unsigned char data)
{
	LcdWriteDataParts(data & 0x0F);
}

void LcdEnable(void)
{
	//Takt
	LCD_Delay(1000);
	AT91C_BASE_PIOA->PIO_SODR = LCD_E;
	//wartezeig
	LCD_Delay(2000);

	//wieder low
	AT91C_BASE_PIOA->PIO_CODR = LCD_E;
	LCD_Delay(1000);
}


void LCD_Write_Command_8(unsigned char command)
{
        
        LcdWriteDataHighNibble(command);
	//LcdWriteDataParts(command>>4);
	AT91C_BASE_PIOA->PIO_CODR = LCD_RS; 	//0=Command 1=data
	LcdEnable();
}

void LCD_Write_Data(unsigned char data)
{
	AT91C_BASE_PIOA->PIO_SODR = LCD_RS; 	//0=Command 1=data
	LcdWriteDataHighNibble(data);
	LcdEnable();
	AT91C_BASE_PIOA->PIO_SODR = LCD_RS; 	//0=Command 1=data
	LcdWriteDataLowNibble(data);
	LcdEnable();
}

void LCD_Write_Command(unsigned char command)
{

        AT91C_BASE_PIOA->PIO_CODR = LCD_RS; 	//0=Command 1=data
	LcdWriteDataHighNibble(command);
	LcdEnable();
	AT91C_BASE_PIOA->PIO_CODR = LCD_RS; 	//0=Command 1=data
	LcdWriteDataLowNibble(command);
	LcdEnable();
}

void LCD_Pos(unsigned char row,unsigned char col)
{
    if (row>4 || col>20) return;
    int abspos=0;
    switch (row)
    {
      case 1:
        abspos=LCD_ROW_OFFSET_0; break;
      case 2:
        abspos=LCD_ROW_OFFSET_1; break;
      case 3:
        abspos=LCD_ROW_OFFSET_2; break;
      case 4:
        abspos=LCD_ROW_OFFSET_3; break;
      default:
        return;
    }
    LCD_Write_Command(SET_DD_RAM + (abspos+col-1));

}


void LCD_WriteText(unsigned char* s)
{

while(*s)
  {
      LcdWriteData(*s++);
  }
}

void LCD_WriteTextPos(unsigned char* text, unsigned char row,unsigned char col)
{
  LCD_Pos(row,col);
  LCD_WriteText(text);
}



void LCD_Scroll()
{

}


void LCD_Text(const char* text)
{
/*
	while (*text!='\0')
	{
		LCD_Write_Data(*text);
		text++;
	}
*/
}

void LcdWriteCharPos(unsigned char c, unsigned char row,unsigned char col)
{
  LCD_Pos(row,col);
  LcdWriteData(c);
}

void LcdWriteData(unsigned char data)
{
	AT91C_BASE_PIOA->PIO_SODR = LCD_RS;	//0=Command 1=data
	LcdWriteDataHighNibble(data);
	LcdEnable();
	LcdWriteDataLowNibble(data);
	LcdEnable();
}

void LcdWriteCommand(unsigned char command)
{
	AT91C_BASE_PIOA->PIO_CODR = LCD_RS;	//0=Command 1=data
	LcdWriteDataHighNibble(command);
	LcdEnable();
	LcdWriteDataLowNibble(command);
	LcdEnable();
	AT91C_BASE_PIOA->PIO_SODR = LCD_RS;
}

void LCD_WriteHexChar(unsigned char hexchar)
{
    if (hexchar<=9)
      LCD_Write_Data(hexchar + '0');
    else
      LCD_Write_Data(hexchar + 55);   // 'A'-10
}
void LCD_WriteInt(unsigned long data,unsigned char mincharlen,unsigned char row, unsigned char col)
{
    int i=0;
    if (row!=0 && col != 0)
      LCD_Pos(row,col);
    unsigned char tmp[12];
    while (data!=0)
    {
      tmp[i]=(data%10);
      data=data/10;
      i++;
    }
    //f�hrende Nullen
    for(data=i;data<mincharlen;data++)
        LCD_WriteHexChar(0);
    //Ziffern
    for (;i>0;i--)
        {
          LCD_WriteHexChar(tmp[i-1]);
        }
}

void LCD_WriteHex(unsigned long data,unsigned char mincharlen,unsigned char row, unsigned char col)
{
    int i;
    unsigned char tmp[8];
    LCD_Pos(row,col);
    
    for (i=0;i<mincharlen;i++)
    {
      tmp[i] = (data & 0x0F);
      data>>=4;
    }
    for (i=1;i<=mincharlen;i++)
    LCD_WriteHexChar(tmp[mincharlen-i]);
}

void LoadCG(unsigned char pos,unsigned char* data)
{
  int i;
  LCD_Write_Command(64+(pos*8));LCD_Delay(1000);
  for (i=0;i<8;i++)
    LcdWriteData(data[i]);
  LCD_Write_Command(RETURN_HOME);LCD_Delay(1000);
}


void Lcd_Init(void)
{
        LCD_Backlight(1);
	//4bit Modus
        LCD_Write_Command_8(SYSTEM_SET_8_BIT);LCD_Delay(10000);
        LCD_Write_Command_8(SYSTEM_SET_8_BIT);LCD_Delay(1000);
        LCD_Write_Command_8(SYSTEM_SET_8_BIT);LCD_Delay(1000);
 	LCD_Write_Command_8(SYSTEM_SET_4_BIT);LCD_Delay(1000);
 	//LCD_Write_Command_8(system_set_4_bit);LCD_Delay(100);
        LCD_Write_Command(SYSTEM_SET_4_BIT);LCD_Delay(1000);
        LCD_Write_Command(SYSTEM_SET_4_BIT);LCD_Delay(1000);
        LCD_Write_Command(DISPLAY_OFF);LCD_Delay(1000);
        LCD_Write_Command(CLEAR_LCD);LCD_Delay(1000);
        LCD_Write_Command(RETURN_HOME);LCD_Delay(1000);
        LCD_Write_Command(DISPLAY_ON);LCD_Delay(1000);

/*        
        LcdWriteData(00);LCD_Delay(1000);
0x00,0x00,0x0A,0x15,0x11,0x0A,0x04,0x00
*/

  unsigned char mychar0[] = {0x00,0x00,0x0A,0x1F,0x1F,0x0E,0x04,0x00};
  LoadCG(0,mychar0);
  unsigned char mychar1[] = {0x00,0x00,0x0A,0x15,0x11,0x0A,0x04,0x00};
  LoadCG(1,mychar1);

  // :)
  unsigned char mychar2[] = {0x00,0x00,0x0A,0x00,0x11,0x0E,0x00,0x00};
  LoadCG(2,mychar2);
  //  )
  unsigned char mychar3[] = {0x00,0x00,0x00,0x00,0x11,0x0E,0x00,0x00};
  LoadCG(3,mychar3);
  
  //SD
  unsigned char mychar4[] = {0x1C,0x12,0x11,0x11,0x11,0x11,0x11,0x1F};
  LoadCG(4,mychar4);

  //frei
  unsigned char mychar5[] = {0x11,0x19,0x1D,0x19,0x13,0x17,0x13,0x11 };
  LoadCG(5,mychar5);

  //Bluetooth
 unsigned char mychar6[] = {0x00,0x06,0x15,0x0E,0x04,0x0E,0x15,0x06};
  LoadCG(6,mychar6);

  //Funk
   unsigned char mychar7[] = {0x11,0x04,0x00,0x04,0x04,0x0E,0x0A,0x11};
  LoadCG(7,mychar7);


LCD_Write_Command(RETURN_HOME);LCD_Delay(1000);



}
