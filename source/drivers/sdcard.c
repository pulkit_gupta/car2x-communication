/*-----------------------------------------------------------------------*/
/* MMC/SDC (in SPI mode) control module  (C)ChaN, 2006                   */
/*-----------------------------------------------------------------------*/

//#include <stdio.h>

/* AT91-port by Martin Thomas */

#include "fs/diskio.h"
#include "AT91SAM7S256.h"
#include "hardware.h"
#include "typedefs.h"

//#include "./fs/ff.h" /* for _BYTE_ACC */

#define SPI_CSR_NUM      0
#define SPI_SCBR_MIN     2

/* Definitions for MMC/SDC command */
#define CMD0	(0x40+0)	/* GO_IDLE_STATE */
#define CMD1	(0x40+1)	/* SEND_OP_COND (MMC) */
#define	ACMD41	(0xC0+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(0x40+8)	/* SEND_IF_COND */
#define CMD9	(0x40+9)	/* SEND_CSD */
#define CMD10	(0x40+10)	/* SEND_CID */
#define CMD12	(0x40+12)	/* STOP_TRANSMISSION */
#define ACMD13	(0xC0+13)	/* SD_STATUS (SDC) */
#define CMD16	(0x40+16)	/* SET_BLOCKLEN */
#define CMD17	(0x40+17)	/* READ_SINGLE_BLOCK */
#define CMD18	(0x40+18)	/* READ_MULTIPLE_BLOCK */
#define CMD23	(0x40+23)	/* SET_BLOCK_COUNT (MMC) */
#define	ACMD23	(0xC0+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(0x40+24)	/* WRITE_BLOCK */
#define CMD25	(0x40+25)	/* WRITE_MULTIPLE_BLOCK */
#define CMD55	(0x40+55)	/* APP_CMD */
#define CMD58	(0x40+58)	/* READ_OCR */

static
UINT CardType;			/* b0:MMC, b1:SDC, b2:Block addressing */


static volatile
DSTATUS Stat = STA_NOINIT;	/* Disk status */

// AT91: thru systime
static volatile
 BYTE Timer;			/* 100Hz decrement timer */



/*--------------------------------*/
/* Transmit a byte to MMC via SPI */
/* (Platform dependent)           */

static 
void xmit_spi(BYTE dat)
{
	spiTransferByte(dat);
}

/*---------------------------------*/
/* Receive a byte from MMC via SPI */
/* (Platform dependent)            */

static
BYTE rcvr_spi(void)
{
	return spiTransferByte(0xff);
}

/* Alternative "macro" (not at AT91 so far) to receive data fast */
static 
void rcvr_spi_m(BYTE *dst)
{
	*dst = rcvr_spi();
}


/*---------------------*/
/* Wait for card ready */

static
BYTE wait_ready ()
{
	BYTE res;
	
	Timer = 50;			/* Wait for ready in timeout of 500ms */
	rcvr_spi();
	do
		res = rcvr_spi();
	while ((res != 0xFF) && Timer);
	return res;
}

/*--------------------------------*/
/* Receive a data packet from MMC */

static
bool rcvr_datablock (
	BYTE *buff,			/* Data buffer to store received data */
	BYTE wc				/* Word count (0 means 256 words) */
)
{
	BYTE token;

	Timer = 10;
	do {							/* Wait for data packet in timeout of 100ms */
		token = rcvr_spi();
	} while ((token == 0xFF) && Timer );
	if(token != 0xFE) return FALSE;	/* If not valid data token, retutn with error */

	do {							/* Receive the data block into buffer */
		rcvr_spi_m(buff++);
		rcvr_spi_m(buff++);
	} while (--wc);
	rcvr_spi();						/* Discard CRC */
	rcvr_spi();

	return TRUE;					/* Return with success */
}



/*---------------------------*/
/* Send a data packet to MMC */

#if	_READONLY == 0
static
bool xmit_datablock (
	const BYTE *buff,	/* 512 byte data block to be transmitted */
	BYTE token			/* Data/Stop token */
)
{
	BYTE resp, wc = 0;


	if (wait_ready() != 0xFF) return FALSE;

	xmit_spi(token);					/* Xmit data token */
	if (token != 0xFD) {	/* Is data token */
		do {							/* Xmit the 512 byte data block to MMC */
			xmit_spi(*buff++);
			xmit_spi(*buff++);
		} while (--wc);
		xmit_spi(0xFF);					/* CRC (Dummy) */
		xmit_spi(0xFF);
		resp = rcvr_spi();				/* Reveive data response */
		if ((resp & 0x1F) != 0x05)		/* If not accepted, return with error */
			return FALSE;
	}

	return TRUE;
}
#endif


/*------------------------------*/
/* Send a command packet to MMC */

static
BYTE send_cmd (
	BYTE cmd,		/* Command byte */
	DWORD arg		/* Argument */
)
{
	BYTE n, res;


	if (wait_ready() != 0xFF) return 0xFF;

	/* Send command packet */
	spiTransferByte(cmd);						/* Command */
	spiTransferByte((BYTE)(arg >> 24));		/* Argument[31..24] */
	spiTransferByte((BYTE)(arg >> 16));		/* Argument[23..16] */
	spiTransferByte((BYTE)(arg >> 8));			/* Argument[15..8] */
	spiTransferByte((BYTE)arg);				/* Argument[7..0] */
	spiTransferByte(0x95);						/* CRC (valid for only CMD0) */

	/* Receive command response */
	if (cmd == CMD12) spiTransferByte();		/* Skip a stuff byte when stop reading */
	n = 10;								/* Wait for a valid response in timeout of 10 attempts */
	do
		res = spiTransferByte();
	while ((res & 0x80) && --n);
        
        //TEST//
        
	return res;			/* Return with the response value */
}




/*-----------------------------------------------------------------------*/
/* Public Functions                                                      */

/*-----------------------*/
/* Shutdown              */
/* (Platform dependent)  */



DSTATUS MMC_disk_shutdown ()
{
	return 0;
}


/*--------------------*/
/* Return Disk Status */

DSTATUS MMC_disk_status ()
{
if (SDCardDetect()) return TRUE;
else return FALSE;
        //return Stat;
}


/*-----------------------*/
/* Initialize Disk Drive */
/* (Platform dependent)  */

DSTATUS MMC_disk_initialize ()
{
	BYTE n;

	AT91PS_PMC pPMC      = AT91C_BASE_PMC;

	SDCardPower(1);					/* Socket power ON */
	//for (Timer = 3; Timer; );	/* Wait for 30ms */		
		
        Delay(300);
	/* MMC socket-switch init */
	// disable internal Pull-Ups if needed
        
	Stat |= STA_NOINIT;
	if (!(Stat & STA_NODISK)) {
		n = 100;						/* Dummy clock */
		do
                
                     //spiDummyByte(0xFF);
                      rcvr_spi();
		while (--n);

		//iprintf("SPI prepare done\n");

        SPI_SelectDevice(SPI_SDCARD);
//	SELECT();			/* CS = L */
		if (send_cmd(CMD0, 0) == 1) {			/* Enter Idle state */
			Timer = 100;						/* Wait for card ready in timeout of 1 sec */
			while (Timer && send_cmd(CMD1, 0));
			if (Timer) Stat &= ~STA_NOINIT;		/* When device goes ready, clear STA_NOINIT */
		}
		
		//iprintf("CMD0 done timer = %i\n", Timer);
		
		//DESELECT();			/* CS = H */
                SPI_UnselectDevice(SPI_SDCARD);
		rcvr_spi();			/* Idle (Release DO) */
		
		//AT91_spiSetSpeed(SPI_SCBR_MIN); 
	}

	if (Stat & STA_NOINIT)
        {
		MMC_disk_shutdown();
                return FALSE;
        }
        return TRUE;
	//return Stat;
}



/*----------------*/
/* Read Sector(s) */

DRESULT MMC_disk_read (
	BYTE *buff,			/* Data buffer to store read data */
	DWORD sector,		/* Sector number (LBA) */
	BYTE count			/* Sector count (1..255) */
)
{
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	if (!count) return RES_PARERR;

	sector *= 512;		/* LBA --> byte address */

	//SELECT();			/* CS = L */
        SPI_SelectDevice(SPI_SDCARD);

	if (count == 1) {	/* Single block read */
		if ((send_cmd(CMD17, sector) == 0)	/* READ_SINGLE_BLOCK */
			&& rcvr_datablock(buff, (BYTE)(512/2)))
			count = 0;
	}
	else {				/* Multiple block read */
		if (send_cmd(CMD18, sector) == 0) {	/* READ_MULTIPLE_BLOCK */
			do {
				if (!rcvr_datablock(buff, (BYTE)(512/2))) break;
				buff += 512;
			} while (--count);
			send_cmd(CMD12, 0);				/* STOP_TRANSMISSION */
		}
	}

	//DESELECT();			/* CS = H */
        SPI_UnselectDevice(SPI_SDCARD);
	rcvr_spi();			/* Idle (Release DO) */

	return count ? RES_ERROR : RES_OK;
}



/*-----------------*/
/* Write Sector(s) */

#if	_READONLY == 0
DRESULT MMC_disk_write (
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector number (LBA) */
	BYTE count			/* Sector count (1..255) */
)
{
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	if (Stat & STA_PROTECT) return RES_WRPRT;
	if (!count) return RES_PARERR;
	sector *= 512;		/* LBA --> byte address */

	//SELECT();			/* CS = L */
        SPI_SelectDevice(SPI_SDCARD);

	if (count == 1) {	/* Single block write */
		if ((send_cmd(CMD24, sector) == 0)	/* WRITE_BLOCK */
			&& xmit_datablock(buff, 0xFE))
			count = 0;
	}
	else {				/* Multiple block write */
		if (send_cmd(CMD25, sector) == 0) {	/* WRITE_MULTIPLE_BLOCK */
			do {
				if (!xmit_datablock(buff, 0xFC)) break;
				buff += 512;
			} while (--count);
			if (!xmit_datablock(0, 0xFD))	/* STOP_TRAN token */
				count = 1;
		}
	}

	//DESELECT();			/* CS = H */
        SPI_UnselectDevice(SPI_SDCARD);
	rcvr_spi();			/* Idle (Release DO) */

	return count ? RES_ERROR : RES_OK;
}
#endif


#if _USE_IOCTL != 0
DRESULT MMC_disk_ioctl (
	BYTE ctrl,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	BYTE n, csd[16], *ptr = buff;
	WORD csize;

        res = RES_ERROR;

	if (ctrl == CTRL_POWER) {
		switch (*ptr) {
		case 0:		/* Sub control code == 0 (POWER_OFF) */
			SDCardPower(0);
			res = RES_OK;
			break;
		case 1:		/* Sub control code == 1 (POWER_ON) */
			SDCardPower(1);
			res = RES_OK;
			break;
		case 2:		/* Sub control code == 2 (POWER_GET) */
			//*(ptr+1) = (BYTE)chk_power();
			res = RES_OK;
			break;
		default :
			res = RES_PARERR;
		}
	}
	else {
		if (Stat & STA_NOINIT) return RES_NOTRDY;

		switch (ctrl) {
		case CTRL_SYNC :		/* Make sure that no pending write process */
			SPI_SelectDevice(SPI_SDCARD);
			if (wait_ready() == 0xFF)
				res = RES_OK;
			break;

		case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (DWORD) */
			if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {
				if ((csd[0] >> 6) == 1) {	/* SDC ver 2.00 */
					csize = csd[9] + ((WORD)csd[8] << 8) + 1;
					*(DWORD*)buff = (DWORD)csize << 10;
				} else {					/* SDC ver 1.XX or MMC*/
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					csize = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(DWORD*)buff = (DWORD)csize << (n - 9);
				}
				res = RES_OK;
			}
			break;

		case GET_SECTOR_SIZE :	/* Get R/W sector size (WORD) */
			*(WORD*)buff = 512;
			res = RES_OK;
			break;

		case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */
			if (CardType & 4) {			/* SDC ver 2.00 */
				if (send_cmd(ACMD13, 0) == 0) {		/* Read SD status */
					rcvr_spi();
					if (rcvr_datablock(csd, 16)) {				/* Read partial block */
						for (n = 64 - 16; n; n--) rcvr_spi();	/* Purge trailing data */
						*(DWORD*)buff = 16UL << (csd[10] >> 4);
						res = RES_OK;
					}
				}
			} else {					/* SDC ver 1.XX or MMC */
				if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {	/* Read CSD */
					if (CardType & 2) {			/* SDC ver 1.XX */
						*(DWORD*)buff = (((csd[10] & 63) << 1) + ((WORD)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
					} else {					/* MMC */
						*(DWORD*)buff = ((WORD)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
					}
					res = RES_OK;
				}
			}
			break;

		case MMC_GET_TYPE :		/* Get card type flags (1 byte) */
			*ptr = CardType;
			res = RES_OK;
			break;

		case MMC_GET_CSD :		/* Receive CSD as a data block (16 bytes) */
			if (send_cmd(CMD9, 0) == 0		/* READ_CSD */
				&& rcvr_datablock(ptr, 16))
				res = RES_OK;
			break;

		case MMC_GET_CID :		/* Receive CID as a data block (16 bytes) */
			if (send_cmd(CMD10, 0) == 0		/* READ_CID */
				&& rcvr_datablock(ptr, 16))
				res = RES_OK;
			break;

		case MMC_GET_OCR :		/* Receive OCR as an R3 resp (4 bytes) */
			if (send_cmd(CMD58, 0) == 0) {	/* READ_OCR */
				for (n = 4; n; n--) *ptr++ = rcvr_spi();
				res = RES_OK;
			}
			break;

		case MMC_GET_SDSTAT :	/* Receive SD statsu as a data block (64 bytes) */
			if (send_cmd(ACMD13, 0) == 0) {	/* SD_STATUS */
				rcvr_spi();
				if (rcvr_datablock(ptr, 64))
					res = RES_OK;
			}
			break;

		default:
			res = RES_PARERR;
		}

		        SPI_UnselectDevice(SPI_SDCARD);
                        rcvr_spi();			/* Idle (Release DO) */
	}
        SPI_UnselectDevice(SPI_SDCARD);
	return res;
}
#endif /* _USE_IOCTL != 0 */


/*---------------------------------------*/
/* Device timer interrupt procedure      */
/* This must be called in period of 10ms */
/* (Platform dependent)                  */

void disk_timerproc ()
{
	static BYTE pv;
	BYTE n, s;
	
	static DWORD pv_d;
	DWORD n_d;
	
	n = Timer;						/* 100Hz decrement timer */
	if (n) Timer = --n;

	// n = pv;
	// pv = SOCKPORT & (SOCKWP | SOCKINS);	/* Sapmle socket switch */
/*	
	n_d = pv_d;
	pv = pPIOA->PIO_PDSR;
*/
#if 0
	if (n == pv) {					/* Have contacts stabled? */
		s = Stat;

		if (pv & SOCKWP)			/* WP is H (write protected) */
			s |= STA_PROTECT;
		else						/* WP is L (write enabled) */
			s &= ~STA_PROTECT;

		if (pv & SOCKINS)			/* INS = H (Socket empty) */
			s |= (STA_NODISK | STA_NOINIT);
		else						/* INS = L (Card inserted) */
			s &= ~STA_NODISK;

		Stat = s;
	}
#endif
        
}

