/*********************************************************************************

 Copyright 2006-2008 MakingThings

 Licensed under the Apache License, 
 Version 2.0 (the "License"); you may not use this file except in compliance 
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0 
 
 Unless required by applicable law or agreed to in writing, software distributed
 under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied. See the License for
 the specific language governing permissions and limitations under the License.

2011-09-12  limir     added a new function for building a xbee packet with
                      a char as parameter

*********************************************************************************/









//#include "config.h"

//#include "stdlib.h"
//#include <stdio.h>
//#include "string.h"
#include "xbee.h"

#define CONTROLLER_OK 1

static bool XBee_GetIOValues( XBeePacket* packet, int *inputs );
void XBeeTask( void* p );
#define XBEEPACKET_Q_SIZE 5
#define XBEE_OSC_RX_TIMEOUT 500

typedef struct
{
  XBeePacket* currentPkt;
  int packetIndex;
  #ifdef OSC
  bool autosend;
  #endif
  bool waitingForConfirm;
} XBeeSubsystem;

XBeeSubsystem* XBee;

int Serial_GetReadable(unsigned char device)
{
  //if (AT91C_BASE_DBGU->DBGU_CSR & AT91C_US_RXRDY) return 1;
  return 0;
}

int Serial_SetChar(unsigned char device, int character )
{
  UartPutChar(device,character);
  return 0;
}

int Serial_GetChar(unsigned char device)
{
  return uart_getc_queue(device);
}

int Serial_Write (unsigned char device,unsigned char* buffer, unsigned int count, unsigned int timeout )
{

  return uart_putbuf(device,buffer,count);
}


int Serial_Read( unsigned char device,unsigned char* buffer, unsigned int count, unsigned int timeout )
{
  return 0;
}



/** \defgroup XBee XBee
	Communicate with XBee (Zigbee) wireless modules via the Make Controller's serial port.
  
  XBee modules from \b MaxStream are small, cheap ($19 each), wireless \b RF (radio frequency) modules that
  can easily be used with the Make Controller Kit to create projects that require wireless communication.  Check
  http://www.maxstream.net/products/xbee/xbee-oem-rf-module-zigbee.php for more info.
  
  \section Overview
  XBee modules are <b>ZigBee/IEEE 802.15.4</b> compliant and can operate in several modes:
  - Transparent serial port.  Messages in one side magically end up at the other endpoint.  Great for enabling wireless
  communication between 2 Make Controllers.
  - AT command mode.  Send traditional AT commands to configure the module itself (as opposed to having
  the data go straight through via serial mode.
  - Packet (API) mode.  Lower level communication that doesn't have to wait for the module to be in
  AT command mode.  Check the \ref XBeePacketTypes for a description of how these packets are laid out.  
  Be sure to call XBeeConfig_SetPacketApiMode before trying to do anything in this mode.

  The general idea is that you have one XBee module connected directly to your Make Controller Kit, and then
  any number of other XBee modules that can communicate with it in order to get information to and from the
  Make Controller.  Or, several Make Controllers can each have an XBee module connected in order to
  communicate wirelessly among themselves.

  XBee modules also have some digital and analog I/O right on them, which means you can directly connect
  sensors to the XBee modules which will both read the values and send them wirelessly.  Check the XBee doc
  for the appropriate commands to send in order to set this up.

  \section API
  The Make Controller API for working with the XBee modules makes use of the XBee Packet API.  If you simply want to make use
  of the transparent serial port functionality, you can use the following functions:
    XBee_SetActive( )
    XBee_Write(  );
    XBee_Read( );
    XBee_GetReadable( );

  Or if you want to handle setup etc. yourself, you don't need to deal with these - just hook up the module to your 
  Make Controller and start reading and writing over the serial port.

  Bytes sent in this way are broadcast to all XBee modules on the same chanel and with the same PAN ID.  All similarly
  configured modules will receive all bytes sent.  However, because these bytes are broadcast, there is no message
  reliability, so there's no guarantee that the messages will actually get there.

  The XBee Packet API allows for much more flexible and powerful communication with the modules.  With the Packet API
  you can send messages to a specific module, detect where messages came from, check signal strength, and most importantly
  packets are sent with a send / acknowledges / retry scheme which greatly increases message reliability.
  
  Packet API uses commands to configure the XBee module itself, and then a handful of Zigbee specified packet types can be sent and received.  
  See \ref XBeePacketTypes for details on these packet types.  
	
	The \b XBeeConfig_ functions are convenient wrappers around some of the most common AT commands you might want to send.  For
	any of the other AT commands, check the XBee documentation and create them using XBee_CreateATCommandPacket( ).  These will always 
	be sent to the XBee module attached to the Make Controller.  The \b XBee_ functions deal with sending and receiving 
	messages to other XBee modules not connected to the Make Controller.

  \ingroup Libraries
  @{
*/

/**	
  Controls the active state of the \b XBee subsystem
  @param state Whether this subsystem is active or not
	@return Zero on success.
*/

int XBee_SetActive( int state )
{

}

/**	
  Read the active state of the \b XBee subsystem
	@return An integer specifying the active state - 1 (active) or 0 (inactive).
*/
int XBee_GetActive( )
{
  //return Serial_GetActive( );
  return 1;
}


/**	
  Write the specified number of bytes into the XBee unit.  It is
  assumed that the unit is in TRANSPARENT, not in PACKET API mode.  To write to the 
  unit using packets first set packet mode (using XBeeConfig_SetPacketApiMode) then use
  XBee_CreateXXXPacket() followed by XBee_SendPacket().
	@param buffer The block of bytes to write
  @param count The number of bytes to write
  @param timeout The time in ms to linger waiting to succeed (0 for no wait)
  @return status
*/
int XBee_Write(uchar device, uchar *buffer, int count, int timeout )
{
  return Serial_Write(device, buffer, count, timeout );
}

/**	
	Read data from the Xbee unit waiting for the specified time. Use XBee_GetReadable() to 
  determine how many bytes are waiting to avoid waiting.    It is
  assumed that the unit is in TRANSPARENT, not in PACKET API mode.  To write to the 
  unit using packets first set packet mode (using XBeeConfig_SetPacketApiMode) then use
  XBee_CreateXXXPacket() followed by XBee_SendPacket().
	@param buffer A pointer to the buffer to read into.
	@param count An integer specifying the maximum number of bytes to read.
  @param timeout Time in milliseconds to block waiting for the specified number of bytes. 0 means don't wait.
  @return number of bytes read (>=0) or error <0 .
*/
int XBee_Read(uchar device, uchar* buffer, int count, int timeout )
{
  return Serial_Read(device,  buffer, count, timeout );
}

/**	
	Returns the number of bytes in the queue waiting to be read.
  @return bytes in the receive queue.
*/
int XBee_GetReadable( void );

/**	
  Receive an incoming XBee packet.
  A single call to this will continue to read from the serial port as long as there are characters 
  to read or until it times out.  If a packet has not been completely received, call it repeatedly
  with the same packet.

  Clear out a packet before reading into it with a call to XBee_ResetPacket( )
  @param packet The XBeePacket to receive into.
  @param timeout The number of milliseconds to wait for a packet to arrive.  Set this to 0 to return as
  soon as there are no characters left to process.
	@return 1 if a complete packet has been received, 0 if not.
  @see XBeeConfig_SetPacketApiMode( )
  @todo Probably need some way to reset the parser if there are no bytes for a while

  \par Example
  \code
  // we're inside a task here...
  XBeePacket myPacket;
  XBee_ResetPacket( &myPacket );
  while( 1 )
  {
    if( XBee_GetPacket( &myPacket, 100 ) )
    {
      // process the new packet
      XBee_ResetPacket( &myPacket ); // then clear it out before reading again
    }
    Sleep( 10 );
  }
  \endcode
*/
#if  1

unsigned char *debug1 = "done ";
unsigned char *debug2 = "not done";
int XBee_BuildPacket(unsigned char newChar, XBeePacket* packet)
{

      switch( packet->rxState )
      {
        case XBEE_PACKET_RX_START:
          if( newChar == XBEE_PACKET_STARTBYTE )
            packet->rxState = XBEE_PACKET_RX_LENGTH_1;
           
          break;
        case XBEE_PACKET_RX_LENGTH_1:
          packet->length = newChar;
          packet->length <<= 8;
          packet->rxState = XBEE_PACKET_RX_LENGTH_2;
          
          break;
        case XBEE_PACKET_RX_LENGTH_2:
          packet->length += newChar;
          
          if( packet->length > XBEE_MAX_PACKET_SIZE ) // in case we somehow get some garbage        
              packet->rxState = XBEE_PACKET_RX_START;
          else
            packet->rxState = XBEE_PACKET_RX_PAYLOAD;
          packet->crc = 0;
          break;
        case XBEE_PACKET_RX_PAYLOAD:
          *packet->dataPtr++ = newChar;
          if( ++packet->index >= packet->length )
            packet->rxState = XBEE_PACKET_RX_CRC;
          packet->crc += newChar;
          break;
        case XBEE_PACKET_RX_CRC:
          packet->crc += newChar;
          packet->rxState = XBEE_PACKET_RX_START;
          if( packet->crc == 0xFF)
         {
              uart1_putbuf(debug1,strlen(debug1));             
              return 1;
         }
         else
          {
            XBee_ResetPacket( packet );

              uart1_putbuf(debug2,strlen(debug2));
            return 0;
          }
      }
}

int XBee_GetPacket(uchar device, XBeePacket* packet, int timeout )
{
  //int time = TaskGetTickCount( );
  //do
  while (1)
  {
    //Serial_ClearErrors();
      //aus queue holen
      int newChar = Serial_GetChar(device);

      //Serial_GetChar( );
      if( newChar == -1 )
        break;
  
      return XBee_BuildPacket((unsigned char) newChar&0xFF, packet);
    
    //if ( timeout > 0 )
      //Sleep( 1 );
  } //while( ( TaskGetTickCount( ) - time ) < timeout );
  return 0;
}

#endif

/**	
  Send an XBee packet.
	Use the following functions to create packets to be sent:
	- XBee_CreateTX16Packet( ) - create a data packet to be sent out wirelessly with a 16-bit address
	- XBee_CreateTX64Packet( ) - create a data packet to be sent out wirelessly with a 64-bit address
	- XBee_CreateATCommandPacket( ) - create an AT command to configure an attached XBee module
  @param packet The XBeePacket to send.
  @param datalength The length of the actual data being sent (not including headers, options, etc.)
	@return Zero on success.
  @see XBeeConfig_SetPacketApiMode( )

  \par Example
  \code
  XBeePacket txPacket;
  uint8 data[] = "ABC"; // 3 bytes of data
  XBee_CreateTX16Packet( &txPacket, 0, 0, 0, data, 3 );
  XBee_SendPacket( &txPacket, 3 );
  \endcode
*/

int XBee_SendPacket(uchar device, XBeePacket* packet, int datalength )
{
   
  Serial_SetChar(device, XBEE_PACKET_STARTBYTE );
  int size = datalength;
  switch( packet->apiId )
  {
    case XBEE_TX64: //account for apiId, frameId, 8 bytes destination, and options
      size += 11;
      break;
    case XBEE_TX16: //account for apiId, frameId, 2 bytes destination, and options
      size += 5; 
      break;
    case XBEE_ATCOMMAND: // length = API ID + Frame ID, + AT Command (+ Parameter Value)
      size = (datalength > 0) ? 8 : 4; // if we're writing, there are 4 bytes of data, otherwise, just the length above
      break;
    default:
      size = 0;
      break;
  }

  Serial_SetChar(device, (size >> 8) & 0xFF ); // send the most significant bit(MSB)
  Serial_SetChar(device, size & 0xFF ); // then the LSB
  packet->crc = 0; // just in case it hasn't been initialized.
  uint8* p = (uint8*)packet;
  while( size-- )
  {
    Serial_SetChar(device, *p );
    packet->crc += *p++;
  }
  //uint8 test = 0xFF - packet->crc;
  Serial_SetChar(device, 0xFF - packet->crc );
  return CONTROLLER_OK;
}

/**	
  Initialize a packet before reading into it.
  @param packet The XBeePacket to initialize.
  @see XBee_GetPacket( )
*/
void XBee_ResetPacket(XBeePacket* packet )
{
  packet->dataPtr = (uint8*)packet;
  packet->crc = 0;
  packet->rxState = XBEE_PACKET_RX_START;
  packet->length = 0;
  packet->index = 0;
  packet->apiId = 0;
  memset( packet->payload, 0, 100 );
}

/** 
  Checks to see if a packet is receiving a message
  @param packet The XBeePacket to check
  @returns true if the packet is busy, false if it's free
*/
int XBee_IsBusyPacket( XBeePacket* packet )
{
  return ( packet->rxState != XBEE_PACKET_RX_START );
}

/**	
  Set a module into packet API mode.  
  XBee modules are in transparent serial port mode by default.  This allows you to work with them
  via the packet API.

  When setting this on, the XBee module needs to wait 1 second after sending the command sequence before it's 
  ready to receive any AT commands - this function will block for that amount of time.  Once you turn it off,
  you won't get any responses to packets you send the module until you turn packet mode on again.
  @param value 1 to turn packet mode on, 0 to turn it off.
	
  \par Example
  \code
	MyTask( void * p )
	{
		XBeeConfig_SetPacketApiMode( 1 ); // initialize the module to be in API mode
		while( 1 )
		{
			// your task here.
		}
	}
  \endcode
*/
void XBeeConfig_SetPacketApiMode(uchar device, int value )
{
  long i=0;int j =0;
  if( value )
  {
    char buf[50];
    sprintf( buf, "+++" ); // enter command mode
    Serial_Write(device, (uchar*)buf, strlen(buf), 0 );
    //tn_task_sleep(1100);
    //Sleep( 1025 ); // have to wait one second after +++ to actually get set to receive in AT mode
    for(i=0;i<6000;i++)
      for (j=0;j<1000;j++);

    sprintf( buf, "ATAP1,CN\r\n" ); // turn API mode on, and leave command mode // AP=1 model
    Serial_Write( device,(uchar*)buf, strlen(buf), 0 );
    for(i=0;i<10000;i++)
      for (j=0;j<100;j++);

    //Sleep(50);
    //Serial_Flush(); // rip the OKs out of there
  }
  else
  {
    XBeePacket xbp;
    uint8 params[4];
    memset( params, 0, 4 );
    XBee_CreateATCommandPacket(&xbp, 0, "AP", params, 4 );
    XBee_SendPacket(device, &xbp, 4 );
  }
}

/**	
  Query whether the module is in API mode.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return 0 if off, 1 if enabled, 2 if enabled with escape characters.
	
  \par Example
  \code
  int mode = XBeeConfig_RequestPacketApiMode( );
  if( mode >= 0 )
  {
    // then we have our mode
  }
  \endcode
*/
int XBeeConfig_RequestPacketApiMode(uchar device)
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, "AP", NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  A convenience function for creating an AT command packet.
  As per the XBee spec, AT Command packets that want to set/write a value must have 4 bytes of data.  
  Packets that query the value of an AT parameter send 0 bytes of data.  Note that multi-byte
  data must be sent \b big-endian (most significant byte first) - see XBee_IntToBigEndianArray( ).
  
  Make sure you're in API mode before creating & sending packets - see XBeeConfig_SetPacketApiMode( ).
	See the XBee documentation for the official list of AT commands that the XBee modules understand.
  @param packet The XBeePacket to create.
  @param frameID The frame ID for this packet that subsequent response/status messages can refer to.
  @param cmd The 2-character AT command.
  @param params A pointer to the buffer containing the data to be sent.
  @param datalength The number of bytes to send from the params buffer.
  
  \par Example - Writing
  \code
  XBeePacket txPacket;
  uint8 params[4];
  XBee_IntToBigEndianArray( 1000, params ); // set our sampling rate to 1000
  XBee_CreateATCommandPacket( &txPacket, 0, "IR", &params, 4 ); // set the sampling rate of the IO pins
  XBee_SendPacket( &txPacket, 4 );
  \endcode

  \par Example - Reading
  \code
  XBeePacket txPacket;
  XBee_CreateATCommandPacket( &txPacket, 0, "IR", NULL, 0 ); // query the sampling rate of the IO pins
  XBee_SendPacket( &txPacket, 0 );
  // then we'll receive a response packet
  \endcode
*/
void XBee_CreateATCommandPacket( XBeePacket* packet, uint8 frameID, char* cmd, uint8* params, uint8 datalength )
{
  packet->apiId = XBEE_ATCOMMAND;
  packet->atCommand.frameID = frameID;
  uint8* p = packet->atCommand.command;
  *p++ = *cmd++;
  *p++ = *cmd;
  p = packet->atCommand.parameters;
  while( datalength-- )
    *p++ = *params++;
}

/**	
  Query the address of the module.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the address of the module, or negative number on failure.
	
  \par Example
  \code
  int address = XBeeConfig_RequestAddress( );
  if( address >= 0 )
  {
    // then we have our address
  }
  \endcode
*/
int XBeeConfig_RequestATResponse(uchar device, char* cmd )
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, cmd, NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  Configure the IO settings on an XBee module.
  IO pins can have one of 5 values:
  - \b XBEE_IO_DISABLED
  - \b XBEE_IO_ANALOGIN - Analog input (10-bit)
  - \b XBEE_IO_DIGITALIN - Digital input
  - \b XBEE_IO_DIGOUT_HIGH - Digital out high
  - \b XBEE_IO_DIGOUT_LOW - Digital out low
  @param pin An integer specifying which pin to configure. There are 9 IO pins (numbered 0-8) on the XBee modules.  
	Only channels 0-7 can be analog inputs - channel 8 can only operate as a digital in or out.
	@param value An int specifying the behavior of this pin (options shown above).
  \par Example
  \code
  // set channel 0 to analog in
  XBeeConfig_SetIO( 0, XBEE_IO_ANALOGIN );
  \endcode
*/
void XBeeConfig_SetIO(uchar device, int pin, int value )
{
  XBeePacket packet;
  XBee_ResetPacket( &packet );
  uint8 params[4];
  char cmd[2];
	sprintf( cmd, "D%d", pin );
	XBee_IntToBigEndianArray( value, params );
	XBee_CreateATCommandPacket( &packet, 0, cmd, params, 4 );
	XBee_SendPacket(device, &packet, 4 );
}

/**	
  Query the configuration of an IO pin.
  See XBeeConfig_SetIO( ) for the possible return values.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the config of the requested pin, or negative number on failure.
	
  \par Example
  \code
  int pin = XBeeConfig_RequestIO( 0 ); // request the configuration of pin 0
  if( pin >= 0 )
  {
    // then we have our pin config
  }
  \endcode
*/
int XBeeConfig_RequestIO(uchar device, int pin )
{
  XBeePacket xbp;
  char cmd[2];
	sprintf( cmd, "D%d", pin );
  XBee_CreateATCommandPacket( &xbp, 0x52, cmd, NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  Create a packet to be transmitted with a 16-bit address.
  If the \b frameID is 0, you won't receive a TX Status message in response.

  @param xbp The XBeePacket to create.
  @param frameID The frame ID for this packet that subsequent response/status messages can refer to.  Set to 0 for no response.
  @param destination The destination address for this packet.  Broadcast Address: 0xFFFF.
  @param options The XBee options for this packet (0 if none).
  @param data A pointer to the data to be sent in this packet.  Up to 100 bytes.
  @param datalength The number of bytes of data to be sent. Maximum 100 bytes.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket txPacket;
  uint8 data[] = "ABC";
  XBee_CreateTX16Packet( &txPacket, 0x52, 0, 0, data, 3 );
  XBee_SendPacket( &txPacket, 3 );
  \endcode
*/
bool XBee_CreateTX16Packet( XBeePacket* xbp, uint8 frameID, uint16 destination, uint8 options, uint8* data, uint8 datalength )
{
  xbp->apiId = XBEE_TX16;
  xbp->tx16.frameID = frameID;
  xbp->tx16.destination[0] = destination >> 8;
  xbp->tx16.destination[1] = destination & 0xFF;
  xbp->tx16.options = options;
  xbp->length = datalength + 5;
  uint8* p = xbp->tx16.data;
  while( datalength-- )
    *p++ = *data++;
  return true;
}

/**	
  Create a packet to be transmitted with a 64-bit address.

  @param xbp The XBeePacket to create.
  @param frameID The frame ID for this packet that subsequent response/status messages can refer to.  Set to 0 for no response.
  @param destination The destination address for this packet.  Broadcast Address: 0xFFFF (same as 16b broadcast address)
  @param options The XBee options for this packet (0 if none).
  @param data A pointer to the data to be sent in this packet.  Up to 100 bytes.
  @param datalength The number of bytes of data to be sent. Maximum 100 bytes.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket txPacket;
  uint8 data[] = "ABCDE";
  XBee_CreateTX16Packet( &txPacket, 0, 0, 0, data, 5 );
  XBee_SendPacket( &txPacket, 5 );
  \endcode
*/
bool XBee_CreateTX64Packet( XBeePacket* xbp, uint8 frameID, uint64 destination, uint8 options, uint8* data, uint8 datalength )
{
  uint8* p;
  int i;
  xbp->apiId = XBEE_TX64;
  xbp->tx64.frameID = frameID;
  for( i = 0; i < 8; i++ )
    xbp->tx64.destination[i] = (destination >> 8*i) & (0xFF * i); // ????????
  xbp->tx64.options = options;
  xbp->length = datalength + 5;
  p = xbp->tx64.data;
  while( datalength-- )
    *p++ = *data++;
  return true;
}

/**	
  Unpack the info from an incoming packet with a 16-bit address.
  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param srcAddress The 16-bit address of this packet.
  @param sigstrength The signal strength of this packet.
  @param options The XBee options for this packet.
  @param data A pointer that will be set to the data of this packet.
  @param datalength The length of data in this packet.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint16 src;
    uint8 sigstrength;
    uint8* data;
    uint8 datalength;
    if( XBee_ReadRX16Packet( &rxPacket, &src, &sigstrength, NULL, &data, &datalength ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadRX16Packet( XBeePacket* xbp, uint16* srcAddress, uint8* sigstrength, uint8* options, uint8** data, uint8* datalength )
{
  
  if( xbp->apiId != XBEE_RX16 )
    return false;

  if( srcAddress )
  {
    *srcAddress = xbp->rx16.source[0];
    *srcAddress <<= 8;
    *srcAddress += xbp->rx16.source[1];
  }
  if( sigstrength )
    *sigstrength = xbp->rx16.rssi;
  if( options )
    *options = xbp->rx16.options;
  if( data )
    *data = xbp->rx16.data;
  if( datalength )
    *datalength = xbp->length - 5;
  return true;
}

/**	
  Unpack the info from an incoming packet with a 64-bit address.
  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param srcAddress The 64-bit address of this packet.
  @param sigstrength The signal strength of this packet.
  @param options The XBee options for this packet.
  @param data A pointer that will be set to the data of this packet.
  @param datalength The length of data in this packet.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint64 src;
    uint8 sigstrength;
    uint8* data;
    uint8 datalength;
    if( XBee_ReadRX64Packet( &rxPacket, &src, &sigstrength, NULL, &data, &datalength ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadRX64Packet( XBeePacket* xbp, uint64* srcAddress, uint8* sigstrength, uint8* options, uint8** data, uint8* datalength )
{
  if( xbp->apiId != XBEE_RX64 )
    return false;

  int i;
  if( srcAddress )
  {
    for( i = 0; i < 8; i++ )
    {
      *srcAddress <<= i*8;
      *srcAddress += xbp->rx64.source[i];
    }
  }
  if( sigstrength )
    *sigstrength = xbp->rx64.rssi;
  if( options )
    *options = xbp->rx64.options;
  if( data )
    *data = xbp->rx64.data;
  if( datalength )
    *datalength = xbp->length - 11;
  return true;
}

/**	
  Unpack the info from an incoming IO packet with a 16-bit address.
  When an XBee module has been given a sample rate, it will sample its IO pins according to their current configuration
  and send an IO packet with the sample data.  This function will extract the sample info into an array of ints for you.
  There are 9 IO pins on the XBee modules, so be sure that the array you pass in has room for 9 ints.

  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param srcAddress A pointer to a uint16 that will be filled up with the 16-bit address of this packet.
  @param sigstrength A pointer to a uint8 that will be filled up with the signal strength of this packet.
  @param options A pointer to a uint8 that will be filled up with the XBee options for this packet.
  @param samples A pointer to an array of ints that will be filled up with the sample values from this packet.
	@return True on success, false on failure.
  @see XBeeConfig_SetSampleRate( ), XBeeConfig_SetIOs( )
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint16 src;
    uint8 sigstrength;
    int samples[9];
    if( XBee_ReadIO16Packet( &rxPacket, &src, &sigstrength, NULL, samples ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadIO16Packet( XBeePacket* xbp, uint16* srcAddress, uint8* sigstrength, uint8* options, int* samples )
{
  if( xbp->apiId != XBEE_IO16 )
    return false;
  if( srcAddress )
  {
    *srcAddress = xbp->io16.source[0];
    *srcAddress <<= 8;
    *srcAddress += xbp->io16.source[1];
  }
  if( sigstrength )
    *sigstrength = xbp->io16.rssi;
  if( options )
    *options = xbp->io16.options;
  if( samples )
  {
    if( !XBee_GetIOValues( xbp, samples ) )
      return false;
  }
  return true;
}

/**	
  Unpack the info from an incoming IO packet with a 64-bit address.
  When an XBee module has been given a sample rate, it will sample its IO pins according to their current configuration
  and send an IO packet with the sample data.  This function will extract the sample info into an array of ints for you.
  There are 9 IO pins on the XBee modules, so be sure that the array you pass in has room for 9 ints.

  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param srcAddress A pointer to a uint64 that will be filled up with the 16-bit address of this packet.
  @param sigstrength A pointer to a uint8 that will be filled up with the signal strength of this packet.
  @param options A pointer to a uint8 that will be filled up with the XBee options for this packet.
  @param samples A pointer to an array of ints that will be filled up with the sample values from this packet.
	@return True on success, false on failure.
  @see XBeeConfig_SetSampleRate( ), XBeeConfig_SetIOs( )
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint64 src;
    uint8 sigstrength;
    int samples[9];
    if( XBee_ReadIO16Packet( &rxPacket, &src, &sigstrength, NULL, samples ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadIO64Packet( XBeePacket* xbp, uint64* srcAddress, uint8* sigstrength, uint8* options, int* samples )
{
  if( xbp->apiId != XBEE_RX64 )
    return false;
  if( srcAddress )
  {
    int i;
    for( i = 0; i < 8; i++ )
    {
      *srcAddress <<= i*8;
      *srcAddress += xbp->io64.source[i];
    }
  }
  if( sigstrength )
    *sigstrength = xbp->io64.rssi;
  if( options )
    *options = xbp->io64.options;
  if( samples )
  {
    if( !XBee_GetIOValues( xbp, samples ) )
      return false;
  }
  return true;
}

/**	
  Unpack the info from an incoming AT Command Response packet.
  In response to a previous AT Command message, the module will send an AT Command Response message.

  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param frameID A pointer to a uint64 that will be filled up with the 16-bit address of this packet.
  @param command A pointer to a uint8 that will be filled up with the signal strength of this packet.
  @param status A pointer to a uint8 that will be filled up with the XBee options for this packet.
  @param datavalue The value of the requested command.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint8 frameID;
    char* command;
    uint8 status;
    int value = -1;
    if( XBee_ReadAtResponsePacket( &rxPacket, &frameID, command, &status, &value ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadAtResponsePacket( XBeePacket* xbp, uint8* frameID, char** command, uint8* status, int* datavalue )
{
  if( xbp->apiId != XBEE_ATCOMMANDRESPONSE )
    return false;
  if( frameID )
    *frameID = xbp->atResponse.frameID;
  if( command )
    *command = (char*)xbp->atResponse.command;
  if( status )
    *status = xbp->atResponse.status;
  if( datavalue )
  {
    uint8 *dataPtr = xbp->atResponse.value;
    int i;
    int datalength = xbp->length - 5; // data comes after apiID, frameID, 2-bytes of cmd, and 1-byte status
    *datavalue = 0;
    for( i = 0; i < datalength; i++ )
    {
      *datavalue <<= 8;
      *datavalue += *dataPtr++;
    }

  }
  return true;
}

/**	
  Unpack the info from TX Status packet.
  When a TX is completed, the modules esnds a TX Status message.  This indicates whether the packet was transmitted
  successfully or not.  If the message you sent had a frameID of 0, a TX status message will not be generated.

  Pass \b NULL into any of the parameters you don't care about.
  @param xbp The XBeePacket to read from.
  @param frameID A pointer to a uint8 that will be filled up with the frame ID of this packet.
  @param status A pointer to a uint8 that will be filled up with the status of this packet.
	@return True on success, false on failure.
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    uint8 frameID;
    uint8 status;
    if( XBee_ReadTXStatusPacket( &rxPacket, &frameID, &status ) )
    {
      // then process the new packet here
      XBee_ResetPacket( &rxPacket ); // and clear it out before reading again
    }
  }
  \endcode
*/
bool XBee_ReadTXStatusPacket( XBeePacket* xbp, uint8* frameID, uint8* status )
{
  if( xbp->apiId != XBEE_TXSTATUS )
    return false;
  if( frameID )
    *frameID = xbp->txStatus.frameID;
  if( status )
    *status = xbp->txStatus.status;
  return true;
}

/**	
  Save the configuration changes you've made on the module to memory.
  When you make configuration changes - setting the module to API mode, or configuring the sample rate, for example - 
  those changes will be lost when the module restarts.  Call this function to save the current state to non-volatile memory.

  As with the other \b XBeeConfig functions, make sure you're in API mode before trying to use this function.
	@return True on success, false on failure.
  @see XBeeConfig_SetPacketApiMode( )
  
  \par Example
  \code
  XBeeConfig_SetPacketApiMode( );
  XBeeConfig_SetSampleRate( 100 );
  XBeeConfig_WriteStateToMemory( );
  \endcode
*/
void XBeeConfig_WriteStateToMemory( uchar device)
{
  XBeePacket xbp;
  uint8 params[4];
  memset( params, 0, 4 );
  XBee_CreateATCommandPacket( &xbp, 0, "WR", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

/**	
  Set this module's address.

  As with the other \b XBeeConfig functions, make sure you're in API mode before trying to use this function.
  @param address An integer specifying the module's address.
	@return True on success, false on failure.
  @see XBeeConfig_SetPacketApiMode( )
  
  \par Example
  \code
  XBeeConfig_SetPacketApiMode( );
  XBeeConfig_SetAddress( 100 );
  XBeeConfig_WriteStateToMemory( );
  \endcode
*/
void XBeeConfig_SetAddress( uchar device,uint16 address )
{
  XBeePacket xbp;
  uint8 params[4]; // big endian - most significant bit first
  XBee_IntToBigEndianArray( address, params );
  XBee_CreateATCommandPacket( &xbp, 0, "MY", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

/**	
  Query the address of the module.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the address of the module, or negative number on failure.
	
  \par Example
  \code
  int address = XBeeConfig_RequestAddress( );
  if( address >= 0 )
  {
    // then we have our address
  }
  \endcode
*/
int XBeeConfig_RequestAddress(uchar device)
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, "MY", NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  Set this PAN (Personal Area Network) ID.
  Only modules with matching PAN IDs can communicate with each other.  Unique PAN IDs enable control of which
  RF packets are received by a module.  Default is \b 0x3332.

  As with the other \b XBeeConfig functions, make sure you're in API mode before trying to use this function.
  @param id A uint16 specifying the PAN ID.
	@return True on success, false on failure.
  @see XBeeConfig_SetPacketApiMode( )
  
  \par Example
  \code
  XBeeConfig_SetPacketApiMode( );
  XBeeConfig_SetPanID( 0x1234 );
  \endcode
*/
void XBeeConfig_SetPanID(uchar device, uint16 id )
{
  XBeePacket xbp;
  uint8 params[4]; // big endian - most significant bit first
  XBee_IntToBigEndianArray( id, params );
  XBee_CreateATCommandPacket( &xbp, 0, "ID", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

/**	
  Query the PAN ID of the module.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the PAN ID of the module, or negative number on failure.
	
  \par Example
  \code
  int panid = XBeeConfig_RequestPanID( );
  if( panid >= 0 )
  {
    // then we have our pan id
  }
  \endcode
*/
int XBeeConfig_RequestPanID(uchar device )
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, "ID", NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  Set the module's channel.
  The channel is one of 3 addressing options available to the module - the others are the PAN ID and the
  destination address.  In order for modules to communicate with each other, the modules must share the same
  channel number.  Default is \b 0x0C.

  This value can have the range \b 0x0B - \b 0x1A for XBee modules, and \b 0x0C - \b 0x17 for XBee-Pro modules.

  As with the other \b XBeeConfig functions, make sure you're in API mode before trying to use this function.
  @param channel A uint8 specifying the channel.
	@return True on success, false on failure.
  @see XBeeConfig_SetPacketApiMode( )
  
  \par Example
  \code
  XBeeConfig_SetPacketApiMode( );
  XBeeConfig_SetChannel( 0x0D );
  \endcode
*/
void XBeeConfig_SetChannel( uchar device,uint8 channel )
{
  XBeePacket xbp;
  uint8 params[4];
  XBee_IntToBigEndianArray( channel, params );
  XBee_CreateATCommandPacket( &xbp, 0, "CH", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

/**	
  Query the channel of the module.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the channel of the module, or negative number on failure.
	
  \par Example
  \code
  int chan = XBeeConfig_RequestChannel( );
  if( chan >= 0 )
  {
    // then we have our channel
  }
  \endcode
*/
int XBeeConfig_RequestChannel(uchar device)
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, "CH", NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/**	
  Set the rate at which the module will sample its IO pins.
  When the sample rate is set, the module will sample all its IO pins according to its current IO configuration and send
  a packet with the sample data.  If this is set too low and the IO configuration is sampling too many channels, 
  the RF module won't be able to keep up.  You can also adjust how many samples are gathered before a packet is sent.

  As with the other \b XBeeConfig functions, make sure you're in API mode before trying to use this function.
  @param rate A uint16 specifying the sample rate in milliseconds.
	@return True on success, false on failure.
  @see XBeeConfig_SetIOs( ), XBeeConfig_SetPacketApiMode( )
  
  \par Example
  \code
  XBeeConfig_SetPacketApiMode( );
  XBeeConfig_SetSampleRate( 0x14 );
  \endcode
*/
void XBeeConfig_SetSampleRate(uchar device, uint16 rate )
{
  XBeePacket xbp;
  uint8 params[4]; // big endian - most significant bit first
  XBee_IntToBigEndianArray( rate, params );
  XBee_CreateATCommandPacket( &xbp, 0, "IR", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

/**	
  Query the sample rate of the module.
  This will block for up to a 1/2 second waiting for a response from the XBee module.
  @return An integer corresponding to the sample rate of the module, or negative number on failure.
	
  \par Example
  \code
  int rate = XBeeConfig_RequestSampleRate( );
  if( rate >= 0 )
  {
    // then we have our rate
  }
  \endcode
*/
int XBeeConfig_RequestSampleRate(uchar device )
{
  XBeePacket xbp;
  XBee_CreateATCommandPacket( &xbp, 0x52, "IR", NULL, 0 );
  XBee_SendPacket(device, &xbp, 0 );
  return CONTROLLER_OK;
}

/** set up the RTS/CTS
*/

void XBeeConfig_SetContorlFlow( uchar device,uint8 data )
{
  XBeePacket xbp;
  uint8 params[4];
  XBee_IntToBigEndianArray( data, params );
  XBee_CreateATCommandPacket( &xbp, 0, "D6", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

void XBeeConfig_SetRO( uchar device,uint8 data )
{
  XBeePacket xbp;
  uint8 params[4];
  XBee_IntToBigEndianArray( data, params );
  XBee_CreateATCommandPacket( &xbp, 0, "RO", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}

void XBeeConfig_SetCE( uchar device,uint8 data )
{
  XBeePacket xbp;
  uint8 params[4];
  XBee_IntToBigEndianArray( data, params );
  XBee_CreateATCommandPacket( &xbp, 0, "CE", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}


void XBeeConfig_SetBD( uchar device,uint8 data )
{
  XBeePacket xbp;
  uint8 params[4];
  XBee_IntToBigEndianArray( data, params );
  XBee_CreateATCommandPacket( &xbp, 0, "BD", params, 4 );
  XBee_SendPacket(device, &xbp, 4 );
}
/**	
  Convert a 32-bit integer into a big endian array of 4 unsigned 8-bit integers.
  This is mostly useful for sending AT Command packets.
  @param value The value to convert.
  @param array An array of 4 unsigned 8-bit integers (uint8).  Be sure you have 4.
  \par Example
  see XBee_CreateATCommandPacket( ) for an example.
*/
void XBee_IntToBigEndianArray( int value, uint8* array )
{
  array[0] = (value >> 24) & 0xFF;
  array[1] = (value >> 16) & 0xFF;
  array[2] = (value >> 8) & 0xFF;
  array[3] = value & 0xFF;
}



/** @}
*/
/**	
  Unpack IO values from an incoming packet.
  @param packet The XBeePacket to read from.
  @param inputs An array of at least 9 integers, which will be populated with the values of the 9 input lines on the XBee module.
	@return 1 if IO values were successfully retrieved, otherwise zero.
  
  \par Example
  \code
  XBeePacket rxPacket;
  if( XBee_GetPacket( &rxPacket, 0 ) )
  {
    int inputs[9];
    if( XBee_GetIOValues( &rxPacket, inputs ) )
    {
      // process new input values here
    }
  }
  \endcode
*/
static bool XBee_GetIOValues( XBeePacket* packet, int *inputs )
{
  if( packet->apiId == XBEE_IO16 || packet->apiId == XBEE_IO64 )
  {
    int i;
    static bool enabled;
    int digitalins = 0;
    uint8* p;
    int channelIndicators;
    if( packet->apiId == XBEE_IO16 )
    {
      p = packet->io16.data;
      channelIndicators = (packet->io16.channelIndicators[0] << 0x08) | packet->io16.channelIndicators[1];
    }
    else // packet->apiId == XBEE_IO64
    {
      p = packet->io64.data;
      channelIndicators = (packet->io64.channelIndicators[0] << 0x08) | packet->io64.channelIndicators[1];
    }

    for( i = 0; i < XBEE_INPUTS; i++ )
    {
      enabled = channelIndicators & 1;
      channelIndicators >>= 1;
      if( i < 9 ) // digital ins
      {
        inputs[i] = 0; // zero out the 9 inputs beforehand
        if( enabled )
        {
          if( !digitalins )
          {
            int dig0 = *p++ << 0x08;
            digitalins = dig0 | *p++;
          }
          inputs[i] = ((digitalins >> i) & 1) * 1023;
        }
      }
      else // analog ins
      {
        if( enabled )
        {
          int ain_msb = *p++ << 0x08;
          inputs[i-9] = ain_msb | *p++;
        }
      }
    }
    return true;
  }
  else
    return false;
}

