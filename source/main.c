/**
* Main.c : All the task mapping is done in this file.
* Defination and usage of driver functions to get the information from CAN and Xbee
*
* Author: Pulkit Gupta
*/

#include "hardware.h"
#include "AT91SAM7S256.h"
#include "main.h"
#include <global.h>
#include <queue.h>

unsigned char flagCan;

void main(void)
 {    
    
    unsigned char *xbuffer;

    InitHardware();        
    xbuffer = "welcome CAR2X \n";
    //LCD_WriteTextPos("Recieving heart-beat",1,1);        
    uart1_putbuf(xbuffer,strlen(xbuffer));
    //XBeeConfig_SetPacketApiMode(0,1);
        //Create the Queues
    xbeeRxQueue = xQueueCreate(256, 1); //recieve data from uart
    
       
    xTaskCreate( vTaskXBee,  (const signed portCHAR * const)"TaskXBee", configMINIMAL_STACK_SIZE, NULL, mainCOM_PRIORITY1, NULL );
    xTaskCreate( vTaskMain, (const signed portCHAR * const)"TaskMain", configMINIMAL_STACK_SIZE, NULL, mainCOM_PRIORITY2, NULL );      
    xTaskCreate( vTaskCAN, (const signed portCHAR * const)"vTaskCAN", configMINIMAL_STACK_SIZE, NULL, mainCOM_PRIORITY2, NULL );
    __enable_interrupt();
    //Start the PIT
    
    vTaskStartScheduler();
    while(1) { };     
 
}



int msgid = 1;
CAN_MESSAGE canMessage;
/* XBEE */
static portTASK_FUNCTION( vTaskXBee, pvParameters )
{
    unsigned char i = '-';    //incomming data from uart is stored here.
    unsigned int timeout = 56;
    XBeePacket xbeeRxPacket;
    
    unsigned short srcId;
    unsigned char rx,dataLength,frameID,status;
    unsigned char* data; // for storing data finally

    XBee_ResetPacket(&xbeeRxPacket); 

    for(;;)
    {                 
     if( xQueueReceive( xbeeRxQueue, &i, ( portTickType ) 100 ) )
     {       

        while(XBee_BuildPacket(i,&xbeeRxPacket)!=1)
        {   
          //uart1_putc(i);         
          xQueueReceive( xbeeRxQueue, &i, ( portTickType ) 100 );          
          timeout--;
          if ( timeout == 0)
          {
            timeout = 56;  //maximum number of bytes to be taken is 56 bytes          
            break;    
          }
          LedSwitch(1,2);
        }        
        timeout = 0;
        if( XBee_ReadRX16Packet( &xbeeRxPacket, &srcId, &rx, NULL, &data, &dataLength ) )
        {
        }
          LCD_WriteTextPos("                   ",1,1);
          LCD_WriteTextPos("Generated CAN MSG",1,1);

          LCD_WriteTextPos("                   ",2,1);
          LCD_WriteTextPos(xbeeRxPacket.rx16.data,2,1);
                            
          mcp2515Init();
          mcp2515_TestData();
          int k =0;
          for(k = 0; k<8;k++)
          {
            canMessage.data[k] = xbeeRxPacket.rx16.data[k];  
          }                  
          //canMessage.dlc = xbeeRxPacket.rx16.dlc;
          canMessage.dlc = 8;
          canMessage.id = 0x01;
          mcp2515_SendMessage(0x01,&canMessage);
          
        /*unsigned char *datais = "data is ";
        unsigned char *temp = xbeeRxPacket.rx16.data;
        uart1_putbuf(datais, strlen(datais));
        uart1_putbuf(temp, strlen(temp));*/
        XBee_ResetPacket(&xbeeRxPacket);
     }       
  }     
 
  vTaskDelay(2); //originaly 2

}

/*DUMMY*/
static portTASK_FUNCTION( vTaskMain, pvParameters )
{
  unsigned int i,len=0;
  //unsigned int flag = 0;
  XBeePacket txPacket;
  
  vTaskDelay(2000);
  for(;;)
  {
            
       LedSwitch(3,2);    
       //LCD_WriteTextPos("sending",4,1);
      // XBee_CreateTX16Packet(  &txPacket, 0x00, 0x0002, 0, xbee_data, 6 ); // to id=1
      //XBee_CreateTX16Packet(  &txPacket, 0x00, 0x0002, 0, xbee_data, 6 ); // to id=2      
      if (i & 0x08) //Up
      {          
        uint8 xbee_data[] = "Dont Overtake!";
        XBee_CreateTX16Packet(  &txPacket, 0x00, 0xFFFF, 0, xbee_data, 15 ); //to all
        XBee_SendPacket(0, &txPacket, strlen(xbee_data) );      
        XBee_ResetPacket(&txPacket);     
        while(ReadSwitches() & 0x08);       
      }
      else if (i & 0x01) //down
      {
        uint8 xbee_data[] = "Hard brakes Stop!!";
        XBee_CreateTX16Packet(   &txPacket, 0x00, 0xFFFF, 0, xbee_data, 19 ); //to all
        XBee_SendPacket(0, &txPacket, strlen(xbee_data) );      
        XBee_ResetPacket(&txPacket);     
        while(ReadSwitches() & 0x01);
      }

      else if (i & 0x02) //left
      {
        uint8 xbee_data[] = "Right-Lane Not free";
        XBee_CreateTX16Packet(  &txPacket, 0x00, 0xFFFF, 0, xbee_data, 19 ); //to all
        XBee_SendPacket(0, &txPacket, strlen(xbee_data) );      
        XBee_ResetPacket(&txPacket);     
        while(ReadSwitches() & 0x02);
      }

      else if (i & 0x04) //right
      {
        uint8 xbee_data[] = "Car on the left";
        XBee_CreateTX16Packet(  &txPacket, 0x00, 0xFFFF, 0, xbee_data, 16 ); //to all
        XBee_SendPacket(0, &txPacket, strlen(xbee_data) );      
        XBee_ResetPacket(&txPacket);     
        while(ReadSwitches() & 0x04);
      }      
    
      vTaskDelay(100);
    i = ReadSwitches(); //dummy   
  }
}


static portTASK_FUNCTION( vTaskCAN, pvParameters )
{
//    volatile unsigned int i;
//    for(;;)
//    {
//        i++;
//        vTaskDelay(20);
//    }
    unsigned int i,len=0;
    unsigned char x;
    mcp2515Init();
    XBeePacket txPacket;
    uint8 xbee_data[] = "Stop!";
    for(;;)
    {            

        if ((flagCan & 0x01) == 0x01)
        {
          flagCan &= ~0x01;          
          //mcp2515_SpiByteWrite(CANINTE,0x01);    

          x = mcp2515_checkData();
          if (x & 0x01){
           LCD_WriteTextPos("            ",2,1); 
           LCD_WriteTextPos("Recieved CAN",3,1);
           mcp2515_GetMessage(0x01,&canMessage);
           //LCD_WriteTextPos(canMessage.data,3,1);

           LCD_WriteInt(canMessage.id, 3, 13,17);
           LCD_WriteTextPos("              ",4,1);
           LCD_WriteTextPos("Data: ",4,1);           
           LCD_WriteTextPos(canMessage.data,4,8);
           
           //Send CAN msg over Xbee
           XBee_CreateTX16Packet(  &txPacket, 0x00, 0xFFFF, 0, canMessage.data, 15 ); //to all
          XBee_SendPacket(0, &txPacket, strlen(canMessage.data) );      
          XBee_ResetPacket(&txPacket);     


          }
          if (x & 0x02){
            LCD_WriteTextPos("             ",2,1); 
           LCD_WriteTextPos("Recieved CAN-Ex",2,1);
           mcp2515_GetMessage(0x02,&canMessage);
          }
          


        }
        //mcp2515_checkData();
       
        
       

        vTaskDelay(10);
    }
}



void vApplicationIdleHook( void )
{
  /* The idle hook simply sends a string of characters to the USB port.
  The characters will be buffered and sent once the port is connected. */
}
