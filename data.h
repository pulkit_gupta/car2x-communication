#ifndef _DATA_H_
#define _DATA_H_

#include "typedefs.h"



extern unsigned char usbmac[] ;
extern unsigned char usbmac1[];


typedef struct CAN_MESSAGE_STRUCT
{
    unsigned long timestamp;
    unsigned int id;
    unsigned char dlc;
    unsigned char isValid;
    unsigned char data[8];
} CAN_MESSAGE;




#endif