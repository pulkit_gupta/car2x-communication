#include "mcp2515v2.h"
#include "spi.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "hardware.h"
#include "typedefs.h"
#include "AT91SAM7S256.h"
#include "data.h"


unsigned char canBuffer[8];
unsigned char canLenght;
unsigned char dummy;
unsigned char gRXFlag;

#define Mode00  0
#define Mode11  1

#define CAN_500kbps 1       //BRP setting in CNF1
#define CAN_250kbps 3       //Based on 16 MHz Fosc
#define CAN_125kbps 7       //


void mcp2515_SpiCommand(unsigned char command)
{
  SPI_SelectDevice(SPI_MRF24J40);
  AT91C_BASE_PIOA->PIO_CODR =  AT91C_PIO_PA1;
  spiTransferByte(command);
  SPI_UnselectDevice();
  AT91C_BASE_PIOA->PIO_SODR =  AT91C_PIO_PA1;

}

void mcp2515_SpiByteWrite(unsigned char addr, unsigned char value )
{
  SPI_SelectDevice(SPI_MRF24J40);
  AT91C_BASE_PIOA->PIO_CODR =  AT91C_PIO_PA1;
  spiTransferByte(CAN_WRITE);
  spiTransferByte(addr);
  spiTransferByte(value);
  SPI_UnselectDevice();
  AT91C_BASE_PIOA->PIO_SODR =  AT91C_PIO_PA1;
}

unsigned char mcp2515_SpiByteRead(unsigned char addr)
{
  unsigned char tempdata;
  AT91C_BASE_PIOA->PIO_CODR =  AT91C_PIO_PA1;
  SPI_SelectDevice(SPI_MRF24J40);
  spiTransferByte(CAN_READ);
  tempdata = spiTransferByte(addr);
  tempdata = spiTransferByte(0xFF);
  SPI_UnselectDevice();
  AT91C_BASE_PIOA->PIO_SODR =  AT91C_PIO_PA1;
  return tempdata;
}

void Delay_ms(unsigned int i)
{
  unsigned int j;
  for (i<<=12;i>0;i--)
    for (j=0;j<100;j++);
}

void mcp2515Init(void)
{
  unsigned int i,j;

  //Chip Reset
  AT91C_BASE_PIOA->PIO_CODR =  AT91C_PIO_PA0;
  for (i=0;i<1000;i++)
    for (j=0;j<100;j++);
  AT91C_BASE_PIOA->PIO_SODR =  AT91C_PIO_PA0;

  //SPIReset();
  mcp2515_SpiCommand(CAN_RESET);
  for (i=0;i<1000;i++);
    //for (j=0;j<100;j++);

  
  dummy = mcp2515_SpiByteRead(CANSTAT);
  //Clear masks to RX all messages
  mcp2515_SpiByteWrite(RXM0SIDH,0x00);
  mcp2515_SpiByteWrite(RXM0SIDL,0x00);

  //Clear filter... really only concerned to clear EXIDE bit
  mcp2515_SpiByteWrite(RXF0SIDL,0x00);

  //Set CNF1
  mcp2515_SpiByteWrite(CNF1,CAN_125kbps);

  //Set CNF2
  //mcp2515_SpiByteWrite(CNF2,0x80 | PHSEG1_3TQ | PRSEG_1TQ);
  // 1<<PHSEG11 = 0x10
  // 1<<BTLMODE = 0x80
  mcp2515_SpiByteWrite(CNF2, 0x80 | 0x10 | 0x00);

  //Set CNF3
  //mcp2515_SpiByteWrite(CNF3, PHSEG2_3TQ);
  // 1<<PHSEG21 = 0x02
  mcp2515_SpiByteWrite(CNF3, 0x02);


    // Buffer 0 : Empfangen aller Nachrichten
  // 1<<RXM1 = 0x40
 
  mcp2515_SpiByteWrite( RXB0CTRL, 0x40 | 0x20);


  // Buffer 1 : Empfangen aller Nachrichten
  mcp2515_SpiByteWrite( RXB1CTRL, 0x40 | 0x20);

  mcp2515_SpiByteWrite(RXM0SIDH, 0);
  mcp2515_SpiByteWrite(RXM0SIDL, 0);
  mcp2515_SpiByteWrite(RXM0EID8, 0);
  mcp2515_SpiByteWrite(RXM0EID0, 0);

  mcp2515_SpiByteWrite(RXM1SIDH, 0);
  mcp2515_SpiByteWrite(RXM1SIDL, 0);
  mcp2515_SpiByteWrite(RXM1EID8, 0);
  mcp2515_SpiByteWrite(RXM1EID0, 0);

  mcp2515_SpiByteWrite(CANCTRL,0x00 | 0x04);
#if 1

  //Set TXB0 DLC and Data for a "Write Register" Input Message to the MCP25020
  mcp2515_SpiByteWrite(TXB0SIDH,0xA0);    //Set TXB0 SIDH
  mcp2515_SpiByteWrite(TXB0SIDL,0x00);    //Set TXB0 SIDL
  //mcp2515_SpiByteWrite(TXB0DLC,DLC_3);    //Set DLC = 3 bytes
  mcp2515_SpiByteWrite(TXB0DLC,0x03);    //Set DLC = 3 bytes
  mcp2515_SpiByteWrite(TXB0D0,0x1E);      //D0 = Addr = 0x1E
  mcp2515_SpiByteWrite(TXB0D1,0x10);      //D1 = Mask = 0x10

  //Set TXB1 DLC and Data for a "READ I/O IRM"
  mcp2515_SpiByteWrite(TXB1SIDH,0x50);    //Set TXB0 SIDH
  mcp2515_SpiByteWrite(TXB1SIDL,0x00);    //Set TXB0 SIDL
  //mcp2515_SpiByteWrite(TXB1DLC,0x40 | DLC_8);    //Set DLC = 3 bytes and RTR bit
  mcp2515_SpiByteWrite(TXB1DLC,0x40 | 0x08);    //Set DLC = 3 bytes and RTR bit
  
  
  //mcp2515_SpiByteWrite(CANINTE,0x01);    //


  
#endif
  //Interrupt on RXB0 - CANINTE
  mcp2515_SpiByteWrite( CANINTE, (RX1IE | RX0IE));
  //Set NORMAL mode
  //mcp2515_SpiByteWrite(CANCTRL,REQOP_NORMAL | CLKOUT_ENABLED);
  mcp2515_SpiByteWrite(CANCTRL,0x00 | 0x04);


  //Verify device entered Normal mode
  dummy = mcp2515_SpiByteRead(CANSTAT);
  //if (OPMODE_NORMAL != (dummy && 0xE0))
	  //mcp2515_SpiByteWrite(CANCTRL,REQOP_NORMAL | CLKOUT_ENABLED);
  if (0x00 != (dummy && 0xE0))
            mcp2515_SpiByteWrite(CANCTRL,0x00 | 0x04);
}

void RTS(unsigned char buffer)
{
    unsigned char tempdata;
	tempdata = mcp2515_SpiByteRead(CAN_RD_STATUS);
	
	if(buffer & 0x01)	//Buffer 0	
		if(tempdata & 0x04)	//Check TXREQ first
		{
			Delay_ms(1);
			mcp2515_SpiByteWrite(TXB0CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(mcp2515_SpiByteRead(CAN_RD_STATUS) & 0x04); //Wait for TXREQ to clear
	  }
	if(buffer & 0x02)	//Buffer 1
		if(tempdata & 0x10)	//Check TXREQ first
		{
  		Delay_ms(1);
  		mcp2515_SpiByteWrite(TXB1CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(mcp2515_SpiByteRead(CAN_RD_STATUS) & 0x10); //Wait for TXREQ to clear
		}
	if(buffer & 0x04)	//Buffer 2
		if(tempdata & 0x40)	//Check TXREQ first
		{
  		Delay_ms(1);
  		mcp2515_SpiByteWrite(TXB2CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(mcp2515_SpiByteRead(CAN_RD_STATUS) & 0x40); //Wait for TXREQ to clear
		}
	
//	CS = LOW;;
//	mcp2515_SpiByteWrite(buffer);
//	CS = HIGH;;
}


unsigned char mcp2515_TestData(void)
{
  unsigned int n,CANData;
  CANData = 0x10;
  mcp2515_SpiByteWrite(TXB0D2,CANData);
  RTS(CAN_RTS_TXB0);  //RTS msg - Input msg to 25020
  RTS(CAN_RTS_TXB1);
}

//unsigned char mcp2515_ReadStatus(void)
//{
//  unsigned char temp;
//  temp = ReadStatus2515();
//  return temp;
//}

unsigned char mcp2515_SendMessage(unsigned char buffer, CAN_MESSAGE* canMessageP)
{
    unsigned char tempdata;
    if (buffer == 0x01)
    {
      //ID
      mcp2515_SpiByteWrite(TXB0SIDH, ((unsigned char) (canMessageP->id>>3)));
      mcp2515_SpiByteWrite(TXB0SIDL, ((unsigned char) (canMessageP->id<<5)));
      mcp2515_SpiByteWrite(TXB0DLC,  (canMessageP->dlc)&0x0F);

      for (tempdata=0; tempdata<8; tempdata++)
      {
          mcp2515_SpiByteWrite((TXB0D0+tempdata),canMessageP->data[tempdata]);
      }
      mcp2515_SpiCommand(CAN_RTS_TXB0);
    }
}

unsigned char mcp2515_GetMessage(unsigned char buffer, CAN_MESSAGE* canMessageP)
{
    unsigned char tempdata;
    if (buffer == 0x01)
    {
      //TimeStamp
      canMessageP->timestamp = xTaskGetTickCount();
      //ID
      canMessageP->id = mcp2515_SpiByteRead(RXB0SIDH);
      (canMessageP->id)<<=3;
      canMessageP->id |= mcp2515_SpiByteRead(RXB0SIDL)>>5;
      //Len
      canMessageP->dlc = mcp2515_SpiByteRead(RXB0DLC) & 0x0F;
      //Data
      for (tempdata=0;tempdata<8;tempdata++)
      {
          canMessageP->data[tempdata] = mcp2515_SpiByteRead(RXB0D0+tempdata);
      }
      canMessageP->isValid = 1;
      mcp2515_SpiByteWrite(CANINTF,(mcp2515_SpiByteRead(CANINTF) & (~RX0IF)));
    }
    else if (buffer == 0x02)
    {
        mcp2515_SpiByteWrite(CANINTF,(mcp2515_SpiByteRead(CANINTF) & (~RX1IF)));
    }
    return tempdata;
}

unsigned char mcp2515_checkData(void)
{
  unsigned char tempdata;
  tempdata = mcp2515_SpiByteRead(CANINTF);
  if ((tempdata & RX0IF) == RX0IF)
  {

  }
  else if ((tempdata & RX1IF) == RX1IF)
  {
    
  }
}