#ifndef _MAIN_H_
#define _MAIN_H_

#include "typedefs.h"
#include "typedefs.h"
#include "FreeRTOS.h"
#include "config.h"
#include "task.h"
#include "queue.h"
#include "xbee.h"
#include "data.h"
#include "semphr.h"
#include "mcp2515v2.h"



static portTASK_FUNCTION_PROTO( vTaskLCD, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskUSB, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskXBee, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskMain, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskActions, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskUartd, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskCAN, pvParameters );
static portTASK_FUNCTION_PROTO( vTaskDummy, pvParameters );

#define mainCOM_PRIORITY1 ( tskIDLE_PRIORITY + 1 )
#define mainCOM_PRIORITY2 ( tskIDLE_PRIORITY + 2 )
#define mainCOM_PRIORITY3 ( tskIDLE_PRIORITY + 3 )

/* XBEE */
//XBeePacket xbeeRxPacket;
xQueueHandle xbeeRxQueue;
/* PCT */
xQueueHandle pctRxQueue;
///* CAN  */
//xQueueHandle spiRxQueue;
///*DUMMY*/
xQueueHandle dummyRxQueue;



#endif