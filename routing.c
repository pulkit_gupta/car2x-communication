#include <usb/device/rndis/RNDISDriver.h>
#include <usb/device/rndis/RNDISDriverDescriptors.h>
#include "xbee.h"
#include "data.h"
#include "config.h"
#include "MRF24WB0.h"
#include "main.h"

#if defined (PLANET_01)
unsigned char PlanetIP[]   = {0x81,0x00,0x00,0x01};
#elif defined (PLANET_06)
  unsigned char PlanetIP[]   = {0x82,0x00,0x00,0x01};
#elif defined (PLANET_08)
  unsigned char PlanetIP[]   = {0x80,0x00,0x00,0x01};
#else
  error
#endif

unsigned char PlanetMask[] = {0xFF,0x00,0x00,0x00};
extern PACKET_STRUCT wifiTxData;
unsigned char mybuff2[]= {
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x1E,0xC0,0x01,0xF0,0xF9,0x08,0x00, //ETH
0x45,0x00,0x00,0x2E,0x00,0x00,0x40,0x00,0x40,0x11,0x3A,0xBA,0x80,0x01,0x00,0x01,0x80,0x01,0x00,0x02,  //IP
0x27,0x10,0x27,0x10,0x00,0x1A,0xA0,0xA5, //UDP
0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0x00,0xAA,0xBB,0xCC,0xDD,0xEE,0xFF,0x00,0x00,0x78  //Payload
};


#define PORT_HOST   1
#define PORT_USB    1
#define PORT_XBEE   2
#define PORT_WLAN   3
#define PORT_PCT    4
#define PORT_MICAZ  6
#define PORT_DUMMY  7


unsigned char routingTable [][4] = {
#if defined   (PLANET_01)
  {128, PORT_XBEE, 0x00 ,0x01},    //
#elif defined (PLANET_06)
  {0, 0, 0 ,0},    //entry 0 -> default
#elif defined (PLANET_08)
  {0, 0, 0 ,0},    //entry 0 -> default
#endif

{128, PORT_XBEE ,0x00,0x01},
{129, PORT_XBEE ,0x00,0x01},
{130, PORT_XBEE ,0x00,0x01},
};

#define ROUTING_ENTRIES ((sizeof(routingTable))>>2)



PACKET_STRUCT usbRxData,xbeeRxData,txData,xbeeRxData;



/* Routing Table */


void sendOverXbee(PACKET_STRUCT* myPacket, unsigned short hopId)
{
  XBeePacket* xbeeTxPacketP = (XBeePacket*)((unsigned int)(myPacket->ethData) + XBEE_OFFSET);
  xbeeTxPacketP->apiId = XBEE_TX16;
  xbeeTxPacketP->tx16.frameID = 0;
  if (hopId == NULL)  //direkt
  {
      xbeeTxPacketP->tx16.destination[1] = (myPacket->ipUdpHeader.ip_dst[1])>> 8;
      xbeeTxPacketP->tx16.destination[0] = (myPacket->ipUdpHeader.ip_dst[1]) & 0xFF;
      xbeeTxPacketP->tx16.options = 0;  //BROADCAST
  }
  else
  {
      xbeeTxPacketP->tx16.destination[1] = hopId >> 8;
      xbeeTxPacketP->tx16.destination[0] = hopId & 0xFF;
      xbeeTxPacketP->tx16.options = 4;  //BROADCAST
  }
  xbeeTxPacketP->length = myPacket->size + 5; //+XbeeHeader
  XBee_SendPacket(0, xbeeTxPacketP, myPacket->size );   
}

void sendOverDummy(PACKET_STRUCT* myPacket, unsigned short hopId)
{
  XBeePacket* xbeeTxPacketP = (XBeePacket*)((unsigned int)(myPacket->ethData) + XBEE_OFFSET);
  xbeeTxPacketP->apiId = XBEE_TX16;
  xbeeTxPacketP->tx16.frameID = 0;
    if (hopId == NULL)  //direkt
  {
      xbeeTxPacketP->tx16.destination[1] = (myPacket->ipUdpHeader.ip_dst[1])>> 8;
      xbeeTxPacketP->tx16.destination[0] = (myPacket->ipUdpHeader.ip_dst[1]) & 0xFF;
      xbeeTxPacketP->tx16.options = 0;  //BROADCAST
  }
  else
  {
      xbeeTxPacketP->tx16.destination[1] = hopId >> 8;
      xbeeTxPacketP->tx16.destination[0] = hopId & 0xFF;
      xbeeTxPacketP->tx16.options = 4;  //BROADCAST
  }
  xbeeTxPacketP->length = myPacket->size + 5; //+XbeeHeader
  XBee_SendPacket(1, xbeeTxPacketP, myPacket->size );   
}


void calcIpChecksum(PACKET_STRUCT* myPacket)
{
    unsigned short i = 0;
    unsigned int s = 0;
    // IP //
    myPacket->ipUdpHeader.ip_sum = 0;
    for (i=0;i<20;i=i+2){
	s +=((myPacket->ipData[i]<<8)&0xFF00)+(myPacket->ipData[i+1]&0xFF);
    } // 4500 4500
    while (s>>16)
	  s = (s & 0xFFFF)+(s >> 16);
//    sum = ~sum;
    myPacket->ipUdpHeader.ip_sum = htons(~s);
    //UDP
}
void calcUdpChecksum(PACKET_STRUCT* myPacket)
{
    unsigned short i = 0;
    unsigned int l = htons(myPacket->ipUdpHeader.udplen);
    unsigned int s = 0;
    // IP //
    myPacket->udpData[l+1] = 0;
    myPacket->ipUdpHeader.udpchksum = 0;
    for (i=0;i<l;i=i+2){
	s +=((myPacket->udpData[i]<<8)&0xFF00)+(myPacket->udpData[i+1]&0xFF);
    }

    for (i=0;i<4;i=i+2)
    {
          s +=  ((myPacket->ipUdpHeader.ip_dst_byte[i]<<8)&0xFF00) + (myPacket->ipUdpHeader.ip_dst_byte[i+1]&0xFF) ;
    }

    s += 0x11 + l; //
    while (s>>16)
	  s = (s & 0xFFFF)+(s >> 16);
//    sum = ~sum;
    //myPacket->ipUdpHeader.udpchksum = htons(~s);
    myPacket->ipUdpHeader.udpchksum = 0;
    //UDP
}

void procPacket(PACKET_STRUCT* myPacket)
{

    volatile unsigned char i,foundGateway;
    
    //Check & Verify IP-Packet
    if (myPacket->ipUdpHeader.ip_vhl != 0x45 || myPacket->ipUdpHeader.ip_p != 0x11)
      return;
    
    
    //dec TTL
    if((--myPacket->ipUdpHeader.ip_ttl)==0x00)   //Drop
            return;

    
    //myPacket->ipUdpHeader.ip_ttl--;

    //wenn subnet == 128.6.x.x -> MicaZ
    //wenn subnet == 128.5.x.x -> Thai
    //wenn subnet == 128.4.x.x -> PCT
    //wenn subnet == 128.3.x.x -> WiFi
    //wenn subnet == 128.2.x.x -> XBee
    //wenn subnet == 128.1.x.x -> 
    //wenn subnet == 128.0.x.x -> USB / WiFi
    //subnet == 129.x.x.x  -> fremdes Netz
    //if (myPacket->ipUdpHeader.ip_dst[0] == myip[0])

    //update checksum IP !!!  TODO

//         if (USBD_GetState()==0x05) 
//                RNDISDriver_Write(myPacket->data, myPacket->size, (TransferCallback) NULL, 0);
    //if (myPacket->ipUdpHeader.ip_dst[0] && 0xFF00 == (128<<8))  //Eigenes Netzwerk
    
    //if ( myPacket->ipUdpHeader.ip_dst[0]  == 0x0180  && myPacket->ipUdpHeader.ip_dst[1]  == 0x0100)
    if (myPacket->ipUdpHeader.ip_dst_byte[0] == 128 &&   //Eigenes Netzwerk
          myPacket->ipUdpHeader.ip_dst_byte[1] == 1 && 
          myPacket->ipUdpHeader.ip_dst_byte[2] == 0 && 
          myPacket->ipUdpHeader.ip_dst_byte[3] == 1)
          {
              myPacket->ipUdpHeader.ip_dst[0] = 0x007F;
              myPacket->ipUdpHeader.ip_dst[1] = 0x0100;
          }

    if (myPacket->ipUdpHeader.ip_dst_byte[0] == PlanetIP[0])  //Eigenes Netzwerk
    {
        switch (myPacket->ipUdpHeader.ip_dst_byte[1])
        {
            case PORT_XBEE:
                sendOverXbee(myPacket,NULL); 
                break;
            case PORT_PCT:
                break;
            case PORT_MICAZ:
                break;
            case PORT_DUMMY:
                sendOverDummy(myPacket,NULL); 
                break;
            case 0:
            case PORT_HOST:
                if (USBD_GetState()==0x05) /*USB*/
                {
                    calcIpChecksum(myPacket);
                    calcUdpChecksum(myPacket);
                    memcpy(myPacket->ethHeader.eth_mac_src, usbmac1,6);
                    memcpy(myPacket->ethHeader.eth_mac_dst, usbmac, 6);
                    myPacket->ethHeader.eth_protocol = 0x0008; //ipv4 0x0800
                    RNDISDriver_Write(myPacket->ethData, myPacket->size+sizeof(ETHERNET_HEADER), (TransferCallback) NULL, 0);
                }
            case PORT_WLAN:
                    i=0;
                    while (wifiTxData.status == 1)  //Warten, wenn TX-Paket schon ansteht
                    {

                          if (i++>10) 
                          {
                            wifiTxData.status = 0;
                            return;
                          }
                          vTaskDelay(10);
                    }
                    //Ehernet2-Header
                    calcIpChecksum(myPacket);
                    calcUdpChecksum(myPacket);
                    memcpy(myPacket->ethHeader.eth_mac_src, (unsigned char*) zg_get_mac(),6); //src-mac
                    memset(myPacket->ethHeader.eth_mac_dst, 0xFF, 6); //dst-mac
                    myPacket->ethHeader.eth_protocol = 0x0008; //ipv4 0x0800
                    memcpy(wifiTxData.ethData,myPacket->ethData,myPacket->size+sizeof(ETHERNET_HEADER)); //eth-packet-size
                    wifiTxData.size = myPacket->size; //interal packet size
                    wifiTxData.status = 1; //ready to send
                break;
        }
    }
    else
    {
        foundGateway = 0;
        for (i=0;i<ROUTING_ENTRIES;i++)
        {
            if (routingTable[i][0] == myPacket->ipUdpHeader.ip_dst_byte[0])
            {
                  foundGateway = 1;
                  //Zielnetzwerk gefunden
                  if (routingTable[i][1] == PORT_XBEE)
                      sendOverXbee(myPacket,*((unsigned short*) (&(routingTable[i][2])))); 
            }
        }
        if (foundGateway==0)
        {
            if (routingTable[0][0] == 0)  //hier muss es raus
            {
//                myPacket->ipUdpHeader.ip_ttl = 0x60;
//                myPacket->ipUdpHeader.srcport = 0x1127;                
//                myPacket->ipUdpHeader.ip_src[0] = htons(0x8001);
//                myPacket->ipUdpHeader.ip_src[1] = htons(0x0709);
                myPacket->ipUdpHeader.ip_dst[0] = htons(0x8001);
                myPacket->ipUdpHeader.ip_dst[1] = htons(0x0001);

                
//                for (i = 0;i<14;i++)
//                      myPacket->udpData[i] = 0x11;
                calcIpChecksum(myPacket);
                calcUdpChecksum(myPacket);
                if (USBD_GetState()==0x05) /*USB*/
                {
                    //Change IP
                    memcpy(myPacket->ethHeader.eth_mac_src, usbmac1,6);
                    memcpy(myPacket->ethHeader.eth_mac_dst, usbmac, 6);
                    myPacket->ethHeader.eth_protocol = 0x0008; //ipv4 0x0800
                    RNDISDriver_Write(myPacket->ethData, myPacket->size+sizeof(ETHERNET_HEADER), (TransferCallback) NULL, 0);
                }
                else // WLAN
                {
                    i=0;
                    while (wifiTxData.status == 1) //Warten, wenn TX-Paket schon ansteht
                    {

                        if (i++>100) 
                        {
                            wifiTxData.status = 0;
                            return;
                        }
                         vTaskDelay(10);
                    }
                    //Ehernet2-Header

                    memcpy(myPacket->ethHeader.eth_mac_src, (unsigned char*) zg_get_mac(),6); //src-mac
                    memset(myPacket->ethHeader.eth_mac_dst, 0xFF, 6); //dst-mac
                    myPacket->ethHeader.eth_protocol = 0x0008; //ipv4 0x0800
#if 1
                    memcpy(wifiTxData.ethData,myPacket->ethData,myPacket->size+sizeof(ETHERNET_HEADER)); //eth-packet-size
                    wifiTxData.size = myPacket->size; //interal packet size
                    wifiTxData.status = 1; //ready to send
#else
      //direktes Senden des Ethernet-Paketes über WLAN
      //TODO schützen mit Semaphore
            if (xSemaphoreTake(xWlan,1000) == pdTRUE)
            {
                zg_send(myPacket->ethData,(myPacket->size+sizeof(ETHERNET_HEADER)));
                xSemaphoreGive(xWlan);  
            }
#endif

                 }

            }
            else  // Zum Default Gateway
            {
              if (routingTable[0][1] == PORT_XBEE)
                  sendOverXbee(myPacket,*((unsigned short*) (&(routingTable[0][2])))); 
            }
        }
    
    }
}

