/* ----------------------------------------------------------------------------
 *		 ATMEL Microcontroller Software Support 
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/*
	Title: RNDISDriver implementation
*/

#include "RNDISDriver.h"
#ifndef NULL
#define NULL 0
#endif

// supported OIDs
static const long oid_supported_list[] = {

	// mandatory generics
	OID_GEN_SUPPORTED_LIST,
	OID_GEN_HARDWARE_STATUS,
	OID_GEN_MEDIA_SUPPORTED,
	OID_GEN_MEDIA_IN_USE,
//	OID_GEN_MAXIMUM_LOOKAHEAD,
	OID_GEN_MAXIMUM_FRAME_SIZE,
	OID_GEN_LINK_SPEED,
//	OID_GEN_TRANSMIT_BUFFER_SPACE,
//	OID_GEN_RECEIVE_BUFFER_SPACE,
	OID_GEN_TRANSMIT_BLOCK_SIZE,
	OID_GEN_RECEIVE_BLOCK_SIZE,
	OID_GEN_VENDOR_ID,
	OID_GEN_VENDOR_DESCRIPTION,
	OID_GEN_VENDOR_DRIVER_VERSION,
	OID_GEN_CURRENT_PACKET_FILTER,
//	OID_GEN_CURRENT_LOOKAHEAD,
//	OID_GEN_DRIVER_VERSION,
	OID_GEN_MAXIMUM_TOTAL_SIZE,
//	OID_GEN_PROTOCOL_OPTIONS,
	OID_GEN_MEDIA_CONNECT_STATUS,
	OID_GEN_PHYSICAL_MEDIUM,
//	OID_GEN_MAXIMUM_SEND_PACKETS,
//	OID_GEN_VENDOR_DRIVER_VERSION,
//	OID_GEN_SUPPORTED_GUIDS,
//	OID_GEN_NETWORK_LAYER_ADDRESSES,
//	OID_GEN_TRANSPORT_HEADER_OFFSET,
//	OID_GEN_MACHINE_NAME,
//	OID_GEN_RNDIS_CONFIG_PARAMETER,
//	OID_GEN_VLAN_ID,

	// mandatory statistics
	OID_GEN_XMIT_OK,
	OID_GEN_RCV_OK,
	OID_GEN_XMIT_ERROR,
	OID_GEN_RCV_ERROR,
	OID_GEN_RCV_NO_BUFFER,

	// mandatory ethernet-specifics
	OID_802_3_PERMANENT_ADDRESS,
	OID_802_3_CURRENT_ADDRESS,
	OID_802_3_MULTICAST_LIST,
	OID_802_3_MAC_OPTIONS,
	OID_802_3_MAXIMUM_LIST_SIZE,
	OID_802_3_RCV_ERROR_ALIGNMENT,
	OID_802_3_XMIT_ONE_COLLISIONS,
	OID_802_3_MORE_COLLISIONS,

	OID_PNP_CAPABILITIES,
	OID_PNP_QUERY_POWER,
	OID_PNP_SET_POWER,
	OID_PNP_ENABLE_WAKE_UP,
	OID_PNP_ADD_WAKE_UP_PATTERN,
	OID_PNP_REMOVE_WAKE_UP_PATTERN

//	OID_GEN_DIRECTED_BYTES_XMIT,
//	OID_GEN_DIRECTED_FRAMES_XMIT,
//	OID_GEN_DIRECTED_BYTES_RCV,
//	OID_GEN_DIRECTED_FRAMES_RCV,
	
};

// Static instance of the RNDIS driver
static RNDISDriver rndisDriver;

// callback for USB requests
void USBDCallbacks_RequestReceived(const USBGenericRequest *request) {
	// just give this function another name
	RNDISDriver_RequestHandler(request);
}
void RNDISDriver_RequestHandler(const USBGenericRequest *request) {

	trace_LOG(trace_DEBUG, "NewReq %x ", ((unsigned char*) request)[0]);
	trace_LOG(trace_DEBUG, "\n\r");
	
	// Handle the request
	switch (request->bmRequestType) {

		// Encapsulated Command wird gesendet --> nun empfangen
		case RNDIS_ENCAPSULATED_COMMAND:
			//trace_LOG(trace_DEBUG, "Enc com, len: %d\n\r", request->wLength);
			rndisDriver.bufLen = request->wLength;
			USBD_Read(0, rndisDriver.buf, request->wLength, (TransferCallback) RNDISDriver_ProcessEncapsulatedCommand, (void *) 0);

			break;
		
		case RNDIS_ENCAPSULATED_RESPONSE:
			//trace_LOG(trace_DEBUG, "RNDIS_GET_ENCAPSULATED_RESPONSE received\n\r");
			RNDISDriver_SendEncapsulatedResponse();
			
			break;

		default:
			trace_LOG(trace_INFO, "Default / unknown request. requesttype: %x, request: %x, wvalue: %x, windex: %x, wlength: %x\n\r", request->bmRequestType, request->bRequest, request->wValue, request->wIndex, request->wLength);
			USBDDriver_RequestHandler(&(rndisDriver.usbdDriver), request);
			break;
	}
}

// RNDIS state machine
void RNDISDriver_SetState(unsigned char new_state) {
	if (rndisDriver.state == new_state)
		return;
	rndisDriver.state = new_state;
	switch (new_state) {
		case RNDIS_STATE_UNINITIALIZED:
			trace_LOG(trace_INFO, "state change to RNDIS_STATE_UNINITIALIZED\n\r");
			break;
		case RNDIS_STATE_INITIALIZED:
			trace_LOG(trace_INFO, "state change to RNDIS_STATE_INITIALIZED\n\r");
			break;
		case RNDIS_STATE_DATA_INITIALIZED:
			trace_LOG(trace_INFO, "state change to RNDIS_STATE_DATA_INITIALIZED\n\r");
			break;
		default:
			trace_LOG(trace_WARNING, "Coding error: state change to UNKNOWN\n\r");
			break;
	}
}

// Tell the host that an encapsulated answer is waiting
void RNDISDriver_WriteResponseAvailable() {
	unsigned char data[8];
	data[0] = 0x01;
	data[1] = 0x00;
	data[2] = 0x00;
	data[3] = 0x00;
	data[4] = 0x00;
	data[5] = 0x00;
	data[6] = 0x00;
	data[7] = 0x00;

	// The following two lines are the trickiest part of the whole driver.
	// Firstly, write a notification (RESPONSE_AVAILABLE, 8 bytes) to the interrupt endpoint (EP 3).
	// Then, write an *empty* message to the control endpoint (0). This message only gets polled after the
	// notification on EP 3.
	//
	// This is the only way that satisfies both Windows and Linux.
	
	USBD_Write(3, data, 8, (TransferCallback) RNDISDriver_WriteCallback, (void *) RNDIS_WRITECALLBACK_RESPONSEAVAILABLE);
	USBD_Write(0, data, 0, (TransferCallback) RNDISDriver_WriteCallback, (void *) RNDIS_WRITECALLBACK_NOTIFICATION);
}

/* Prepare an encapsulated response for transmission and store it in rndisDriver->buf / bufLen
 * Finally send it to the host */
void RNDISDriver_EncapsulatedResponse(unsigned char *srcBuf, unsigned short srcBufLen) {
	unsigned char *tBuf = rndisDriver.buf;
	int i;
	rndisDriver.bufLen = srcBufLen;

	trace_LOG(trace_DEBUG, "rspns: cnt=%d ", srcBufLen);
	for (i=0; i<srcBufLen; i++)
		trace_LOG(trace_DEBUG, "%02x ", srcBuf[i]);
	trace_LOG(trace_DEBUG, "\n\r");

	while (srcBufLen) {
		*tBuf = *srcBuf;
		srcBuf++;
		tBuf++;
		srcBufLen--;
	}
	RNDISDriver_WriteResponseAvailable();
}

// Callback after (un)successful write on USB
void RNDISDriver_WriteCallback(unsigned int argument, unsigned char status, unsigned int received, unsigned int remaining) {
	trace_LOG(trace_DEBUG, "Write callback: status %d, received %d bytes, %d remaining, argument %d\n\r", status, received, remaining, argument);
	if (remaining == 0)
		switch (argument) {
			case RNDIS_WRITECALLBACK_RESPONSEAVAILABLE:
				trace_LOG(trace_DEBUG, "RESPONSE_AVAILABLE successfully written\n\r");
				break;
			case RNDIS_WRITECALLBACK_NOTIFICATION:
				trace_LOG(trace_DEBUG, "NOTIFICATION successfully written\n\r");
				break;
			case RNDIS_WRITECALLBACK_ENCAPSULATEDRESPONSE:
				trace_LOG(trace_DEBUG, "ENCAPSULATED_RESPONSE successfully written\n\r");
				break;
			default:
				trace_LOG(trace_INFO, "Unknown command successfully written\n\r");
		}
}

/* RESPONSE FUNCTIONS */

// Write the "Init response" message
void RNDISDriver_InitResponse() {
	rndis_init_msg_type *query = (rndis_init_msg_type *) rndisDriver.buf;
	rndis_init_cmplt_type response;

	response.message_type = RNDIS_INITIALIZE_CMPLT;
	response.message_length = 52; // constant
	response.request_id = query->request_id;
	response.status = RNDIS_STATUS_SUCCESS;
	response.major_version = RNDIS_MAJOR_VERSION;
	response.minor_version = RNDIS_MINOR_VERSION;
	response.device_flags = RNDIS_DF_CONNECTIONLESS;
	response.medium = RNDIS_MEDIUM_802_3;
	// FIXME: allow multi-packet transfers
	response.max_packets_per_transfer = 1;
	// FIXME
	response.max_transfer_size = 256;
	response.packet_alignment_factor = 0;
	response.af_list_offset = 0;
	response.af_list_size = 0;
	RNDISDriver_EncapsulatedResponse((unsigned char *) &response, 52);
}

// Response to an OID "get" query
void RNDISDriver_QueryResponse() {

	rndis_query_msg_type *query = (rndis_query_msg_type *) rndisDriver.buf;
	unsigned char respBuf[24+sizeof(oid_supported_list)];
	rndis_query_cmplt_type *response = (rndis_query_cmplt_type *) respBuf;

	long *outbuf_long = (long *) &(response[1]);
	unsigned char *outbuf_char = (unsigned char *) &(response[1]);
	long *length = &(response->information_buffer_length);

	short count, i;

	response->message_type = RNDIS_QUERY_CMPLT;
	response->request_id = query->request_id;
	response->status = RNDIS_STATUS_SUCCESS;
	response->information_buffer_offset = 16;
	*length = 4; // common value

	switch (query->oid) {

		case OID_GEN_SUPPORTED_LIST:
			trace_LOG(trace_INFO, "query: OID_GEN_SUPPORTED_LIST\n\r");
			*length = sizeof(oid_supported_list);
			count = (*length)>>2;
			for (i=0; i<count; i++)
				outbuf_long[i] = oid_supported_list[i];
			break;

		case OID_GEN_HARDWARE_STATUS:
			trace_LOG(trace_INFO, "query: OID_GEN_HARDWARE_STATUS\n\r");
			// gadget hardware is obviously ready
			outbuf_long[0] = 0;
			break;
			
		case OID_GEN_MEDIA_SUPPORTED:
			trace_LOG(trace_INFO, "query: OID_GEN_MEDIA_SUPPORTED\n\r");
			// ethernet medium type is 0x0
			outbuf_long[0] = 0;
			break;

		case OID_GEN_MEDIA_IN_USE:
			trace_LOG(trace_INFO, "query: OID_GEN_MEDIA_IN_USE\n\r");
			// ethernet medium type is 0x0
			outbuf_long[0] = RNDIS_MEDIUM_802_3;
			break;

		case OID_GEN_MAXIMUM_FRAME_SIZE:
			trace_LOG(trace_INFO, "query: OID_GEN_MAXIMUM_FRAME_SIZE\n\r");
			outbuf_long[0] = RNDIS_MTU;
			break;

		case OID_GEN_LINK_SPEED:
			trace_LOG(trace_INFO, "query: OID_GEN_LINK_SPEED\n\r");
			// FIXME: make this dependent on state of link
			// better higher speed than less - 100 Mbps (1 unit means 100 bps)
			outbuf_long[0] = 1000000;
			break;

		case OID_GEN_TRANSMIT_BUFFER_SPACE:
		case OID_GEN_TRANSMIT_BLOCK_SIZE:
			trace_LOG(trace_INFO, "query: OID_GEN_TRANSMIT_BLOCK_SIZE\n\r");
			// FIXME: is that really the MTU?
			outbuf_long[0] = RNDIS_MTU;
			break;

		case OID_GEN_RECEIVE_BUFFER_SPACE:
		case OID_GEN_RECEIVE_BLOCK_SIZE:
			trace_LOG(trace_INFO, "query: OID_GEN_RECEIVE_BLOCK_SIZE\n\r");
			// FIXME: is that really the MTU?
			outbuf_long[0] = RNDIS_MTU;
			break;

		case OID_GEN_VENDOR_ID:
			trace_LOG(trace_INFO, "query: OID_GEN_VENDOR_ID\n\r");
			outbuf_long[0] = RNDISDriverDescriptors_VENDORID;
			break;

		case OID_GEN_VENDOR_DESCRIPTION:
			trace_LOG(trace_INFO, "query: OID_GEN_VENDOR_DESCRIPTION\n\r");
			// memcpy
			*length=0;
			char *ptr = vendor_description;
			while (*ptr != 0)
				outbuf_char[(*length)++] = *(ptr++);
			break;

		case OID_GEN_CURRENT_PACKET_FILTER:
			trace_LOG(trace_INFO, "query: OID_GEN_CURRENT_PACKET_FILTER\n\r");
			outbuf_long[0] = rndisDriver.filter;
			break;

		case OID_GEN_MAXIMUM_TOTAL_SIZE:
			trace_LOG(trace_INFO, "query: OID_GEN_MAXIMUM_TOTAL_SIZE\n\r");
			// FIXME: is it right to return the MTU?
			outbuf_long[0] = RNDIS_MTU;
			break;

		case OID_GEN_MEDIA_CONNECT_STATUS:
			trace_LOG(trace_INFO, "query: OID_GEN_MEDIA_CONNECT_STATUS\n\r");
			// FIXME: make this link-state dependent
			outbuf_long[0] = rndisDriver.link_state;
			break;

		// statistical OIDs
		case OID_GEN_XMIT_OK:
			trace_LOG(trace_INFO, "query: OID_GEN_XMIT_OK = %04x%04x\n\r",(unsigned int) (rndisDriver.no_xmit_frames>>16), (unsigned int) (rndisDriver.no_xmit_frames & 0xFFFF));
			outbuf_long[0] = rndisDriver.no_xmit_frames;
			break;

		case OID_GEN_RCV_OK:
			trace_LOG(trace_INFO, "query: OID_GEN_RCV_OK = %04x%04x\n\r",(unsigned int) (rndisDriver.no_recv_frames>>16), (unsigned int) (rndisDriver.no_recv_frames & 0xFFFF));
			outbuf_long[0] = rndisDriver.no_recv_frames;
			break;

		case OID_GEN_XMIT_ERROR:
			trace_LOG(trace_INFO, "query: OID_GEN_XMIT_ERROR = %04x%04x\n\r",(unsigned int) (rndisDriver.no_xmit_errors>>16), (unsigned int) (rndisDriver.no_xmit_errors & 0xFFFF));
			outbuf_long[0] = rndisDriver.no_xmit_errors;
			break;

		case OID_GEN_RCV_ERROR:
			trace_LOG(trace_INFO, "query: OID_GEN_RCV_ERROR = %04x%04x\n\r",(unsigned int) (rndisDriver.no_recv_errors>>16), (unsigned int) (rndisDriver.no_recv_errors & 0xFFFF));
			outbuf_long[0] = rndisDriver.no_recv_errors;
			break;

		case OID_GEN_RCV_NO_BUFFER:
			trace_LOG(trace_INFO, "query: OID_GEN_RCV_NO_BUFFER = %04x%04x\n\r",(unsigned int) (rndisDriver.no_frames_missed>>16), (unsigned int) (rndisDriver.no_frames_missed & 0xFFFF));
			outbuf_long[0] = rndisDriver.no_frames_missed;
			break;

		// ethernet OIDs
		case OID_802_3_PERMANENT_ADDRESS:
			trace_LOG(trace_INFO, "query: OID_802_3_PERMANENT_ADDRESS\n\r");
			*length = 6;
			for (i=0; i<6; i++)
				outbuf_char[i] = rndisDriver.mac_addr[i];
			break;

		case OID_802_3_CURRENT_ADDRESS:
			trace_LOG(trace_INFO, "query: OID_802_3_CURRENT_ADDRESS\n\r");
			*length = 6;
			for (i=0; i<6; i++)
				outbuf_char[i] = rndisDriver.mac_addr[i];
			break;

		case OID_802_3_MULTICAST_LIST:
			trace_LOG(trace_INFO, "query: OID_802_3_MULTICAST_LIST\n\r");
			// just return multicast base adrress
			outbuf_long[0] = 0xE0000000;
			break;

		case OID_802_3_MAXIMUM_LIST_SIZE:
			trace_LOG(trace_INFO, "query: OID_802_3_MAXIMUM_LIST_SIZE\n\r");
			// maximum one address --> virtually don't support multicast
			outbuf_long[0] = 1;
			break;

		case OID_802_3_RCV_ERROR_ALIGNMENT:
			trace_LOG(trace_INFO, "query: OID_802_3_RCV_ERROR_ALIGNMENT\n\r");
			// hope that's okay...
			outbuf_long[0] = 0;
			break;

		case OID_802_3_XMIT_ONE_COLLISIONS:
			trace_LOG(trace_INFO, "query: OID_802_3_XMIT_ONE_COLLISIONS\n\r");
			// hope that's okay...
			outbuf_long[0] = 0;
			break;

		case OID_802_3_MORE_COLLISIONS:
			trace_LOG(trace_INFO, "query: OID_802_3_MORE_COLLISIONS\n\r");
			// hope that's okay...
			outbuf_long[0] = 0;
			break;

		// obviously also mandatory requests (in contrast to MS RNDIS spec!)
		case OID_GEN_MAXIMUM_SEND_PACKETS:
			trace_LOG(trace_INFO, "query: OID_GEN_MAXIMUM_SEND_PACKETS\n\r");
			outbuf_long[0] = 1;
			break;

		case OID_GEN_MAXIMUM_LOOKAHEAD:
			trace_LOG(trace_INFO, "query: OID_GEN_MAXIMUM_LOOKAHEAD\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_SUPPORTED_GUIDS:
			trace_LOG(trace_INFO, "query: OID_GEN_SUPPORTED_GUIDS\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_CURRENT_LOOKAHEAD:
			trace_LOG(trace_INFO, "query: OID_GEN_CURRENT_LOOKAHEAD\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_VENDOR_DRIVER_VERSION:
			trace_LOG(trace_INFO, "query: OID_GEN_VENDOR_DRIVER_VERSION\n\r");
			outbuf_long[0] = 1;
			break;

		case OID_GEN_MAC_OPTIONS:
			trace_LOG(trace_INFO, "query: OID_GEN_MAC_OPTIONS\n\r");
			outbuf_long[0] = 0x00000012;
			break;

		case OID_GEN_DIRECTED_BYTES_XMIT:
			trace_LOG(trace_INFO, "query: OID_GEN_DIRECTED_BYTES_XMIT\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_DIRECTED_FRAMES_XMIT:
			trace_LOG(trace_INFO, "query: OID_GEN_DIRECTED_FRAMES_XMIT\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_DIRECTED_BYTES_RCV:
			trace_LOG(trace_INFO, "query: OID_GEN_DIRECTED_BYTES_RCV\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_DIRECTED_FRAMES_RCV:
			trace_LOG(trace_INFO, "query: OID_GEN_DIRECTED_FRAMES_RCV\n\r");
			outbuf_long[0] = 0;
			break;

		case OID_GEN_PHYSICAL_MEDIUM:
			trace_LOG(trace_INFO, "query: OID_GEN_PHYSICAL_MEDIUM\n\r");
			outbuf_long[0] = 0;
			break;

		default:
			// OID not supported
			trace_LOG(trace_INFO, "unknown OID queried: %04x%04x\n\r", (unsigned int) ((query->oid)>>16), (unsigned int) (query->oid & 0xFFFF));
			response->status = RNDIS_STATUS_NOT_SUPPORTED;
			*length = 0;
			break;
	}

	response->message_length = 24 + response->information_buffer_length;


	RNDISDriver_EncapsulatedResponse((unsigned char *) response,response->message_length);
}

// response to an OID "set" query
void RNDISDriver_SetResponse() {
	rndis_set_msg_type *query = (rndis_set_msg_type *) rndisDriver.buf;
	rndis_set_cmplt_type response;

	unsigned char *inbuf = (unsigned char *) (&(query[1]));

	response.message_type = RNDIS_SET_CMPLT;
	response.message_length = 16;
	response.request_id = query->request_id;
	response.status = RNDIS_STATUS_SUCCESS;

	switch (query->oid) {

		case OID_GEN_CURRENT_PACKET_FILTER:
			/* The most important SET command. If parameter is non-empty, then start transfer.
			 * If empty, stop transfer. Accordingly, state machine must be triggered
			 */
			rndisDriver.filter = *((long *) inbuf);
			trace_LOG(trace_INFO, "set: OID_GEN_CURRENT_PACKET_FILTER, filter=%04x%04x\n\r",(unsigned int) (rndisDriver.filter>>16), (unsigned int) (rndisDriver.filter & 0xFFFF));

			// now start / stop transfer
			if (rndisDriver.filter)
				// start
				RNDISDriver_SetState(RNDIS_STATE_DATA_INITIALIZED);
			else
				// stop
				RNDISDriver_SetState(RNDIS_STATE_INITIALIZED);
			break;

		case OID_GEN_RNDIS_CONFIG_PARAMETER:
			trace_LOG(trace_INFO, "set: OID_GEN_RNDIS_CONFIG_PARAMETER, parameter=%04x%04x\n\r",(unsigned int) ((*((long *) inbuf))>>16), (unsigned int) (*((long *) inbuf) & 0xFFFF));
			break;

		case OID_GEN_MACHINE_NAME:
			trace_LOG(trace_INFO, "set: OID_GEN_MACHINE_NAME\n\r");
			break;

		case OID_GEN_NETWORK_LAYER_ADDRESSES:
			trace_LOG(trace_INFO, "set: OID_GEN_NETWORK_LAYER_ADDRESSES\n\r");
			break;

		case OID_GEN_TRANSPORT_HEADER_OFFSET:
			trace_LOG(trace_INFO, "set: OID_GEN_TRANSPORT_HEADER_OFFSET\n\r");
			break;

		default:
			trace_LOG(trace_INFO, "unknown OID set request: %04x%04x\n\r", (unsigned int) ((query->oid)>>16), (unsigned int) (query->oid & 0xFFFF));
			response.status = RNDIS_STATUS_NOT_SUPPORTED;
			break;

	}

	RNDISDriver_EncapsulatedResponse((unsigned char *) &response,response.message_length);
}

// response to a "reset" message
void RNDISDriver_ResetResponse() {
	rndis_reset_cmplt_type response;
	response.message_type = RNDIS_RESET_CMPLT;
	response.message_length = 16;
	response.status = RNDIS_STATUS_SUCCESS;
	response.addressing_reset = 1;
	RNDISDriver_EncapsulatedResponse((unsigned char *) &response,response.message_length);
}

// response to a "keep alive" message
void RNDISDriver_KeepaliveResponse() {
	rndis_keepalive_cmplt_type response;
	rndis_keepalive_msg_type *query = (rndis_keepalive_msg_type *) rndisDriver.buf;
	response.message_type = RNDIS_KEEPALIVE_CMPLT;
	response.message_length = 16;
	response.request_id = query->request_id;
	response.status = RNDIS_STATUS_SUCCESS;
	RNDISDriver_EncapsulatedResponse((unsigned char *) &response,response.message_length);
}

// send "indicate status" message when link state has changed
void RNDISDriver_IndicateStatus() {
	rndis_indicate_status_msg_type response;
	response.message_type = RNDIS_INDICATE_STATUS_MSG;
	response.message_length = 28;
	response.status = RNDIS_STATUS_SUCCESS;
	response.status_buffer_length = 8;
	response.status_buffer_offset = 12;
	response.diag_status = rndisDriver.link_state;
	response.error_offset = 0;
	RNDISDriver_EncapsulatedResponse((unsigned char *) &response,response.message_length);
}


// Callback, processing encapsulated data (after GetEncapsulatedCommand)
void RNDISDriver_ProcessEncapsulatedCommand(unsigned int unused, unsigned char status, unsigned int received, unsigned int remaining) {

	unsigned int i;

	if (status == USBD_STATUS_SUCCESS) {
		trace_LOG(trace_DEBUG, "Enc Cmd: received %d bytes\n\r", received);

		// Check if bytes have been discarded
		if ((received == rndisDriver.bufLen) && (remaining > 0))
			trace_LOG(trace_WARNING, "Encapsulated Cmd: %u bytes discarded\n\r",remaining);

		// message mustn't be too short
		if (received >= 4) {

			// classify the request
			switch (((unsigned long *) rndisDriver.buf)[0]) {

				case RNDIS_INITIALIZE_MSG:
					trace_LOG(trace_INFO, "RNDIS_INIT_MSG received\n\r");
					RNDISDriver_SetState(RNDIS_STATE_INITIALIZED);
					RNDISDriver_InitResponse();
					break;

				case RNDIS_HALT_MSG:
					trace_LOG(trace_INFO, "RNDIS_HALT_MSG received\n\r");
					RNDISDriver_SetState(RNDIS_STATE_UNINITIALIZED);
					USBD_Disconnect();
					break;

				case RNDIS_QUERY_MSG:
					trace_LOG(trace_DEBUG, "RNDIS_QUERY_MSG received\n\r");
					RNDISDriver_QueryResponse();
					break;

				case RNDIS_SET_MSG:
					trace_LOG(trace_DEBUG, "RNDIS_SET_MSG received\n\r");
					RNDISDriver_SetResponse();
					break;

				case RNDIS_RESET_MSG:
					trace_LOG(trace_INFO, "RNDIS_RESET_MSG received\n\r");
					RNDISDriver_ResetResponse();
					break;

				case RNDIS_KEEPALIVE_MSG:
					trace_LOG(trace_INFO, "RNDIS_KEEPALIVE_MSG received\n\r");
					RNDISDriver_KeepaliveResponse();
					break;

				default:
					trace_LOG(trace_WARNING, "Unknown RNDIS message received: len: %d, buf:", rndisDriver.bufLen);
					for (i=0; i<rndisDriver.bufLen; i++)
						trace_LOG(trace_WARNING, " %02x", rndisDriver.buf[i]);
					trace_LOG(trace_WARNING, "\n\r");
					break;
			}
		}
	}
	else {
		trace_LOG(trace_WARNING, "Encapsulated Cmd: Transfer error\n\r");
	}
}


/* Transmit an encapsulated response, which has already been prepared in rndisDriver->buf and rndisDriver->bufLen
 * precondition: host already sent a GET_ENCAPSULATED_RESPONSE message */
void RNDISDriver_SendEncapsulatedResponse() {
	USBD_Write(0, rndisDriver.buf, rndisDriver.bufLen, (TransferCallback) RNDISDriver_WriteCallback, (void *) RNDIS_WRITECALLBACK_ENCAPSULATEDRESPONSE);
}

// Initialization
// @param link_state: either RNDIS_STATUS_MEDIA_CONNECT or RNDIS_STATUS_MEDIA_DISCONNECT
// @param mac_addr: pointer to initial mac address (6 data bytes, no termination)
void RNDISDriver_Initialize(long link_state, char *mac_addr) {

	short i;

	trace_LOG(trace_INFO, "RNDISDriver_Initialize\n\r");

	// Initialize the standard driver
	USBDDriver_Initialize(&(rndisDriver.usbdDriver),
			&rndisDriverDescriptors,
			0); // Multiple settings for interfaces not supported

	// Initialize the USB driver
	USBD_Init();

	// initialize RNDIS state machine
	rndisDriver.state = RNDIS_STATE_UNINITIALIZED;

	// initialize statistics
	rndisDriver.no_recv_frames = 0;
	rndisDriver.no_xmit_frames = 0;
	rndisDriver.no_recv_errors = 0;
	rndisDriver.no_xmit_errors = 0;
	rndisDriver.no_frames_missed = 0;

	for (i=0; i<6; i++)
		rndisDriver.mac_addr[i] = mac_addr[i];

	rndisDriver.filter = 0;
	rndisDriver.link_state = link_state;
}

// exported function: call whenever ethernet link got engaged or disengaged
// @param link_state: either RNDIS_STATUS_MEDIA_CONNECT or RNDIS_STATUS_MEDIA_DISCONNECT
void RNDISDriver_SetLinkState(long link_state) {
	rndisDriver.link_state = link_state;
	RNDISDriver_IndicateStatus();
}

// read DATA from USB
unsigned char RNDISDriver_Read(void *data, unsigned int size, TransferCallback callback, void *argument) {
//	trace_LOG(trace_INFO, "setting read callback...\n\r");
	rndisDriver.read_callback = callback;
	// use DOWN / OUT bulk data endpoint
	return USBD_Read(1, data, size, (TransferCallback) RNDISDriver_DummyReadCallback, argument);
}
// necessary for statistics
void RNDISDriver_DummyReadCallback(void *argument, unsigned char status, unsigned int received, unsigned int remaining) {
	trace_LOG(trace_DEBUG, "invoking read callback... status=%x, received=%x, remaining=%x\n\r", status, received, remaining);
	rndisDriver.no_recv_frames++;
	if (rndisDriver.read_callback != NULL)
		rndisDriver.read_callback(argument, status, received, remaining);
}

// write DATA to USB
unsigned char RNDISDriver_Write(void *data, unsigned int size, TransferCallback callback, void *argument) {
	trace_LOG(trace_DEBUG, "setting write callback...\n\r");
	// use UP / IN bulk data endpoint. Only write if in DATA_INITIALIZED state
	if (rndisDriver.state == RNDIS_STATE_DATA_INITIALIZED) {

		int i;
		
		// create the RNDIS_PACKET_MSG buffer (max. 256 payload bytes + 44 bytes header)
		unsigned char msgbuf[300];
		// pointer to the structure
		rndis_packet_msg_type *structure = (rndis_packet_msg_type *) &msgbuf;
		// pointer to the payload
		unsigned char *payload = msgbuf + 44;

		// fill in header data
		structure->message_type = RNDIS_PACKET_MSG;
		structure->message_length = size + 44;
		structure->data_offset = 36;
		structure->data_length = size;
		structure->oob_data_offset = 0;
		structure->oob_data_length = 0;
		structure->num_oob_data_elements = 0;
		structure->per_packet_info_offset = 0;
		structure->per_packet_info_length = 0;
		structure->vc_handle = 0;
		structure->reserved = 0;

		// copy payload
		for (i=0; i<size; i++)
			payload[i] = ((unsigned char *) data)[i];

		rndisDriver.write_callback = callback;
		//return USBD_Write(2, data, size, (TransferCallback) RNDISDriver_DummyWriteCallback, argument);
                return USBD_Write(2, structure, structure->message_length, (TransferCallback) RNDISDriver_DummyWriteCallback, argument);
	}
	else
		return 0;
}
// necessary for statistics
void RNDISDriver_DummyWriteCallback(void *argument, unsigned char status, unsigned int received, unsigned int remaining) {
	trace_LOG(trace_DEBUG, "invoking write callback... status=%x, received=%x, remaining=%x\n\r", status, received, remaining);
	rndisDriver.no_xmit_frames++;
	if (rndisDriver.write_callback != NULL)
		rndisDriver.write_callback(argument, status, received, remaining);
}

