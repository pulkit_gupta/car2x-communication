/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support 
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/*
    Title: RNDISDriver

    About: Purpose
        Definition of a class for implementing a USB device CDC serial driver.

    About: Usage
        1 - Re-implement the <USBDCallbacks_RequestReceived> method to pass
            received requests to <RNDISDriver_RequestHandler>. *This is
            automatically done unless the NOAUTOCALLBACK symbol is defined*.
        2 - Initialize the CDC serial and USB drivers using
            <RNDISDriver_Initialize>.
        3 - Logically connect the device to the host using <USBD_Connect>.
        4 - Send serial data to the USB host using <RNDISDriver_Write>.
        5 - Receive serial data from the USB host using <RNDISDriver_Read>.
*/

#ifndef RNDISDRIVER_H
#define RNDISDRIVER_H

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "RNDISDriverDescriptors.h"
#include <utility/trace.h>
#include <utility/assert.h>
#include <usb/device/core/USBDDriver.h>
#include <usb/common/cdc/CDCLineCoding.h>
#include <usb/common/cdc/CDCGenericRequest.h>
#include <usb/common/cdc/CDCSetControlLineStateRequest.h>
#include <usb/common/core/USBGenericRequest.h>
#include <usb/device/core/USBD.h>

//------------------------------------------------------------------------------
//         Definitions
//------------------------------------------------------------------------------


#define RNDIS_ENCAPSULATED_COMMAND	0x21
#define RNDIS_ENCAPSULATED_RESPONSE	0xA1

// states
#define RNDIS_STATE_UNINITIALIZED	0x00
#define RNDIS_STATE_INITIALIZED		0x01
#define RNDIS_STATE_DATA_INITIALIZED	0x02

// write callback arguments
#define RNDIS_WRITECALLBACK_RESPONSEAVAILABLE		0x00
#define RNDIS_WRITECALLBACK_NOTIFICATION		0x01
#define RNDIS_WRITECALLBACK_ENCAPSULATEDRESPONSE	0x02

// message types

/* Message Set for Connectionless (802.3) Devices */
#define RNDIS_PACKET_MSG          0x00000001U
#define RNDIS_INITIALIZE_MSG      0x00000002U     /* Initialize device */
#define RNDIS_HALT_MSG            0x00000003U
#define RNDIS_QUERY_MSG           0x00000004U
#define RNDIS_SET_MSG             0x00000005U
#define RNDIS_RESET_MSG           0x00000006U
#define RNDIS_INDICATE_STATUS_MSG 0x00000007U
#define RNDIS_KEEPALIVE_MSG       0x00000008U

/* Message completion */
#define RNDIS_INITIALIZE_CMPLT    0x80000002U
#define RNDIS_QUERY_CMPLT         0x80000004U
#define RNDIS_SET_CMPLT           0x80000005U
#define RNDIS_RESET_CMPLT         0x80000006U
#define RNDIS_KEEPALIVE_CMPLT     0x80000008U

/* RNDIS version */
#define RNDIS_MAJOR_VERSION	1
#define RNDIS_MINOR_VERSION	0

/* MTU. FIXME: is that ok? */
#define RNDIS_MTU		256

/* return / status values */
#define RNDIS_STATUS_SUCCESS            0x00000000U
#define RNDIS_STATUS_FAILURE            0xC0000001U
#define RNDIS_STATUS_INVALID_DATA       0xC0010015U
#define RNDIS_STATUS_NOT_SUPPORTED      0xC00000BBU
#define RNDIS_STATUS_MEDIA_CONNECT      0x4001000BU
#define RNDIS_STATUS_MEDIA_DISCONNECT   0x4001000CU

/* device flags */
#define RNDIS_DF_CONNECTIONLESS		1
#define RNDIS_DF_CONNECTION_ORIENTED	2

/* medium */
#define RNDIS_MEDIUM_802_3		0

/* OIDs */
/* Required Object IDs (OIDs) */
#define OID_GEN_SUPPORTED_LIST            0x00010101
#define OID_GEN_HARDWARE_STATUS           0x00010102
#define OID_GEN_MEDIA_SUPPORTED           0x00010103
#define OID_GEN_MEDIA_IN_USE              0x00010104
#define OID_GEN_MAXIMUM_LOOKAHEAD         0x00010105
#define OID_GEN_MAXIMUM_FRAME_SIZE        0x00010106
#define OID_GEN_LINK_SPEED                0x00010107
#define OID_GEN_TRANSMIT_BUFFER_SPACE     0x00010108
#define OID_GEN_RECEIVE_BUFFER_SPACE      0x00010109
#define OID_GEN_TRANSMIT_BLOCK_SIZE       0x0001010A
#define OID_GEN_RECEIVE_BLOCK_SIZE        0x0001010B
#define OID_GEN_VENDOR_ID                 0x0001010C
#define OID_GEN_VENDOR_DESCRIPTION        0x0001010D
#define OID_GEN_CURRENT_PACKET_FILTER     0x0001010E
#define OID_GEN_CURRENT_LOOKAHEAD         0x0001010F
#define OID_GEN_DRIVER_VERSION            0x00010110
#define OID_GEN_MAXIMUM_TOTAL_SIZE        0x00010111
#define OID_GEN_PROTOCOL_OPTIONS          0x00010112
#define OID_GEN_MAC_OPTIONS               0x00010113
#define OID_GEN_MEDIA_CONNECT_STATUS      0x00010114
#define OID_GEN_MAXIMUM_SEND_PACKETS      0x00010115
#define OID_GEN_VENDOR_DRIVER_VERSION     0x00010116
#define OID_GEN_SUPPORTED_GUIDS           0x00010117
#define OID_GEN_NETWORK_LAYER_ADDRESSES   0x00010118
#define OID_GEN_TRANSPORT_HEADER_OFFSET   0x00010119
#define OID_GEN_MACHINE_NAME              0x0001021A
#define OID_GEN_RNDIS_CONFIG_PARAMETER    0x0001021B
#define OID_GEN_VLAN_ID                   0x0001021C

/* Optional OIDs */
#define OID_GEN_MEDIA_CAPABILITIES        0x00010201
#define OID_GEN_PHYSICAL_MEDIUM           0x00010202

/* Required statistics OIDs */
#define OID_GEN_XMIT_OK                   0x00020101
#define OID_GEN_RCV_OK                    0x00020102
#define OID_GEN_XMIT_ERROR                0x00020103
#define OID_GEN_RCV_ERROR                 0x00020104
#define OID_GEN_RCV_NO_BUFFER             0x00020105

/* Optional statistics OIDs */
#define OID_GEN_DIRECTED_BYTES_XMIT       0x00020201
#define OID_GEN_DIRECTED_FRAMES_XMIT      0x00020202
#define OID_GEN_MULTICAST_BYTES_XMIT      0x00020203
#define OID_GEN_MULTICAST_FRAMES_XMIT     0x00020204
#define OID_GEN_BROADCAST_BYTES_XMIT      0x00020205
#define OID_GEN_BROADCAST_FRAMES_XMIT     0x00020206
#define OID_GEN_DIRECTED_BYTES_RCV        0x00020207
#define OID_GEN_DIRECTED_FRAMES_RCV       0x00020208
#define OID_GEN_MULTICAST_BYTES_RCV       0x00020209
#define OID_GEN_MULTICAST_FRAMES_RCV      0x0002020A
#define OID_GEN_BROADCAST_BYTES_RCV       0x0002020B
#define OID_GEN_BROADCAST_FRAMES_RCV      0x0002020C
#define OID_GEN_RCV_CRC_ERROR             0x0002020D
#define OID_GEN_TRANSMIT_QUEUE_LENGTH     0x0002020E
#define OID_GEN_GET_TIME_CAPS             0x0002020F
#define OID_GEN_GET_NETCARD_TIME          0x00020210
#define OID_GEN_NETCARD_LOAD              0x00020211
#define OID_GEN_DEVICE_PROFILE            0x00020212
#define OID_GEN_INIT_TIME_MS              0x00020213
#define OID_GEN_RESET_COUNTS              0x00020214
#define OID_GEN_MEDIA_SENSE_COUNTS        0x00020215
#define OID_GEN_FRIENDLY_NAME             0x00020216
#define OID_GEN_MINIPORT_INFO             0x00020217
#define OID_GEN_RESET_VERIFY_PARAMETERS   0x00020218

/* IEEE 802.3 (Ethernet) OIDs */
#define OID_802_3_PERMANENT_ADDRESS       0x01010101
#define OID_802_3_CURRENT_ADDRESS         0x01010102
#define OID_802_3_MULTICAST_LIST          0x01010103
#define OID_802_3_MAXIMUM_LIST_SIZE       0x01010104
#define OID_802_3_MAC_OPTIONS             0x01010105
#define OID_802_3_RCV_ERROR_ALIGNMENT     0x01020101
#define OID_802_3_XMIT_ONE_COLLISIONS      0x01020102
#define OID_802_3_MORE_COLLISIONS    0x01020103
#define OID_802_3_XMIT_DEFERRED           0x01020201
#define OID_802_3_XMIT_MAX_COLLISIONS     0x01020202
#define OID_802_3_RCV_OVERRUN             0x01020203
#define OID_802_3_XMIT_UNDERRUN           0x01020204
#define OID_802_3_XMIT_HEARTBEAT_FAILURE  0x01020205
#define OID_802_3_XMIT_TIMES_CRS_LOST     0x01020206
#define OID_802_3_XMIT_LATE_COLLISIONS    0x01020207


/* packet filter */
#define NDIS_PACKET_TYPE_DIRECTED       0x00000001
#define NDIS_PACKET_TYPE_MULTICAST      0x00000002
#define NDIS_PACKET_TYPE_ALL_MULTICAST  0x00000004
#define NDIS_PACKET_TYPE_BROADCAST      0x00000008
#define NDIS_PACKET_TYPE_SOURCE_ROUTING 0x00000010
#define NDIS_PACKET_TYPE_PROMISCUOUS    0x00000020
#define NDIS_PACKET_TYPE_SMT            0x00000040
#define NDIS_PACKET_TYPE_ALL_LOCAL      0x00000080
#define NDIS_PACKET_TYPE_GROUP          0x00000100
#define NDIS_PACKET_TYPE_ALL_FUNCTIONAL 0x00000200
#define NDIS_PACKET_TYPE_FUNCTIONAL     0x00000400
#define NDIS_PACKET_TYPE_MAC_FRAME      0x00000800

/* state of link */
#define NDIS_MEDIA_STATE_CONNECTED	0
#define NDIS_MEDIA_STATE_DISCONNECTED	1


#define OID_PNP_CAPABILITIES                    0xFD010100
#define OID_PNP_SET_POWER                       0xFD010101
#define OID_PNP_QUERY_POWER                     0xFD010102
#define OID_PNP_ADD_WAKE_UP_PATTERN             0xFD010103
#define OID_PNP_REMOVE_WAKE_UP_PATTERN          0xFD010104
#define OID_PNP_ENABLE_WAKE_UP                  0xFD010106
 
//------------------------------------------------------------------------------
//      Global data structure
//------------------------------------------------------------------------------
typedef struct {

	USBDDriver usbdDriver;

	// buffer for every request
	unsigned char buf[256];
	unsigned short bufLen;

	// state machine of RNDIS
	unsigned char state;
	
	// statistical stuff
	long no_recv_frames;
	long no_xmit_frames;
	long no_recv_errors;
	long no_xmit_errors;
	long no_frames_missed;

	// mac addr
	unsigned char mac_addr[6];
	// filter
	long filter;
	// media connected?
	long link_state;

	// cache callback functions
	TransferCallback read_callback, write_callback;

} RNDISDriver;

//------------------------------------------------------------------------------
//      Data structures - for RNDIS messages
//------------------------------------------------------------------------------

typedef struct rndis_init_msg_type {
        long message_type;
        long message_length;
        long request_id;
        long major_version;
        long minor_version;
        long max_transfer_size;
} rndis_init_msg_type;

typedef struct rndis_init_cmplt_type {
        long message_type;
        long message_length;
        long request_id;
        long status;
        long major_version;
        long minor_version;
        long device_flags;
        long medium;
        long max_packets_per_transfer;
        long max_transfer_size;
        long packet_alignment_factor;
        long af_list_offset;
        long af_list_size;
} rndis_init_cmplt_type;

typedef struct rndis_halt_msg_type {
        long message_type;
        long message_length;
        long request_id;
} rndis_halt_msg_type;

typedef struct rndis_query_msg_type {
        long message_type;
        long message_length;
        long request_id;
        long oid;
        long information_buffer_length;
        long information_buffer_offset;
        long device_vc_handle;
} rndis_query_msg_type;

typedef struct rndis_query_cmplt_type {
        long message_type;
        long message_length;
        long request_id;
        long status;
        long information_buffer_length;
        long information_buffer_offset;
} rndis_query_cmplt_type;

typedef struct rndis_set_msg_type {
        long message_type;
        long message_length;
        long request_id;
        long oid;
        long information_buffer_length;
        long information_buffer_offset;
        long device_vc_handle;
} rndis_set_msg_type;

typedef struct rndis_set_cmplt_type {
        long message_type;
        long message_length;
        long request_id;
        long status;
} rndis_set_cmplt_type;

typedef struct rndis_reset_msg_type {
        long message_type;
        long message_length;
        long reserved;
} rndis_reset_msg_type;

typedef struct rndis_reset_cmplt_type {
        long message_type;
        long message_length;
        long status;
        long addressing_reset;
} rndis_reset_cmplt_type;

typedef struct rndis_indicate_status_msg_type {
        long message_type;
        long message_length;
        long status;
        long status_buffer_length;
        long status_buffer_offset;
	long diag_status;
	long error_offset;
} rndis_indicate_status_msg_type;

typedef struct rndis_keepalive_msg_type {
        long message_type;
        long message_length;
        long request_id;
} rndis_keepalive_msg_type;

typedef struct rndis_keepalive_cmplt_type {
        long message_type;
        long message_length;
        long request_id;
        long status;
} rndis_keepalive_cmplt_type;

typedef struct rndis_packet_msg_type {
        long message_type;
        long message_length;
        long data_offset;
        long data_length;
        long oob_data_offset;
        long oob_data_length;
        long num_oob_data_elements;
        long per_packet_info_offset;
        long per_packet_info_length;
        long vc_handle;
        long reserved;
} rndis_packet_msg_type;

typedef struct rndis_config_parameter {
        long parameter_name_offset;
        long parameter_name_length;
        long parameter_type;
        long parameter_value_offset;
        long parameter_value_length;
} rndis_config_parameter;


//------------------------------------------------------------------------------
//      All functions
//------------------------------------------------------------------------------

void USBDCallbacks_RequestReceived(const USBGenericRequest *request);
void RNDISDriver_RequestHandler(const USBGenericRequest *request);

void RNDISDriver_SetState(unsigned char new_state);

void RNDISDriver_WriteResponseAvailable();
void RNDISDriver_EncapsulatedResponse(unsigned char *srcBuf, unsigned short srcBufLen);
void RNDISDriver_WriteCallback(unsigned int argument, unsigned char status, unsigned int received, unsigned int remaining);

void RNDISDriver_InitResponse();
void RNDISDriver_QueryResponse();
void RNDISDriver_SetResponse();
void RNDISDriver_ResetResponse();
void RNDISDriver_KeepaliveResponse();
void RNDISDriver_IndicateStatus();

void RNDISDriver_ProcessEncapsulatedCommand(unsigned int unused, unsigned char status, unsigned int received, unsigned int remaining);
void RNDISDriver_SendEncapsulatedResponse();

void RNDISDriver_DummyReadCallback(void *argument, unsigned char status, unsigned int received, unsigned int remaining);
void RNDISDriver_DummyWriteCallback(void *argument, unsigned char status, unsigned int received, unsigned int remaining);

//------------------------------------------------------------------------------
//      Exported functions
//------------------------------------------------------------------------------

/* 
 * RNDISDriver_Initialize
 *
 * Call to initialize RNDIS driver. Call *before* USB gets initialized!
 *
 * @param link_state: either RNDIS_STATUS_MEDIA_CONNECT or RNDIS_STATUS_MEDIA_DISCONNECT
 * @param mac_addr: pointer to initial mac address (6 data bytes, no termination)
 *
 * Sample initialization code:
 * 
 *   // driver initialization
 *   char mac[] = {0x47, 0x11, 0x13, 0x37, 0x08, 0x15};
 *   RNDISDriver_Initialize(NDIS_MEDIA_STATE_CONNECTED, mac);
 *
 *   // connect USB
 *   USBD_Connect();
 *
 *   while (USBD_GetState() < USBD_STATE_CONFIGURED);
 */
void RNDISDriver_Initialize(long link_state, char *mac_addr);

/*
 * RNDISDriver_SetLinkState
 *
 * Call whenever present state of ethernet plug changes.
 * @param link_state: either RNDIS_STATUS_MEDIA_CONNECT or RNDIS_STATUS_MEDIA_DISCONNECT
 */
void RNDISDriver_SetLinkState(long link_state);

/*
 * RNDISDriver_Read
 *
 * Use this to poll OUT/DOWN traffic.
 * @param data: data buffer, must be allocated already
 * @param size: (maximum) size of data buffer
 * @param callback: function to call when data has arrived
 * @param argument: optional argument which gets passed to the callback
 *
 * The RNDISDriver_Read function must be called whenever data is to be received. In particular, this function has to be called
 * again after the callback was invoked.
 * It is also possible to call RNDISDriver_Read in a regular interval.
 *
 * Sample Callback header:
 *   void callback(void *argument, unsigned char status, unsigned int received, unsigned int remaining);
 */
unsigned char RNDISDriver_Read(void *data, unsigned int size, TransferCallback callback, void *argument);

/*
 * RNDISDriver_Write
 *
 * Use this to write IN/UP traffic.
 * @param data: data buffer
 * @param size: size of data to be transmitted
 * @param callback: callback function (to be invoked when transfer is finished)
 * @param argument: optional argument which gets passed to the callback
 *
 * Limitation: The size of the buffer mustn't exceed 256 bytes.
 */
unsigned char RNDISDriver_Write(void *data, unsigned int size, TransferCallback callback, void *argument);


#endif //#ifndef RNDISDRIVER_H

