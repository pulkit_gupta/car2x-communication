CAR 2 CAR Communication version 2.0: Where 2 cars are given functioanlity to communicate with each other with help of CAN BUS controller and XBEE modules. For more idea please go to the path

[rootFolder]/VIDEO_SCREENSHOTS

This includes a video showing the working of PLANet boards and also contains some images



For detailed documentation, please go to thge following path

[rootFolder]/DOCUMENTATION/


In this path, you will find the documentation of the project in 3 formats HTML, LATEX and RTF.
Its highly recommended to use HTML version because of ease in usage and jumping from one topic to another.

Hence complete path will be 


[rootFolder]/DOCUMENTATION/html/index.html



Please install any web browser on your computer to read the documentation.
This documentation contains all the steps to install and use the code for further development or even to use the current functionlity. All the code is heavily commented and displayed with GUI docs.

For more detailed information about this project please go to path:

[rootFolder]/DOCUMENTATION/Car2Car_Planet.pdf

This file contains all information about this project.

All source code is available at path:
[rootFolder]/source/


Driver source files are available at:
[rootFolder]/source/drivers


Header files used by all sources are placed at:
[rootFolder]/include/



Developer Contact:

Pulkit Gupta

Email: Pulkit.itp@gmail.com


