var spi_8c =
[
    [ "spi_exchange", "spi_8c.html#a14b4906c27adf88c47cdde55a731b7f9", null ],
    [ "spi_get_byte", "spi_8c.html#ae85f9e4e7a6442ca107609e648b703b1", null ],
    [ "spi_init_buffer", "spi_8c.html#a4778f4898421d6780775c7e6361be035", null ],
    [ "spi_load_byte", "spi_8c.html#affd7ee385962557209d585c15b2ea54f", null ],
    [ "spi_load_zeros", "spi_8c.html#a8f7a045878b18042a4064fe19eb911ae", null ],
    [ "SPI_SelectDevice", "spi_8c.html#abd1d96b788e58911c232460b29d12c42", null ],
    [ "SPI_UnselectDevice", "spi_8c.html#a33185bacfecd9035468d86de41a6821e", null ],
    [ "spiDummyByte", "spi_8c.html#a0be764400c6aca6f657c0e073beaa31e", null ],
    [ "SPIRead", "spi_8c.html#ad8671716e55a9c0b62dafc0a8c1e3eab", null ],
    [ "spiTransfer16", "spi_8c.html#a6d8448b3d4a48873208847fcc937aecd", null ],
    [ "spiTransfer32", "spi_8c.html#aadcd6e4ee63606e8c94901c12ed8d808", null ],
    [ "spiTransferByte", "spi_8c.html#a78f7197c0aa322f800ce77311a6efe99", null ],
    [ "SPIWrite", "spi_8c.html#af4db4b233c22b8481b045a6b078a0466", null ],
    [ "spi_counter", "spi_8c.html#a9861782834c2cf02a0b5a66d4a9bfee7", null ],
    [ "spi_data_buffer", "spi_8c.html#af58126cee23b0e58a4d2edc794b5cd50", null ],
    [ "spi_pointer", "spi_8c.html#aebd39b35f10767a2a55c9ec3af3a789b", null ],
    [ "temp", "spi_8c.html#a2643ad9dd7301f6523ce47c52fab0058", null ]
];